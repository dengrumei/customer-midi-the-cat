<img src="https://img.itch.zone/aW1hZ2UvMjc3ODg4LzEzNDc3NTgucG5n/original/AdH9GJ.png">

# Customer Project - Midi the Cat
![Maintenance](https://img.shields.io/badge/2017.4-Mac/Windows-green.svg)
![Maintenance](https://img.shields.io/badge/2018.1-Mac/Windows-green.svg)
![Maintenance](https://img.shields.io/badge/2018.2-Mac/Windows-green.svg)
![Maintenance](https://img.shields.io/badge/2018.3-Mac/Windows-green.svg)
![Maintenance](https://img.shields.io/badge/2019.1-Mac/Windows-green.svg)
![Maintenance](https://img.shields.io/badge/2019.2-Mac/Windows-green.svg)
![Maintenance](https://img.shields.io/badge/Project_Not_Linux_Compatible-red.svg)
<p>Midi the Cat the musical adventure game.  

Simple musical patterns control the cat, and playing on the rhythm will speed you up. Designed to be playable by a total beginner to music, the game introduces basic musical concepts in an approachable way. 

Game can be played using a Midi keyboard as well as a regular PC keyboard.

You can view the project on youtube <a href="https://youtu.be/zneWKb94rRw" target="_blank">**here**</a>. 

*Uploaded with Unity Version: 2017.4.3f1*<p>

## Table of Contents

- [Features](#features)
- [Platforms](#platforms)
- [Setup](#setup)
- [Known Issues](#knownissues)
- [Tests](#tests)
- [Documentation](#documentation)

## Features

![Uses-TextMeshPro !](https://img.shields.io/badge/Uses-TextMeshPro-1abc9c.svg)
Tilemap
Sprites 
Materials and Lighting
Animations
Audio and Midi Input

## Platforms

- Mac OSX
- Windows
- Linux ---> while Midi builds and works on Linux from Windows, i have issues with the MidiPlugin in the Linuz Editor Project. This is a 3rd party plugin and i don't know if it will ever work.

## Setup
When upgrading from older versions, you may need to
- Switch from Asset Store Version of TMP to the Package Manager one
- Resolve Tilemap include errors

## KnownIssues

## Tests
Start from the Intro scene. 
You should see a video playing as a trailer for the game.
Press any Key to Start the game.
Game starts with a cutscene. Wait for the cutscene to finish.
Press QWERTY to make the cat move. Follow the tutorial on the screen to progress into the game.

## Jenkins
[https://regqa.prd.it.unity3d.com/view/job/customer-projects/job/Customer-Midi-the-Cat/](https://regqa.prd.it.unity3d.com/view/Upgrade%20Testing/job/customer-projects/job/Customer-Midi-the-Cat/)
