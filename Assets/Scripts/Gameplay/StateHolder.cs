﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface StateHolder<T>
{
    void SetState(T s);
    T GetState();
}
