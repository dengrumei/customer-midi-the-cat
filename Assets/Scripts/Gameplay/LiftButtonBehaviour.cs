﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftButtonBehaviour : TileObject
{
    PlayerControl m_player;
    public AudioClip m_triggerSound;

    bool m_triggered;

	// Use this for initialization
	void Start ()
    {
        m_player = FindObjectOfType<PlayerControl>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!m_triggered && IsOnTileWith(m_player.GetPlayerMidPoint()))
        {
            PushButton();
        }
	}

    void PushButton()
    {
        // Play sound
        if (m_triggerSound != null)
        {
            MidiUtil.PlayClipAtPoint(m_triggerSound, transform.position, 1.0f, 1.0f);
        }
        if (m_triggered)
        {
            return;
        }
        LiftBehaviour[] lifts = FindObjectsOfType<LiftBehaviour>();
        foreach (LiftBehaviour lift in lifts)
        {
            lift.StartLift();
        }

        m_triggered = true;
    }
}
