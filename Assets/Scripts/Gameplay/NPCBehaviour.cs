﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using System;

public class NPCBehaviour : TileObject, StateHolder<NPCBehaviour.State>
{
    PlayerControl m_player;
    //MusicManager m_musicManager;
    Flowchart m_flowChart;
    Animator m_animator;

    public Transform m_moveTarget;
    Vector3 m_startPosition;
    float m_animationTimer;

    bool m_alreadyInRange;

    public float m_moveSpeed = 1.0f;

    [Serializable]
    public class State
    {
        // What flowchart state we are in
        //List<string> m_runningBlockNames;
        //List<int> m_runningBlockCommandIndices;
        // ...and all the flowchart variables
        List<string> m_boolVariableKeys;
        List<bool>   m_boolVariableValues;

        List<string> m_floatVariableKeys;
        List<float>  m_floatVariableValues;

        List<string> m_intVariableKeys;
        List<int>    m_intVariableValues;

        List<string> m_stringVariableKeys;
        List<string> m_stringVariableValues;

        public State()
        {
            //m_runningBlockNames = new List<string>();
            //m_runningBlockCommandIndices = new List<int>();

            m_boolVariableKeys = new List<string>();
            m_boolVariableValues = new List<bool>();
            m_floatVariableKeys = new List<string>();
            m_floatVariableValues = new List<float>();
            m_intVariableKeys = new List<string>();
            m_intVariableValues = new List<int>();
            m_stringVariableKeys = new List<string>();
            m_stringVariableValues = new List<string>();
        }

        public void ReadFlowchartState(Flowchart flowChart)
        {
            // Remember state of the flowchart
            /*m_runningBlockNames.Clear();
            m_runningBlockCommandIndices.Clear();

            List<Block> runningBlocks = flowChart.GetExecutingBlocks();
            foreach (Block block in runningBlocks)
            {
                m_runningBlockNames.Add(block.BlockName);
                int commandIndex = block.CommandList.IndexOf(block.ActiveCommand);
                m_runningBlockCommandIndices.Add(commandIndex);
            }*/
            m_boolVariableKeys.Clear();
            m_boolVariableValues.Clear();
            m_floatVariableKeys.Clear();
            m_floatVariableValues.Clear();
            m_intVariableKeys.Clear();
            m_intVariableValues.Clear();
            m_stringVariableKeys.Clear();
            m_stringVariableValues.Clear();

            List <Variable> variables = flowChart.Variables;
            foreach(Variable variable in variables)
            {
                if (variable.Scope == VariableScope.Public)
                {
                    return;
                }
                if (variable is BooleanVariable)
                {
                    BooleanVariable theVariable = (BooleanVariable)variable;
                    m_boolVariableKeys.Add(theVariable.Key);
                    m_boolVariableValues.Add(theVariable.Value);
                }
                else if (variable is FloatVariable)
                {
                    FloatVariable theVariable = (FloatVariable)variable;
                    m_floatVariableKeys.Add(theVariable.Key);
                    m_floatVariableValues.Add(theVariable.Value);
                }
                else if (variable is IntegerVariable)
                {
                    IntegerVariable theVariable = (IntegerVariable)variable;
                    m_intVariableKeys.Add(theVariable.Key);
                    m_intVariableValues.Add(theVariable.Value);
                }
                else if (variable is StringVariable)
                {
                    StringVariable theVariable = (StringVariable)variable;
                    m_stringVariableKeys.Add(theVariable.Key);
                    m_stringVariableValues.Add(theVariable.Value);
                }
            }
        }

        public void SetFlowchartState(Flowchart flowChart)
        {
            //if (m_runningBlockNames.Count == 0)
            {
                
                //return;
            }
            //else
            {
                /*for(int i = 0; i < m_runningBlockNames.Count; i++)
                {
                    Block toExecute = flowChart.FindBlock(m_runningBlockNames[i]);
                    flowChart.ExecuteBlock(toExecute, m_runningBlockCommandIndices[i]);
                }*/
                for(int i = 0; i < m_boolVariableKeys.Count; i++)
                {
                    flowChart.SetBooleanVariable(m_boolVariableKeys[i], m_boolVariableValues[i]);
                }
                for (int i = 0; i < m_floatVariableKeys.Count; i++)
                {
                    flowChart.SetFloatVariable(m_floatVariableKeys[i], m_floatVariableValues[i]);
                }
                for (int i = 0; i < m_intVariableKeys.Count; i++)
                {
                    flowChart.SetIntegerVariable(m_intVariableKeys[i], m_intVariableValues[i]);
                }
                for (int i = 0; i < m_stringVariableKeys.Count; i++)
                {
                    flowChart.SetStringVariable(m_stringVariableKeys[i], m_stringVariableValues[i]);
                }
            }
            flowChart.ExecuteBlock("Start");
        }
    }

    State m_state;

    public void SetState(State s)
    {
        m_state = s;
    }

    public State GetState()
    {
        return m_state;
    }

    // Use this for initialization
    void Start ()
    {
        m_player = FindObjectOfType<PlayerControl>();
        //m_musicManager = FindObjectOfType<MusicManager>();
        m_flowChart = GetComponent<Flowchart>();
        m_animator = GetComponent<Animator>();
        m_alreadyInRange = false;

        if (m_state == null)
        {
            m_state = new State();
        }
        m_startPosition = transform.position;
	}

    public void SetMoveTarget(Transform destination)
    {
        m_moveTarget = destination;
    }

    void StartDialogue()
    {
        Debug.Log("Starting dialogue");

        m_state.SetFlowchartState(m_flowChart);

        //m_musicManager.SetTrackVolume(0, 0.0f, 8.0f);
        //m_musicManager.SetTrackVolume(1, 0.0f, 8.0f);
        //m_musicManager.SetTrackVolume(2, 0.0f, 8.0f);
        //m_musicManager.SetTrackVolume(3, 0.0f, 8.0f);

        m_player.SetIgnoreWhiteInputs(true);
        //RhythmRingControl ringControl = FindObjectOfType<RhythmRingControl>();
        //ringControl.HideRings();
        //ringControl.enabled = false;

        InfoTextBehaviour info = FindObjectOfType<InfoTextBehaviour>();
        if (info != null)
        {
            Character chara = GetComponent<Character>();
            info.ShowText(chara.NameText + "\n" + chara.GetDescription(), 20.0f, chara.NameColor);
        }

        StaffUIControl staff = FindObjectOfType<StaffUIControl>();
        if (staff != null)
        {
            staff.SetFadeTarget(0.0f);
        }
    }

    public void EndDialogue()
    {
        m_flowChart.StopAllBlocks();

        Stage stage = FindObjectOfType<Stage>();
        if (stage != null)
        {
            Character[] characters = stage.CharactersOnStage.ToArray();
            foreach(Character character in characters)
            {
                stage.Hide(character, "Offscreen Right");
            }
        }
        DialogueHolder[] holders = FindObjectsOfType<DialogueHolder>();
        foreach(DialogueHolder holder in holders)
        {
            holder.StopWorldSay();
        }
        DialoguePanelBehaviour.Instance.ResetChoices();

        //m_musicManager.SetTrackVolume(0, 1.0f, 8.0f);

        m_player.SetIgnoreWhiteInputs(false);
        //RhythmRingControl ringControl = FindObjectOfType<RhythmRingControl>();
        //ringControl.enabled = true;

        InfoTextBehaviour info = FindObjectOfType<InfoTextBehaviour>();
        if (info != null)
        {
            info.StopText();
        }
    }

    void StartPushButtonAnimation()
    {
        m_animationTimer = 6;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (m_animationTimer > 0)
        {
            float time = 6.0f - m_animationTimer;
            if (time < 2.0f)
            {
                transform.position = Vector3.MoveTowards(transform.position, m_moveTarget.position, m_moveSpeed * Time.deltaTime);
                if (Vector3.Magnitude(m_moveTarget.position - transform.position) < 0.1f)
                {
                    m_animator.SetTrigger("StopWalk");
                }
                else
                {
                    m_animator.SetTrigger("StartWalk");
                }
                transform.rotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
            }
            else if (time < 4.0f)
            {

            }
            else if (time < 6.0f)
            {
                transform.position = Vector3.MoveTowards(transform.position, m_startPosition, m_moveSpeed * Time.deltaTime);
                if (Vector3.Magnitude(m_startPosition - transform.position) < 0.1f)
                {
                    m_animator.SetTrigger("StopWalk");
                }
                else
                {
                    m_animator.SetTrigger("StartWalk");
                }
                transform.rotation = Quaternion.Euler(0.0f, -90.0f, 0.0f);
            }

            m_animationTimer -= Time.deltaTime;
            if (m_animationTimer <= 0)
            {
                transform.position = m_startPosition;
            }
        }
        else
        {
            bool inRange = IsInRangeOf(m_player.GetPlayerMidPoint(), 2.25f);
            if (inRange && !m_alreadyInRange) // Start the dialogue when just moved into range
            {
                StartDialogue();
            }

            if (m_flowChart.HasExecutingBlocks() || DialoguePanelBehaviour.Instance.IsWaitingForPlayer())
            {
                m_state.ReadFlowchartState(m_flowChart);
            }
            else if (inRange)
            {
                EndDialogue();
            }

            if (!inRange && m_alreadyInRange)
            {
                EndDialogue();
            }
            m_alreadyInRange = inRange;
        }
	}
}
