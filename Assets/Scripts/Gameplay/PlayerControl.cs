﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControl : MonoBehaviour, StateHolder<PlayerControl.State>
{
    int m_stepsLayer;
    int m_stepsDownLayer;
    int m_deathLayer;
    int m_obstacleLayer;
    int m_raycastMask;

    public enum StepState
    {
        Off,
        GoingUp,
        UpEnd,
        GoingDown,
        DownEnd
        //AtBranch
    }

    public enum StepType
    {
        Upwards,
        Downwards
    }

    public enum WallState
    {
        Off,
        Hanging
    }

    int m_lastNote;
    // The beat time that that note was allocated to, so you know when a new note should be allocated for
    double[] m_noteHistory = { -1, -2, -3 };
    double m_lastNoteTimeSeconds;

    double m_lastFallBeat;
    bool m_ignoreWhiteInputs = false;

    int m_health = 1;

    public bool m_debugMode;
    MusicManager m_musicManager;
    CatGraphicsControl m_catGraphics;
    MoveStorage m_moveStore;
    MusicalFeedbackController m_feedbackControl;

    [Serializable]
    public class State : ICloneable
    {
        public Vector3 m_position;
        public int m_lastDirection = 1;
        public StepState m_stepState = StepState.Off;
        public WallState m_wallState = WallState.Off;
        public Vector2 m_stepBottom;
        public int m_stepID;
        public StepType m_stepType;
        public int m_stepCount;
        public int m_momentum = 1;
        public bool m_sneaking = false;
        public bool m_canSneak = false;
        public bool m_linearMode = false;

        object ICloneable.Clone()
        {
            return MemberwiseClone();
        }
        public State Clone()
        {
            return (State)MemberwiseClone();
        }

        public Vector2 GetRaycastPosition()
        {
            return new Vector2(m_position.x, m_position.y + 0.1f);
        }

        public Vector2 GetMidPoint()
        {
            return new Vector2(m_position.x, m_position.y + 0.5f);
        }
    }

    Vector2 GetStepSize()
    {
        int stepsOnATile = m_state.m_stepCount;
        float tileWidth = 1.0f;
        float tileHeight = 0.5f;
        return new Vector2(tileWidth / stepsOnATile, tileHeight / stepsOnATile);
    }

    State m_state;
    State m_lastSafeState;

    public void SetState(State s)
    {
        m_state = s;
        SaveSafeState ();
        transform.position = s.m_position;
    }

    public State GetState()
    {
        m_state.m_position = transform.position;
        return m_state;
    }

    void Start()
    {
        m_stepsLayer = LayerMask.NameToLayer("Steps");
        m_stepsDownLayer = LayerMask.NameToLayer("StepsDown");
        m_deathLayer = LayerMask.NameToLayer("Death");
        m_obstacleLayer = LayerMask.NameToLayer("Default");
        m_raycastMask = ~ (LayerMask.GetMask("HittableButtons") | LayerMask.GetMask("Player"));
        m_musicManager = FindObjectOfType<MusicManager>();
        m_catGraphics = FindObjectOfType<CatGraphicsControl>();
        m_moveStore = GetComponent<MoveStorage>();

        if (m_state == null)
        {
            m_state = new State();
        }
        SaveSafeState ();
        MoveStorage.Step initialStep = new MoveStorage.Step(transform.position.xy(), transform.position.xy(), MoveStorage.MoveType.Walk);
        initialStep.m_direction = m_state.m_lastDirection;
        m_moveStore.AddStep(initialStep);
        m_moveStore.SetMovementTime(-10, -10);
    }

    public void SaveSafeState()
    {
        if (MovementUtils.IsOnGround(m_state))
        {
            m_lastSafeState = m_state;
        }
    }

    public StepState GetStepState()
    {
        return m_state.m_stepState;
    }

    public WallState GetWallState()
    {
        return m_state.m_wallState;
    }

    public void SetWallState(WallState newState)
    {
        if (newState != m_state.m_wallState)
        {
            m_state.m_wallState = newState;
            if (newState == WallState.Off)
            {
                m_catGraphics.StopHangWall();
            }
        }
    }

    public int GetLastDirection()
    {
        return m_state.m_lastDirection;
    }

    public int GetMomentum()
    {
        return m_state.m_momentum;
    }

    public double GetLastNoteAllocatedBeatTime()
    {
        return m_noteHistory[0];
    }

    /// <summary>
    /// Index 0 is the newest note time, and older note times are at the end of the array
    /// </summary>
    /// <returns></returns>
    public double[] GetNoteHistory()
    {
        return m_noteHistory;
    }

    public int GetLastNote()
    {
        return m_lastNote;
    }

    public void SetLastNote(int noteID)
    {
        m_lastNote = noteID;
    }

    public double GetCurrentIntervalLength()
    {
        double intervalLength = 1.0;
        //if (m_state.m_stepState != StepState.Off)
        //{
        //    intervalLength = 0.5;
        //}
        return intervalLength;
    }

    public double GetClosestAllowableBeat(double currentBeatTime)
    {
        double intervalLength = GetCurrentIntervalLength();
        double closestAllowableBeat = MidiUtil.RoundBeatToNearestInterval(currentBeatTime, intervalLength);
        return closestAllowableBeat;
    }

    public void SetIgnoreWhiteInputs(bool ignore)
    {
        m_ignoreWhiteInputs = ignore;
    }

    public bool GetIgnoreWhiteInputs()
    {
        return m_ignoreWhiteInputs;
    }

    public void StoreNoteAllocation(double noteTime)
    {
        for (int i = m_noteHistory.Length - 1; i > 0; i--)
        {
            m_noteHistory[i] = m_noteHistory[i - 1];
        }
        m_noteHistory[0] = noteTime;
    }

    bool m_variableRhythm = false;
    
    public void Move(Vector3 movement, bool snapGraphics = false, MoveStorage.MoveType moveType = MoveStorage.MoveType.Walk, int direction = 0)
    {
        if (snapGraphics)
        {
            double currentBeatTime = m_musicManager.GetCurrentBeat();
            m_moveStore.ApplyInstantStepShift(currentBeatTime, movement.xy(), MoveStorage.MoveType.Walk);
        }
        else
        {
            MoveStorage.Step newStep = new MoveStorage.Step(transform.position, transform.position + movement, moveType);
            if (direction != 0)
            {
                newStep.m_direction = direction;
            }
            m_moveStore.AddStep(newStep);
        }
        transform.position += movement;
    }

	// Update is called once per frame
	void Update()
    {
        double currentBeatTime = m_musicManager.GetCurrentBeat();
        double currentInterval = GetCurrentIntervalLength();
        if (currentBeatTime > GetLastNoteAllocatedBeatTime() + (currentInterval * 1.5))
        {
            m_state.m_momentum = 1;
        }
        HandleFalling(currentBeatTime);
        if (m_health <= 0)
        {
            Die();
        }
        if (Input.GetKeyDown(KeyCode.F6))
        {
            m_musicManager.SetMusicSpeed(1.0f);
            m_variableRhythm = !m_variableRhythm;
        }
	}

    public void Damage()
    {
        m_health--;
    }

    void Die()
    {
        m_catGraphics.enabled = false;
        m_catGraphics.GetComponent<Animator>().speed = 0.0f;
        bool loadedSave = SaveGameSystem.LoadCurrentSaveGame();
        if (!loadedSave)
        {
            AutoFade.LoadLevel(SceneManager.GetActiveScene().buildIndex, 1.0f, 1.0f, Color.black);
        }
        gameObject.SetActive(false);
    }

    void HandleFalling(double currentBeatTime)
    {
        if (m_moveStore.HasPendingMoves(currentBeatTime))
        {
            return;
        }
        double fallInterval = 0.5; // We want the falls to be every half-beat

        double currentFallBeat = MidiUtil.RoundBeatToLatestInterval(currentBeatTime, fallInterval);

        double nextFallBeatAfterMoving = MidiUtil.RoundBeatToNearestInterval(GetLastNoteAllocatedBeatTime() + fallInterval, fallInterval) + fallInterval;
        double nextFallBeatAfterFalling = m_lastFallBeat + fallInterval;
        double nextFallBeat = MidiUtil.Max(nextFallBeatAfterMoving, nextFallBeatAfterFalling);

        if (currentFallBeat >= nextFallBeat && m_state.m_wallState == WallState.Off && m_state.m_stepState == StepState.Off)
        {
            m_lastFallBeat = currentFallBeat;
            Vector2 position = GetPlayerPosition();
            RaycastHit2D downHit = DebuggableRaycast(position, new Vector2(0.0f, -1.0f));
            if (downHit.collider == null || downHit.transform.gameObject.layer == m_deathLayer)
            {
                Fall(1.0f, currentFallBeat, currentFallBeat + 0.5, m_state.m_lastDirection);
                //Debug.Log("Falling 1");
            }
            else if (downHit.collider != null && IsStepsLayer(downHit.transform.gameObject.layer))
            {
                float fallDistance = 1.0f;
                if (downHit.distance < 0.25f)
                {
                    fallDistance = 0.5f;
                }
                //Debug.Log("Falling onto steps " + fallDistance);
                StepType stepType = downHit.transform.gameObject.layer == MovementUtils.s_stepsLayer ? StepType.Upwards : StepType.Downwards;
                LandOnSteps(m_state.m_lastDirection, stepType, fallDistance, currentFallBeat, currentFallBeat + (0.5 * fallDistance));
            }
            else if (downHit.collider != null && downHit.distance > 0.5f)
            {
                //Debug.Log("Falling 0.5");
                Fall(0.5f, currentFallBeat, currentFallBeat + 0.25, m_state.m_lastDirection);
            }
        }
    }

    bool IsStepsLayer(int layerID)
    {
        return layerID == m_stepsLayer || layerID == m_stepsDownLayer;
    }
    bool IsDeathLayer(int layerID)
    {
        return layerID == m_deathLayer;
    }
    bool IsObstacleLayer(int layerID)
    {
        return layerID == m_obstacleLayer;
    }

    Vector3 GetPlayerPosition3()
    {
        return transform.position + new Vector3(0.0f, 0.1f, 0.0f);
    }

    public Vector2 GetPlayerPosition()
    {
        return new Vector2(transform.position.x, transform.position.y + 0.1f);
    }

    public Vector3 GetPlayerMidPoint()
    {
        return new Vector3(transform.position.x, transform.position.y + 0.5f, 0.0f);
    }

    bool Step(int direction, int verticalDirection)
    {
        switch(m_state.m_stepState)
        {
            case StepState.Off:
                RaycastHit2D forwardHit = DebuggableRaycast(GetPlayerPosition(), new Vector2(direction, 0.0f));
                RaycastHit2D aheadDownHit = DebuggableRaycast(GetPlayerPosition() + new Vector2(direction * (0.75f), -0.2f), new Vector2(0.0f, -0.25f));
                if (forwardHit.collider != null)
                {
                    if (IsStepsLayer(forwardHit.transform.gameObject.layer))
                    {
                        if (aheadDownHit.collider != null && IsStepsLayer(aheadDownHit.transform.gameObject.layer))
                        {
                            if (verticalDirection > 0)
                            {
                                StartGoingUpwards(direction);
                            }
                            else if (verticalDirection < 0)
                            {
                                StartGoingDownwards(direction);
                            }
                            else
                            {
                                Debug.Log("Hitting branching steps!");
                                Move(Vector3.zero, false, MoveStorage.MoveType.Walk, direction);
                                return false;
                            }
                        }
                        else
                        {
                            Debug.Log("Hitting steps up");
                            StartGoingUpwards(direction);
                        }
                    }
                    else
                    {
                        Debug.Log("Hitting wall");
                        Move(Vector3.zero, false, MoveStorage.MoveType.Walk, direction);
                        return false;
                    }
                }
                else
                {
                    if (aheadDownHit.collider != null && IsStepsLayer(aheadDownHit.transform.gameObject.layer))
                    {
                        StartGoingDownwards(direction);
                    }
                    else
                    {
                        Move(new Vector3(direction, 0.0f, 0.0f));
                    }
                }
                break;

            case StepState.GoingUp:
                SetStepPosition(m_state.m_stepBottom, m_state.m_stepID + direction, m_state.m_stepType, m_state.m_stepCount, direction);
                SearchForEndOfStepsUp(direction);
                break;

            case StepState.GoingDown:
                SetStepPosition(m_state.m_stepBottom, m_state.m_stepID + direction, m_state.m_stepType, m_state.m_stepCount, direction);
                SearchForEndOfStepsDown(direction);
                break;

            case StepState.UpEnd:
            case StepState.DownEnd:
                Vector2 nextGridSquare = m_state.m_stepBottom + new Vector2(direction, 0.0f);
                if ((m_state.m_stepType == StepType.Upwards && direction == 1) || (m_state.m_stepType == StepType.Downwards && direction == -1)) // If we're going up...
                {
                    nextGridSquare.y += 0.5f;
                }

                Vector3 toDestination = new Vector3(nextGridSquare.x, nextGridSquare.y, 0.0f) - transform.position;
                Move(toDestination, false, MoveStorage.MoveType.Walk, direction);
                m_state.m_stepState = StepState.Off;
                break;
        }
        return true;
    }

    void SearchForEndOfStepsUp(int direction)
    {
        RaycastHit2D postStepForwardHit = DebuggableRaycast(GetPlayerPosition() + new Vector2(GetStepSize().x * direction, 0.0f), new Vector2(direction * 0.5f, 0.0f));
        if (postStepForwardHit.collider == null)
        {
            m_state.m_stepState = StepState.UpEnd;
            Debug.Log("At end of stairs up");
            //m_musicManager.SetTrackVolume(1, 0.0f);
        }
    }

    void SearchForEndOfStepsDown(int direction)
    {
        RaycastHit2D postStepAheadDownHit = DebuggableRaycast(GetPlayerPosition() + new Vector2(GetStepSize().x * direction, 0.0f), new Vector2(0.0f, -1.0f), Color.green);
        if (postStepAheadDownHit.collider == null || !IsStepsLayer(postStepAheadDownHit.transform.gameObject.layer))
        {
            m_state.m_stepState = StepState.DownEnd;
            Debug.Log("At end of stairs down");
            //m_musicManager.SetTrackVolume(1, 0.0f);
        }
    }

    Vector2 GetPositionOnSteps(Vector2 stepBottom, int stepID, StepType stepType)
    {
        Vector2 stepSize = GetStepSize();
        Vector2 bottomLeft = stepBottom + new Vector2(-(0.5f + stepSize.x * 0.5f), 0.0f);
        Vector2 stepOffset = stepSize * stepID;
        if (stepType == StepType.Downwards)
        {
            stepOffset.y = (0.5f + stepSize.y) - stepOffset.y;
        }
        return bottomLeft + stepOffset;
    }

    void SetStepPosition(Vector2 stepBottom, int stepID, StepType stepType, int stepCount, int direction)
    {
        if (stepID < 1)
        {
            stepID += stepCount;
            float bottomOfLeftStep = (stepType == StepType.Upwards ? -0.5f : 0.5f);
            stepBottom = stepBottom + new Vector2(-1.0f, bottomOfLeftStep);
        }
        else if (stepID > stepCount)
        {
            stepID -= stepCount;
            float bottomOfRightStep = (stepType == StepType.Upwards ? 0.5f : -0.5f);
            stepBottom = stepBottom + new Vector2(1.0f, bottomOfRightStep);
        }
        m_state.m_stepBottom = stepBottom;
        m_state.m_stepID = stepID;
        m_state.m_stepType = stepType;
        m_state.m_stepCount = stepCount;
        Vector2 stepPosition = GetPositionOnSteps(stepBottom, stepID, stepType);

        MoveStorage.MoveType moveType = MoveStorage.MoveType.WalkRightUpwards;
        if (stepType == StepType.Downwards)
        {
            moveType = MoveStorage.MoveType.WalkRightDownwards;
        }
        Move(new Vector3(stepPosition.x, stepPosition.y, 0.0f) - transform.position, false, moveType, direction);
    }

    //void StartOnBranchingSteps(int direction)
    //{
    //    Move(new Vector3(direction * 0.5f, 0.0f, 0.0f));
    //    m_musicManager.SetTrackVolume(1, 1.0f, 0.125f);
    //    m_state.m_stepState = StepState.AtBranch;
    //}

    void BackAwayFromBranchingSteps(int direction)
    {
        Move(new Vector3(direction * 0.5f, 0.0f, 0.0f));
        //m_musicManager.SetTrackVolume(1, 0.0f);
        m_state.m_stepState = StepState.Off;
        //m_catGraphics.StartWalk(direction);
    }

    void StartGoingUpwards(int direction)
    {
        int stepCount = 2;
        SetStepPosition(transform.position + new Vector3(direction, 0.0f, 0.0f), (direction == 1 ? 1 : stepCount), (direction == 1 ? StepType.Upwards : StepType.Downwards), stepCount, direction);

        m_state.m_stepState = StepState.GoingUp;
        //m_musicManager.SetTrackVolume(1, 1.0f, 0.125f);
    }

    void StartGoingDownwards(int direction)
    {
        int stepCount = 2;
        SetStepPosition(transform.position + new Vector3(direction, -0.5f, 0.0f), (direction == 1 ? 1 : stepCount), (direction == 1 ? StepType.Downwards : StepType.Upwards), stepCount, direction);

        m_state.m_stepState = StepState.GoingDown;
        //m_musicManager.SetTrackVolume(1, 1.0f, 0.125f);
    }

    void LandOnSteps(int direction, StepType stepType, float fallDistance, double currentBeatTime, double landTime)
    {
        int stepCount = (m_state.m_linearMode ? 1 : 2);
        SetStepPosition(transform.position + new Vector3(0.0f, -fallDistance, 0.0f), (direction == 1 ? (stepCount / 2) + 1 : (stepCount / 2)), stepType, stepCount, direction);
        if (direction == 1)
        {
            if (stepType == StepType.Downwards)
            {
                m_state.m_stepState = StepState.GoingDown;
            }
            else
            {
                m_state.m_stepState = StepState.GoingUp;
            }
        }
        else
        {
            if (stepType == StepType.Downwards)
            {
                m_state.m_stepState = StepState.GoingUp;
            }
            else
            {
                m_state.m_stepState = StepState.GoingDown;
            }
        }

        //m_catGraphics.SetTimeToMove(1.0, 0.5);
        m_moveStore.SetMovementTime(currentBeatTime, landTime, false);
        //m_musicManager.SetTrackVolume(1, 1.0f, 0.125f);

        if (m_state.m_stepState == StepState.GoingUp)
        {
            SearchForEndOfStepsUp(direction);
        }
        else
        {
            SearchForEndOfStepsDown(direction);
        }
    }

    public RaycastHit2D DebuggableRaycast(Vector2 position, Vector2 direction, Color? colour = null, int? mask = null)
    {
        return Utils.DebuggableRaycast(position, direction, colour, (mask != null ? mask.Value : m_raycastMask));
    }

    void Fall(float distance, double currentBeatTime, double landTime, int direction)
    {
        Move(new Vector3(0.0f, -distance, 0.0f), false, MoveStorage.MoveType.Fall, direction);
        m_moveStore.SetMovementTime(currentBeatTime, landTime, false);
        //m_catGraphics.SetTimeToMove(1.0, 0.5);
    }
}
