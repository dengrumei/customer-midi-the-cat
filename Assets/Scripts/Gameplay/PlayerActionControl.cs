﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionControl : MonoBehaviour
{
    public ActionSetConfig m_actionSet;

    PlayerControl m_playerControl;
    MoveStorage m_moveStore;
    StaffUIControl m_staffUI;
    TutorialPanelManager m_tutorialManager;
    MusicalFeedbackController m_musicalFeedback;

    //CircularBuffer<InputEvent> m_inputHistory;
    CircularBuffer<PlayerControl.State> m_stateHistory;
    double m_lastAllocatedBeat = -1;
    ActionConfig m_lastAllocatedAction = null;

    MusicManager m_musicManager;

    // Use this for initialization
    void Start()
    {
        MovementUtils.InitialiseMasks();
        foreach (ActionConfig config in m_actionSet.m_actionConfigs)
        {
            if (config.m_inputPattern != null)
            {
                config.m_inputPattern.Initialise();
            }
        }

        m_musicManager = FindObjectOfType<MusicManager>();
        m_playerControl = GetComponent<PlayerControl>();
        m_moveStore = GetComponent<MoveStorage>();
        m_staffUI = FindObjectOfType<StaffUIControl>();
        m_tutorialManager = FindObjectOfType<TutorialPanelManager>();
        m_musicalFeedback = FindObjectOfType<MusicalFeedbackController>();

        //m_inputHistory = new CircularBuffer<InputEvent>(5);
        m_stateHistory = new CircularBuffer<PlayerControl.State>(5);
        MidiInput.Instance.MidiIOEvent += HandleMidiIOEvent;
    }

    double GetClosestAllocatableBeat(double currentBeat)
    {
        // Just assume constant rhythm right now
        double lastBeat = (int)currentBeat;
        double nextBeat = (int)currentBeat + 1;

        double fromLastBeat = currentBeat - lastBeat;
        double along = fromLastBeat / (nextBeat - lastBeat);
        if (along > 0.5)
        {
            return nextBeat;
        }
        else
        {
            return lastBeat;
        }
    }

    void HandleMidiIOEvent(int note, bool down, float velocity)
    {
        double beatTime = m_musicManager.GetCurrentBeat();
        InputEvent newEvent = new InputEvent(note, down, beatTime);
        //m_inputHistory.Add(new InputEvent(note, down, beatTime));
        PlayerControl.State startState = m_playerControl.GetState();
        m_stateHistory.Add(startState);
        m_playerControl.SaveSafeState ();

        double beatToAllocateTo = GetClosestAllocatableBeat(beatTime);

        bool doneAnyAction = false;
        bool matchedTowardsAValidAction = false;
        for (int i = 0; i < m_actionSet.m_actionConfigs.Length; i++)
        {
            ActionConfig action = m_actionSet.m_actionConfigs[i];
            bool matches = action.m_inputPattern.AddEvent(newEvent);

            if (matches)
            {
                int inputCount = action.m_inputPattern.m_inputs.Length;
                PlayerControl.State sourceState = m_stateHistory.m_history[inputCount - 1];
                // Longer patterns and non-rematched patterns should be prioritied
                bool cannotUseBeat = beatToAllocateTo == m_lastAllocatedBeat && m_lastAllocatedAction != null && m_lastAllocatedAction.m_inputPattern.m_inputs.Length >= action.m_inputPattern.m_inputs.Length && !(m_lastAllocatedAction.m_rematchOnCompletion && !action.m_rematchOnCompletion);
                bool canDoAction = !m_playerControl.GetIgnoreWhiteInputs() && sourceState != null && action.IsValidInState(sourceState) && !doneAnyAction
                    && (m_tutorialManager == null || m_tutorialManager.HasTaughtAction(action) || action.m_startsLearnt) && !cannotUseBeat;
                if (canDoAction)
                {
                    matchedTowardsAValidAction = true;
                }
                if (action.m_inputPattern.IsSearchComplete())
                {
                    //Debug.Log("Input matches " + action.m_name);

                    if (sourceState != null && canDoAction)
                    {
                        Debug.Log("Doing action " + action.m_name);

                        PlayerControl.State newState = sourceState.Clone();
                        newState.m_sneaking = false;
                        action.DoAction(ref newState, m_moveStore);
                        m_moveStore.SetMovementTime(beatTime, beatTime + 1, true);
                        m_playerControl.SetState(newState);

                        if (m_tutorialManager != null)
                        {
                            m_tutorialManager.DoneAction(action);
                        }

                        //ResetActionMatchesOfLength(inputCount);
                        doneAnyAction = true;
                        m_lastAllocatedAction = action;
                        m_lastAllocatedBeat = beatToAllocateTo;
                    }
                    action.m_inputPattern.ClearProgress();

                    if (action.m_rematchOnCompletion)
                    {
                        Debug.Assert(action.m_inputPattern.m_inputs.Length > 1, "It doesn't make sense to rematch on completion when your input is less than 2 inputs long!");
                        action.m_inputPattern.AddEvent(newEvent);
                    }
                }
            }
        }

        if (doneAnyAction)
        {
            m_playerControl.StoreNoteAllocation(beatTime);
            m_playerControl.SetLastNote(note);
            double closestBeat = MidiUtil.RoundBeatToNearestInterval(beatTime, 1.0);
            m_musicManager.SetPlaybackTargetBeat(closestBeat + 1);
        }
        else if (down)
        {
            m_playerControl.GetState().m_momentum = 1;
        }

        if (m_playerControl.GetState().m_linearMode)
        {
            m_playerControl.GetState().m_momentum = 1;
        }

        if (down && !m_playerControl.GetIgnoreWhiteInputs())
        {
            //m_musicManager.SetTrackVolume(0, 1.0f, 0.5f);
            //if (!m_playerControl.GetState().m_sneaking)
            //{
            //    m_musicManager.SetTrackVolume(0, 1.0f, 0.5f);
            //    m_musicManager.SetTrackVolume(2, 1.0f, 0.5f);
            //    //m_musicManager.SetTrackVolume(4, 0.0f, 0.5f);
            //}

            //else
            //{
            //    m_musicManager.SetTrackVolume(0, 0.0f, 0.5f);
            //    m_musicManager.SetTrackVolume(2, 0.0f, 0.5f);
            //    m_musicManager.SetTrackVolume(4, 1.0f, 0.5f);
            //}
            //m_musicManager.SetTrackVolume(3, 0.0f, 0.5f);
            if (m_staffUI != null)
            {
                m_staffUI.AddNote(note, beatTime, !matchedTowardsAValidAction);
                if (!doneAnyAction && beatToAllocateTo == m_lastAllocatedBeat)
                {
                    m_musicalFeedback.EmitFeedbackText("Press on each beat to speed up");
                }
            }
        }
    }

    void ResetActionMatchesOfLength(int length)
    {
        for (int i = 0; i < m_actionSet.m_actionConfigs.Length; i++)
        {
            ActionConfig action = m_actionSet.m_actionConfigs[i];
            if (action.m_inputPattern.m_inputs.Length == length)
            {
                action.m_inputPattern.ClearProgress();
            }
        }
    }
}
