﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForcefieldBehaviour : MonoBehaviour
{
    ParticleSystem[] m_particleSystems;
    BoxCollider2D m_collider;

    bool m_active = true;

	// Use this for initialization
	void Start ()
    {
        if (m_particleSystems == null)
        {
            SetupReferences();
        }
	}

    void SetupReferences()
    {
        m_particleSystems = GetComponentsInChildren<ParticleSystem>();
        m_collider = GetComponent<BoxCollider2D>();
    }

    public void SetActive(bool active)
    {
        if (active != m_active)
        {
            if (m_particleSystems == null)
            {
                SetupReferences();
            }

            if (active)
            {
                foreach (ParticleSystem system in m_particleSystems)
                {
                    system.Play();
                }
                m_collider.enabled = true;
            }
            else
            {
                foreach (ParticleSystem system in m_particleSystems)
                {
                    system.Stop();
                }
                m_collider.enabled = false;
            }
            m_active = active;
        }
    }
}
