﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Player))]
public class PlayerInputMidi : MonoBehaviour
{
    private Player player;
    private MidiInput m_midiIO;
    //private MusicManager m_musicManager;

    private void Start()
    {
        player = GetComponent<Player>();
        m_keysDown = new List<int>();
        m_midiIO = GetComponent<MidiInput>();
        m_midiIO.MidiIOEvent += HandleMidiIOEvent;
        //m_musicManager = FindObjectOfType<MusicManager>();
    }

    List<int> m_keysDown;
    int m_lastNote;

    void HandleMidiIOEvent (int note, bool down, float velocity)
    {
        if (down)
        {
            bool blackKey = MidiUtil.IsBlackNote(note);
            float direction = 1.0f;
            if (blackKey)
            {
                direction = -direction;
            }
            player.SetDirectionalInput(new Vector2(direction, 0.0f));

            // Did we start a jump?
            bool jump = false;
            foreach(int otherNote in m_keysDown)
            {
                if (MidiUtil.KeysBetweenNotes(otherNote, note, blackKey) == 3 || MidiUtil.KeysBetweenNotes(note, otherNote, blackKey) == 3)
                {
                    jump = true;
                    break;
                }
            }

            if (jump)
            {
                player.OnJumpInputDown();
            }

            int verticalDirection = 0;

            if (MidiUtil.KeysBetweenNotes(m_lastNote, note, blackKey) == 2)
            {
                verticalDirection = 1;
            }
            else if (MidiUtil.KeysBetweenNotes(note, m_lastNote, blackKey) == 2)
            {
                verticalDirection = -1;
            }

            player.SetDirectionalInput(new Vector2(direction, verticalDirection));

            //if (!blackKey)
            //{
            //    m_musicManager.SetTrackVolume(2, 1.0f, 0.5f);
            //    m_musicManager.SetTrackVolume(3, 0.0f, 0.5f);
            //}
            //else
            //{
            //    m_musicManager.SetTrackVolume(2, 0.0f, 0.5f);
            //    m_musicManager.SetTrackVolume(3, 1.0f, 0.5f);
            //}

            m_keysDown.Add(note);
            m_lastNote = note;
        }
        else
        {
            

            m_keysDown.Remove(note);
            if (m_keysDown.Count == 0)
            {
                player.SetDirectionalInput(new Vector2(0.0f, 0.0f));
                player.OnJumpInputUp();
            }
        }
    }

    private void Update()
    {
        //Vector2 directionalInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        //player.SetDirectionalInput(directionalInput);

        if (Input.GetButtonDown("Jump"))
        {
            player.OnJumpInputDown();
        }

        if (Input.GetButtonUp("Jump"))
        {
            player.OnJumpInputUp();
        }
    }
}
