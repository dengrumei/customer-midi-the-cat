﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface DestroyableStateHolder<T, AddressType> : StateHolder<T>
{
    AddressType GetAddress();
}
