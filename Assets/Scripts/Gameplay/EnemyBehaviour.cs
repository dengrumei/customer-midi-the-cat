﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public float m_movementPerBeat = 1.0f;
    float m_actualSpeed;
    int m_direction = 1;
    public float m_beatRotateTime = 0.25f;
    float m_rotateSpeed;
    public float m_animatedSpeed = 1.0f;
    public Transform m_rotationTransform;
    float m_animationSpeedMultiplier;

    Animator m_animator;

    public AudioClip[] m_footstepClips;
    AudioSource m_audioSource;
    float m_startVolume;
    MusicManager m_musicManager;

    int m_lastBeat;

    //PlayerControl m_player;

    void Start()
    {
        m_musicManager = FindObjectOfType<MusicManager>();
        //m_player = FindObjectOfType<PlayerControl>();
        m_animator = GetComponentInChildren<Animator>();

        UpdateSpeed();
        
        m_animator.Play("Anim_Walk", 0, 0.1f);

        m_audioSource = GetComponent<AudioSource>();
        m_startVolume = m_audioSource.volume;
    }

    Vector2 GetPosition()
    {
        return new Vector2(transform.position.x, transform.position.y + 0.1f);
    }

    void UpdateSpeed()
    {
        m_actualSpeed = m_movementPerBeat * (float)m_musicManager.SecondsToBeat(1.0);
        float timeToRotate = (float)m_musicManager.GetTimePerBeat() * m_beatRotateTime;
        m_rotateSpeed = 180.0f / timeToRotate;
        float animatedSpeed = 2.0f / m_animator.GetCurrentAnimatorClipInfo(0)[0].clip.length; // Cycle is both feet
        m_animationSpeedMultiplier = m_actualSpeed / animatedSpeed;
        m_animator.speed = m_animationSpeedMultiplier;
    }

    void Update()
    {
        UpdateSpeed();
        Vector2 position = GetPosition();
        RaycastHit2D hitAheadLow = Utils.DebuggableRaycast(position, new Vector2(0.5f * m_direction, 0.0f), Color.red, MovementUtils.s_raycastMask);
        RaycastHit2D hitAheadHigh = Utils.DebuggableRaycast(position + new Vector2(0.0f, 1.0f), new Vector2(0.5f * m_direction, 0.0f), Color.red, MovementUtils.s_raycastMask);
        RaycastHit2D hitAheadBelow = Utils.DebuggableRaycast(position + new Vector2(0.5f * m_direction, 0.0f), new Vector2(0.0f, -0.5f), Color.red, MovementUtils.s_raycastMask);
        if (hitAheadLow.collider != null || hitAheadHigh.collider != null ||
            (hitAheadBelow.collider == null || hitAheadBelow.collider.gameObject.layer == LayerMask.NameToLayer("Steps") || hitAheadBelow.collider.gameObject.layer == LayerMask.NameToLayer("StepsDown")))
        {
            m_direction = -m_direction;
        }
        transform.position += new Vector3(m_actualSpeed * Time.deltaTime * m_direction, 0.0f, 0.0f);

        Quaternion targetRotation = transform.localRotation;
        if (m_direction == 1)
        {
            targetRotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
        }
        else if (m_direction == -1)
        {
            targetRotation = Quaternion.Euler(0.0f, 270.0f, 0.0f);
        }
        m_rotationTransform.localRotation = Quaternion.RotateTowards(m_rotationTransform.localRotation, targetRotation, m_rotateSpeed * Time.deltaTime);

        double beatLookahead = m_musicManager.SecondsToBeat(0.5);
        double futureTime = m_musicManager.GetCurrentBeat() + beatLookahead;
        int latestBeat = (int)MidiUtil.RoundBeatToLatestInterval(futureTime, 1.0);
        if (latestBeat > m_lastBeat)
        {
            m_audioSource.clip = m_footstepClips[latestBeat % m_footstepClips.Length];
            m_audioSource.PlayScheduled(m_musicManager.GetDSPScheduleTime(latestBeat));
            m_lastBeat = latestBeat;
        }
        m_audioSource.volume = m_startVolume * m_musicManager.GetTrackVolume(0); // Link it to music volume, so a dimming will dim this too
    }
}
