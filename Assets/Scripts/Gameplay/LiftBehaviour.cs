﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftBehaviour : TileObject, StateHolder<LiftBehaviour.State>
{
    MusicManager m_musicManager;
    double m_lastMoveBeat;

    public int[] m_stops;

    [Serializable]
    public class State
    {
        public bool m_activated;
        public int m_liftPosition;
        public int m_lastStop;
        public int m_nextStop = 1;
    }

    public void SetState(State s)
    {
        m_state = s;
    }

    public State GetState()
    {
        return m_state;
    }

    [NonSerialized, HideInInspector]
    public State m_state;

    PlayerControl m_player;

	// Use this for initialization
	void Start ()
    {
        m_musicManager = FindObjectOfType<MusicManager>();
        m_player = FindObjectOfType<PlayerControl>();

        m_lastMoveBeat = -1.0;
        if (m_state == null)
        {
            m_state = new State();
        }
        else
        {
            Debug.Log("Lift starting loaded");
            transform.position = transform.position + new Vector3(0.0f, m_state.m_liftPosition, 0.0f);
        }
	}

    public override void HandleCustomProperties(IDictionary<string, string> props)
    {
        Debug.Log("Handling custom lift properties");
        List<int> stopList = new List<int>();
        for (int i = 0; i < 10; i++)
        {
            if (props.ContainsKey("Stop" + i))
            {
                int stopValue = 0;
                bool parsed = int.TryParse(props["Stop" + i], out stopValue);
                if (parsed)
                {
                    stopList.Add(stopValue);
                }
            }
        }
        m_stops = stopList.ToArray();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (m_state.m_activated)
        {
            int latestBeat = m_musicManager.GetLatestBeatID(-0.5); // We want the moves to be on half-beats
            if (latestBeat > m_lastMoveBeat)
            {
                m_lastMoveBeat = latestBeat;
                MoveTowardsNextStop();
            }
        }
	}

    void MoveTowardsNextStop()
    {
        if(m_stops == null || m_stops.Length == 0)
        {
            Debug.Log("A lift has no stops");
            return;
        }

        int lastStopY = m_stops[m_state.m_lastStop];
        int nextStopY = m_stops[m_state.m_nextStop];
        if (nextStopY != m_state.m_liftPosition)
        {
            int toNextStopY = nextStopY - lastStopY;
            int liftDirection = (int)Mathf.Sign(toNextStopY);
            ActuallyMove(liftDirection);
        }
        else
        {
            // By not moving this beat, it looks like we're stopping for a beat

            // Last stop is where we just arrived
            m_state.m_lastStop = m_state.m_nextStop;

            // Next stop is in id direction (unless we reached the end of the list, in which case, loop)
            int nextStopID = (m_state.m_lastStop + 1) % m_stops.Length;
            m_state.m_nextStop = nextStopID;
        }
    }

    void ActuallyMove(int liftDirection)
    {
        m_state.m_liftPosition += liftDirection;
        // Detect if the player in 1 tile above the lift, and if so, move it as well
        Vector3 playerPositionDownATile = m_player.GetPlayerMidPoint() - new Vector3(0.0f, 1.0f, 0.0f);
        if (IsOnTileWith(playerPositionDownATile))
        {
            m_player.Move(new Vector3(0.0f, liftDirection, 0.0f), true);
            if (liftDirection > 0)
            {
                m_player.SetWallState(PlayerControl.WallState.Off); // Detach the player from the wall if the lift pushes them up
            }
        }
        else if (m_player.GetWallState() == PlayerControl.WallState.Hanging) // Move the player with the lift if they are hanging onto the lift
        {
            Vector3 playerPositionHangingTile = m_player.GetPlayerMidPoint() + new Vector3(m_player.GetLastDirection(), 0.0f, 0.0f);
            if (IsOnTileWith(playerPositionHangingTile))
            {
                // Check to see if player can move in lift direction
                RaycastHit2D wouldHitFloorOrCeiling = m_player.DebuggableRaycast(m_player.GetPlayerPosition(), new Vector2(0.0f, liftDirection));
                if (wouldHitFloorOrCeiling.collider != null)
                {
                    m_player.SetWallState(PlayerControl.WallState.Off);
                }
                else
                {
                    m_player.Move(new Vector3(0.0f, liftDirection, 0.0f), true);
                }
            }
        }
        transform.position += new Vector3(0.0f, liftDirection, 0.0f);
    }

    public void StartLift()
    {
        m_state.m_activated = true;
        // When it activates, it should start on the next beat
        int latestBeat = m_musicManager.GetLatestBeatID(-0.5); // We want the moves to be on half-beats
        m_lastMoveBeat = latestBeat;
    }
}
