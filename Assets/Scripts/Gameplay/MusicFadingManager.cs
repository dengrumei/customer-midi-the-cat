﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicFadingManager : MonoBehaviour {

    PlayerControl m_player;
    SneakBehaviour m_sneakBehaviour;
    MusicManager m_musicManager;

    private void Awake()
    {
        m_player = FindObjectOfType<PlayerControl>();
        m_sneakBehaviour = m_player.GetComponent<SneakBehaviour>();
        m_musicManager = GetComponent<MusicManager>();
    }
    private void Start()
    {
        m_musicManager.MuteAllTracks();
    }

    void Update ()
    {
        // Things that can modify music
        // Cutscenes, Conversations, Sneaking, (Initial movement?)
        if (!m_player.gameObject.activeSelf || m_player.GetIgnoreWhiteInputs())
        {
            m_musicManager.SetTrackVolume(0, 0.0f, 8.0f);
            m_musicManager.SetTrackVolume(1, 0.0f, 8.0f);
            m_musicManager.SetTrackVolume(2, 0.0f, 8.0f);
            m_musicManager.SetTrackVolume(3, 0.0f, 8.0f);
            m_musicManager.SetTrackVolume(4, 0.0f, 8.0f);
        }
        else
        {
            float sneakFade = m_sneakBehaviour.GetSneakFade();
            m_musicManager.SetTrackVolume(0, 1.0f - sneakFade, 1.0f);
            m_musicManager.SetTrackVolume(1, m_player.GetState().m_stepState != PlayerControl.StepState.Off ? 1.0f : 0.0f, 1.0f);
            m_musicManager.SetTrackVolume(2, m_player.GetNoteHistory()[0] > 0 ? (1.0f - sneakFade) : 0.0f, 1.0f);
            m_musicManager.SetTrackVolume(4, sneakFade, 1.0f);
        }
	}
}
