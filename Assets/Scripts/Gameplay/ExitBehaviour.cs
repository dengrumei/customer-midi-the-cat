﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitBehaviour : TileObject
{
    PlayerControl m_player;
    MusicManager m_musicManager;

    static int s_previousLevel = -1;

    public bool m_startLocked;
    public int m_targetLevel;

    bool m_spawnedHere;
    int m_lastBeatHere;
    int m_beatWhenMovedOffSpawn;

    void Start()
    {
        m_player = FindObjectOfType<PlayerControl>();
        m_musicManager = FindObjectOfType<MusicManager>();

        m_lastBeatHere = -1;
        m_spawnedHere = IsOnTileWith(m_player.GetPlayerMidPoint());

        if (s_previousLevel == m_targetLevel)
        {
            // Spawn the player here
            Vector3 spawnPos = m_midpointChild.position + new Vector3(0.0f, -0.5f, 0.0f);
            m_player.transform.position = spawnPos;
            MoveStorage moveStorage = m_player.GetComponent<MoveStorage>();
            MoveStorage.Step firstStep = new MoveStorage.Step(spawnPos, spawnPos, MoveStorage.MoveType.Walk);
            firstStep.m_direction = m_player.GetState().m_lastDirection;
            moveStorage.AddStep(firstStep);
            moveStorage.SetMovementTime(-5.0, -5.0, false);
            CatGraphicsControl catGraphics = FindObjectOfType<CatGraphicsControl>();
            catGraphics.SetPosition(spawnPos);
            m_spawnedHere = true;
            s_previousLevel = -1;
            SaveGameSystem.SaveCurrentSaveGame();
        }
    }

    public override void HandleCustomProperties(IDictionary<string, string> props)
    {
        Debug.Log("Handling custom Exit properties");

        if (props.ContainsKey("TargetLevel"))
        {
            int targetLevel = 0;
            bool parsed = int.TryParse(props["TargetLevel"], out targetLevel);
            if (parsed)
            {
                m_targetLevel = targetLevel;
            }
        }

        if (props.ContainsKey("StartLocked"))
        {
            bool startLocked = false;
            bool parsed = bool.TryParse(props["StartLocked"], out startLocked);
            if (parsed)
            {
                m_startLocked = startLocked;
            }
        }
    }

    void Update()
    {
        Vector3 playerCenter = m_player.GetPlayerMidPoint();
        if (IsOnTileWith(playerCenter))
        {
            if (m_spawnedHere)
            {
                return;
            }

            double currentBeatTime = m_musicManager.GetCurrentBeat();
            int mostRecentBeat = (int)currentBeatTime;
            if (mostRecentBeat <= m_beatWhenMovedOffSpawn + 1) // If moved back quickly, then reset flag
            {
                m_spawnedHere = true;
                return;
            }

            if (mostRecentBeat > m_lastBeatHere + 3)
            {
                m_lastBeatHere = mostRecentBeat;
            }
            else if (mostRecentBeat == m_lastBeatHere + 3)
            {
                // Save the state of the current level before transitioning
                SaveGameSystem.SaveCurrentSaveGame();
                SaveGameSystem.SetLevelTransition(m_targetLevel);

                s_previousLevel = SceneManager.GetActiveScene().buildIndex;
                AutoFade.LoadLevel(m_targetLevel, 1.0f, 1.0f, Color.black);
                m_player.gameObject.SetActive(false);
                this.enabled = false;
            }
        }
        else
        {
            if (m_spawnedHere)
            {
                m_beatWhenMovedOffSpawn = (int)m_musicManager.GetCurrentBeat();
                m_spawnedHere = false;
            }
        }
    }
}
