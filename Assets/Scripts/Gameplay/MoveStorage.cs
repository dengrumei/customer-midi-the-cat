﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveStorage : MonoBehaviour
{
    public enum MoveType
    {
        Walk,
        WalkRightUpwards,
        WalkRightDownwards,
        Jump,
        JumpIntoCeiling,
        JumpToWall,
        Fall,
        Climb,
        WallGrab,
        WallRelease,
        Turn
    }

    [Serializable]
    public class Step
    {
        public Vector2 m_startPosition;
        public Vector2 m_endPosition;
        public double m_startBeat;
        public double m_endBeat;
        public MoveType m_moveType;
        public int m_direction;

        public Step(Vector2 startPosition, Vector2 endPosition, MoveType moveType)
        {
            m_startPosition = startPosition;
            m_endPosition = endPosition;
            m_startBeat = 0;
            m_endBeat = 0;
            m_moveType = moveType;

            Vector2 movement = endPosition - startPosition;
            m_direction = 0;
            if (movement.x > 0)
            {
                m_direction = 1;
            }
            else if (movement.x < 0)
            {
                m_direction = -1;
            }
        }

        public Vector2 GetLerpedPosition(double currentBeatTime)
        {
            if (m_endBeat == m_startBeat)
            {
                return m_endPosition;
            }
            
            return Vector2.Lerp(m_startPosition, m_endPosition, GetT(currentBeatTime)); // Lerp will clamp t to 0 and 1
        }
        public float GetT(double currentBeatTime)
        {
            float t = (float)((currentBeatTime - m_startBeat) / (m_endBeat - m_startBeat));
            return t;
        }
    }


    List<Step> m_newSteps;
    List<Step> m_steps;
    List<Step> m_toRemove;

    MusicManager m_musicManager;

    void Awake()
    {
        m_newSteps = new List<Step>();
        m_steps = new List<Step>();
        m_toRemove = new List<Step>();

        m_musicManager = FindObjectOfType<MusicManager>();
    }

    void Update()
    {
        double currentBeatTime = m_musicManager.GetCurrentBeat();
        RemoveOldSteps(currentBeatTime);
#if UNITY_EDITOR
        RenderDebug(currentBeatTime);
#endif
    }

    void RenderDebug(double currentBeatTime)
    {
        for (int i = 0; i < m_steps.Count; i++)
        {
            Step step = m_steps[i];
            bool current = step.m_startBeat <= currentBeatTime && step.m_endBeat >= currentBeatTime;
            Color colour = (current ? Color.green : Color.yellow);

            Debug.DrawLine(step.m_startPosition, step.m_endPosition, colour);

            double timeToStart = step.m_startBeat - currentBeatTime;
            double timeToEnd = step.m_endBeat - currentBeatTime;
            Vector2 timeDebugVector = -Utils.Rotate90Clockwise(step.m_endPosition - step.m_startPosition).normalized;
            Debug.DrawLine(step.m_startPosition, step.m_startPosition + timeDebugVector * (float)timeToStart, Color.black);
            Debug.DrawLine(step.m_endPosition, step.m_endPosition + timeDebugVector * (float)timeToEnd, Color.blue);
        }
    }

    void RemoveOldSteps(double currentBeatTime)
    {
        Step latestStep = GetLatestStep(currentBeatTime);
        for (int i = 0; i < m_steps.Count; i++)
        {
            Step step = m_steps[i];
            if (step.m_endBeat < currentBeatTime && step != latestStep)
            {
                m_toRemove.Add(step);
            }
        }

        foreach (Step toRemove in m_toRemove)
        {
            m_steps.Remove(toRemove);
        }
        m_toRemove.Clear();
    }

    public void AddStep(Step step)
    {
        m_newSteps.Add(step);
        //Debug.Log("Adding step from " + step.m_startPosition + " to " + step.m_endPosition);
    }

    public void SetMovementTime(double currentBeatTime, double arrivalTime, bool interrupt = false)
    {
        // Either cut old notes to be responsive, or delay new ones...
        if (interrupt)
        {
            Step latestStep = GetLatestStep(currentBeatTime);
            if (latestStep != null && m_newSteps.Count > 0)
            {
                Vector2 currentPosition = latestStep.GetLerpedPosition(currentBeatTime);
                Vector2 difference = currentPosition - m_newSteps[0].m_startPosition;
                // Keep time separation, but shift position points to fill the position gap
                for (int i = 0; i < m_newSteps.Count; i++)
                {
                    float startAlong = i / (float)m_newSteps.Count;
                    float endAlong = (i + 1) / (float)m_newSteps.Count;
                    float startShiftScale = 1.0f - startAlong;
                    float endShiftScale = 1.0f - endAlong;
                    m_newSteps[i].m_startPosition += difference * startShiftScale;
                    m_newSteps[i].m_endPosition += difference * endShiftScale;
                }
                m_steps.Clear();
            }
        }
        else
        {
            Step lastStep = GetLastStep();
            if (lastStep != null && lastStep.m_endBeat > currentBeatTime) // Delay until the last one would finish
            {
                currentBeatTime = lastStep.m_endBeat;
            }
        }

        double lastEndBeat = currentBeatTime;
        for (int i = 0; i < m_newSteps.Count; i++)
        {
            Step step = m_newSteps[i];
            step.m_startBeat = lastEndBeat;
            double endProportion = (i + 1.0) / m_newSteps.Count;
            step.m_endBeat = Utils.Lerp(currentBeatTime, arrivalTime, endProportion);
            lastEndBeat = step.m_endBeat;
            //Debug.Log("Step from " + step.m_startBeat + ":" + step.m_startPosition + " to " + step.m_endBeat + ":" + step.m_endPosition);
            m_steps.Add(step);
        }
        m_newSteps.Clear();
    }

    public List<Step> GetStepList()
    {
        return m_steps;
    }

    public Step GetLastStep()
    {
        Step lastStep = null;
        double lastEndTime = double.MinValue;
        for (int i = 0; i < m_steps.Count; i++)
        {
            Step step = m_steps[i];
            if (step.m_endBeat > lastEndTime)
            {
                lastEndTime = step.m_endBeat;
                lastStep = step;
            }
        }
        return lastStep;
    }

    public Step GetLatestStep(double currentBeatTime)
    {
        Step latestStep = null;
        double closestTimeSinceEnd = double.MaxValue;
        for (int i = 0; i < m_steps.Count; i++)
        {
            Step step = m_steps[i];
            //Debug.Log("Step times from " + step.m_startBeat + " to " + step.m_endBeat);
            if (currentBeatTime >= step.m_startBeat && currentBeatTime <= step.m_endBeat)
            {
                latestStep = step;
                break;
            }
            else
            {
                double timeSinceEnd = currentBeatTime - step.m_endBeat;
                if (timeSinceEnd > 0.0 && timeSinceEnd < closestTimeSinceEnd)
                {
                    latestStep = step;
                    closestTimeSinceEnd = timeSinceEnd;
                }
            }
        }
        return latestStep;
    }

    public void ApplyInstantStepShift(double beatTime, Vector2 movement, MoveType? moveTypeChange = null)
    {
        if (m_newSteps.Count > 0)
        {
            Debug.LogError("We shouldn't be applying instant step shift while adding new moves!");
            return;
        }
        int stepCount = m_steps.Count;
        bool changedAnything = false;
        for(int i = 0; i < stepCount; i++)
        {
            Step step = m_steps[i];
            if (beatTime >= step.m_startBeat && beatTime <= step.m_endBeat)
            {
                //Split up move
                Vector2 splitPosition = step.GetLerpedPosition(beatTime);
                Step secondHalf = new Step(splitPosition + movement, step.m_endPosition + movement, (moveTypeChange != null ? moveTypeChange.Value : step.m_moveType));
                secondHalf.m_startBeat = beatTime;
                secondHalf.m_endBeat = step.m_endBeat;
                secondHalf.m_direction = step.m_direction;
                m_steps.Add(secondHalf);

                // Adjust existing step
                step.m_endPosition = splitPosition;
                step.m_endBeat = beatTime;
                changedAnything = true;
            }
            else if (beatTime < step.m_startBeat)
            {
                step.m_startPosition += movement;
                step.m_endPosition += movement;
                if (moveTypeChange != null)
                {
                    step.m_moveType = moveTypeChange.Value;
                }
                changedAnything = true;
            }
            else if (beatTime > step.m_endBeat)
            {
                // Only adjust steps that end AFTER the change
            }
        }
        if (!changedAnything)
        {
            // Then all moves must be BEFORE the change, so just adjust the latest one
            Step latest = GetLatestStep(beatTime);
            latest.m_endPosition += movement;
            if (moveTypeChange != null)
            {
                latest.m_moveType = moveTypeChange.Value;
            }
        }
    }

    public bool HasPendingMoves(double currentBeatTime)
    {
        for (int i = 0; i < m_steps.Count; i++)
        {
            if (m_steps[i].m_endBeat > currentBeatTime)
            {
                return true;
            }
        }
        return false;
    }
}
