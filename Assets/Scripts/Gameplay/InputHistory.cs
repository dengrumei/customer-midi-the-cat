﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularBuffer<T>
{
    // Most recent inputs are at the front of the array
    public T[] m_history;

    public CircularBuffer(int size)
    {
        m_history = new T[size];
    }

    public void Add(T key)
    {
        for(int i = m_history.Length - 1; i > 0; i--)
        {
            m_history[i] = m_history[i - 1];
        }
        m_history[0] = key;
    }
}

public struct InputEvent
{
    public int m_noteID;
    public bool m_down;
    public double m_beatTime;

    public InputEvent(int noteID, bool down, double beatTime)
    {
        m_noteID = noteID;
        m_down = down;
        m_beatTime = beatTime;
    }
}
