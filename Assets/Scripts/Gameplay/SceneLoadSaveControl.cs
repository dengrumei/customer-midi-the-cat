﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoadSaveControl : MonoBehaviour
{
    MidiInput m_midiInput;

	void Awake ()
	{
        SaveGame saveGame = SaveGameSystem.s_currentSaveGame;
        if (saveGame != null && saveGame.m_levelID == SaveGameSystem.GetCurrentLevelID())
        {
            saveGame.Load(SaveGameSystem.GetCurrentLevelID());
            GameObject[] introObjects = GameObject.FindGameObjectsWithTag("LevelIntro");
            foreach(GameObject toRemove in introObjects)
            {
                Destroy(toRemove);
            }
        }
    }

    void Start()
    {
        m_midiInput = FindObjectOfType<MidiInput>();
        m_midiInput.MidiIOEvent += HandleMidiIOEvent;
    }

    private void HandleMidiIOEvent(int note, bool down, float velocity)
    {

    }

    void Update()
    {
        // Debug quicksave and quickload
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.F5))
        {
            Debug.Log("Quicksaving!");
            bool saved = SaveGameSystem.SaveCurrentSaveGame();
            Debug.Log("Saved = " + saved);
        }
        if (Input.GetKeyDown(KeyCode.F9))
        {
            Debug.Log("Quickloading!");
            bool loaded = SaveGameSystem.LoadCurrentSaveGame();
            Debug.Log("Loaded = " + loaded);
        }
#endif
        if (Input.GetKey(KeyCode.F3))
        {
            Debug.Log("Resetting game");
            AutoFade.LoadLevel(0, 1.0f, 1.0f, Color.black);
        }
    }
}
