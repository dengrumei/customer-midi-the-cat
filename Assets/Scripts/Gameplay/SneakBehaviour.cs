﻿using Light2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SneakBehaviour : MonoBehaviour
{
    PlayerControl m_player;
    CatGraphicsControl m_catGraphics;
    CatTailControl m_catTail;
    AudioLowPassFilter m_lowPassFilter;
    MusicManager m_musicManager;

    LightingSystem m_lightSystem;
    LightSprite[] m_lightSprites;
    LightSprite m_catLight;
    EnemyBehaviour[] m_enemies;
    RaycastHit2D[] m_raycastHits;

    Color m_startCatLightColour;
    float m_startCatTailRotationAmount;
    float m_startLowPassFrequency;
    public float m_onSneakLowPassFrequncy = 1024.0f;

    float m_sneakFade;
    public float m_beatsToWaitBeforeSneaking = 2.0f;
    public float m_beatsToStartSneaking = 1.0f;
    public float m_beatsToStopSneaking = 0.5f;
    public float m_sneakYDistance = 1.0f;
    public float m_sneakDistance = 15.0f;

    public bool IsSneaking()
    {
        return m_sneakFade > 0.5f;
    }

    public float GetSneakFade()
    {
        return m_sneakFade;
    }

	void Start ()
	{
        m_player = GetComponent<PlayerControl>();
        m_catGraphics = FindObjectOfType<CatGraphicsControl>();
        m_lowPassFilter = FindObjectOfType<AudioLowPassFilter>();
        m_catTail = FindObjectOfType<CatTailControl>();
        m_musicManager = FindObjectOfType<MusicManager>();
        m_lightSystem = FindObjectOfType<LightingSystem>();
        m_enemies = FindObjectsOfType<EnemyBehaviour>();

        m_raycastHits = new RaycastHit2D[16];
        m_startCatTailRotationAmount = m_catTail.m_rotationAmount;
        m_startLowPassFrequency = m_lowPassFilter.cutoffFrequency;

        int playerLayer = LayerMask.NameToLayer("Player");
        LightSprite[] allLights = FindObjectsOfType<LightSprite>();
        List<LightSprite> illuminatingLights = new List<LightSprite>();
        foreach (LightSprite light in allLights)
        {
            if (light.gameObject.layer != playerLayer)
            {
                illuminatingLights.Add(light);
            }
            else
            {
                m_catLight = light; // Assuming a single cat light
                m_startCatLightColour = m_catLight.Color;
            }
        }
        m_lightSprites = illuminatingLights.ToArray();
	}

    bool InRangeOfEnemies()
    {
        float sneakYDistance = m_sneakYDistance;
        float sneakDistance = m_sneakDistance;
        float sneakDistanceSqrd = sneakDistance * sneakDistance;
        for (int i = 0; i < m_enemies.Length; i++)
        {
            EnemyBehaviour enemy = m_enemies[i];
            if (Mathf.Abs(enemy.transform.position.y - transform.position.y) <= sneakYDistance && Vector3.SqrMagnitude(enemy.transform.position - transform.position) < sneakDistanceSqrd)
            {
                return true;
            }
        }
        return false;
    }

    bool InAnyLight()
    {
        for (int i = 0; i < m_lightSprites.Length; i++)
        {
            LightSprite light = m_lightSprites[i];
            // Project player position into light space to see if we're inside the light
            Vector3 playerPoint = m_player.GetPlayerMidPoint();
            Vector3 localPoint = light.transform.InverseTransformPoint(playerPoint);
            //Debug.DrawLine(light.transform.position, light.transform.position + localPoint, Color.cyan);
            bool inLight = localPoint.x > -0.5 && localPoint.x < 0.5 && localPoint.y > 0 && localPoint.y < 2;

            if (inLight)
            {
                Vector2 toPlayer = playerPoint - light.transform.position;
                int result = Physics2D.RaycastNonAlloc(light.transform.position, toPlayer, m_raycastHits, toPlayer.magnitude, MovementUtils.s_obstaclesAndStepsMask);
                if (result <= 0)
                {
                    Debug.DrawLine(light.transform.position, playerPoint, Color.yellow);
                    return true;
                }
            }
        }
        return false;
    }
	
	void Update ()
	{
        if (m_lightSystem == null || !m_lightSystem.enabled)
        {
            return;
        }
        bool sneaking = false;

        // We'll let the player sneak if enemies are near (or they are already in sneak mode)
        bool canSneak = (InRangeOfEnemies() || m_sneakFade > 0.0f) && !InAnyLight();
        m_player.GetState().m_canSneak = canSneak;
        if (canSneak)
        { 
            double[] noteHistory = m_player.GetNoteHistory();
            double currentBeat = m_musicManager.GetCurrentBeat();
            double sneakWait = m_beatsToWaitBeforeSneaking;
            if (currentBeat >= noteHistory[0] + sneakWait || m_player.GetState().m_sneaking || (noteHistory[0] >= noteHistory[1] + sneakWait))
            {
                sneaking = true;
            }
        }

        if (sneaking)
        {
            m_sneakFade += Time.deltaTime / (float)m_musicManager.BeatToSeconds(m_beatsToStartSneaking);
            if (m_sneakFade > 1.0f)
            {
                m_sneakFade = 1.0f;
            }
        }
        else
        {
            m_sneakFade -= Time.deltaTime / (float)m_musicManager.BeatToSeconds(m_beatsToStopSneaking);
            if (m_sneakFade < 0.0f)
            {
                m_sneakFade = 0.0f;
            }
        }

        m_catGraphics.SetCatColour(Color.Lerp(Color.white, Color.grey, m_sneakFade));
        m_catLight.Color = Color.Lerp(m_startCatLightColour, new Color(0.0f, 0.0f, 0.0f, m_startCatLightColour.a), m_sneakFade);
        m_lowPassFilter.cutoffFrequency = Mathf.Lerp(m_startLowPassFrequency, m_onSneakLowPassFrequncy, m_sneakFade);
        m_lowPassFilter.enabled = m_sneakFade > 0.0f;
        //if (!m_player.GetIgnoreWhiteInputs() && !m_musicManager.IsTrackFading(0))
        //{
        //    m_musicManager.SetTrackVolume(0, 1.0f - m_sneakFade, 0.0f);
        //    m_musicManager.SetTrackVolume(1, 1.0f - m_sneakFade, 0.0f);
        //    m_musicManager.SetTrackVolume(2, 1.0f - m_sneakFade, 0.0f);
        //    m_musicManager.SetTrackVolume(3, 1.0f - m_sneakFade, 0.0f);
        //    m_musicManager.SetTrackVolume(4, m_sneakFade, 0.0f);
        //}
        m_catTail.m_intervalLengthMultiplier = !sneaking ? 1.0f : 2.0f;
        m_catTail.m_rotationAmount = !sneaking ? m_startCatTailRotationAmount : m_startCatTailRotationAmount * 0.5f;
    }
}
