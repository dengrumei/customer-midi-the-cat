﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CogTileBehaviour : MonoBehaviour
{
    MusicManager m_musicManager;

    BoxCollider2D m_collider;
    Transform m_toRotate;
    AudioSource m_audioSource;
    float m_startVolume;

    public AudioClip m_lineupClip;


    public float m_sequenceLength = 4.0f;
    public float m_beatOffset = 0.0f;
    public float m_beatLength = 1.0f;

    double m_lastSoundTime = -10;

	void Start ()
	{
        m_musicManager = FindObjectOfType<MusicManager>();
        m_collider = GetComponent<BoxCollider2D>();
        m_toRotate = transform.GetChild(0);
        m_audioSource = GetComponent<AudioSource>();
        m_startVolume = m_audioSource.volume;
	}
	
	void Update ()
	{
        double currentBeatTime = m_musicManager.GetCurrentBeat();

        double lastLineUpTime = MidiUtil.RoundBeatToLatestInterval(currentBeatTime - m_beatOffset, m_sequenceLength) + m_beatOffset;
        double lastLineUpEndTime = lastLineUpTime + m_beatLength;
        double beatsSinceLastLineUp = currentBeatTime - lastLineUpEndTime;
        if (beatsSinceLastLineUp <= 0)
        {
            m_toRotate.localRotation = Quaternion.Euler(Vector3.zero);
            m_collider.enabled = true;
        }
        else
        {
            double progressToNextLiningUp = (currentBeatTime - lastLineUpEndTime) / (m_sequenceLength - m_beatLength);
            m_toRotate.localRotation = Quaternion.Euler(0.0f, 0.0f, 360.0f * (float)progressToNextLiningUp);
            m_collider.enabled = false;
        }

        double beatLookahead = m_musicManager.SecondsToBeat(0.5);
        double futureTime = currentBeatTime + beatLookahead;
        double nextLineupTime = lastLineUpTime + m_sequenceLength;
        if (futureTime > nextLineupTime && nextLineupTime > m_lastSoundTime)
        {
            m_audioSource.clip = m_lineupClip;
            m_audioSource.PlayScheduled(m_musicManager.GetDSPScheduleTime(nextLineupTime));
            m_lastSoundTime = nextLineupTime;
        }
        m_audioSource.volume = m_startVolume * m_musicManager.GetTrackVolume(0); // Link it to music volume, so a dimming will dim this too
    }
}
