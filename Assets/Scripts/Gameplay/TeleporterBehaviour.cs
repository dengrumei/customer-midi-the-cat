﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterBehaviour : TileObject
{
    public int m_teleporterID;

    bool m_alreadyInRange;

    PlayerControl m_player;
    CatGraphicsControl m_catGraphics;

    float m_teleportAnimationLength = 2.0f;
    float m_teleportAnimationProgress;

    TeleporterBehaviour m_teleportDestination;

    ParticleSystem m_particleSystem;

    public AudioClip m_teleportSound;
    public ColourSetConfig m_noteColoursFromA;
    public bool m_cancelable = true;

    void Start()
    {
        m_player = FindObjectOfType<PlayerControl>();
        m_catGraphics = FindObjectOfType<CatGraphicsControl>();
        m_particleSystem = GetComponentInChildren<ParticleSystem>();
        ParticleSystem.ColorOverLifetimeModule colourModule = m_particleSystem.colorOverLifetime;

        int note = MidiUtil.MajorScaleNoteToNoteOffset(m_teleporterID);
        int notesFromA = MidiUtil.NotesFromA(note) % 12;
        Color noteColour = m_noteColoursFromA.m_colours[notesFromA];

        Color startColour = noteColour;
        Color endColour = noteColour;
        Gradient grad = new Gradient();
        grad.SetKeys(new GradientColorKey[] { new GradientColorKey(startColour, 0.0f), new GradientColorKey(endColour, 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(0.0f, 0.0f), new GradientAlphaKey(1.0f, 1.0f) });
        colourModule.color = grad;
    }

    void Update()
    {
        if (m_teleportDestination != null)
        {
            m_teleportAnimationProgress += Time.deltaTime;
            ParticleSystem.MainModule main = m_particleSystem.main;
            main.gravityModifierMultiplier = m_teleportAnimationProgress * -1.0f / m_teleportAnimationLength;
            m_catGraphics.transform.localScale = new Vector3(1.0f - m_teleportAnimationProgress / m_teleportAnimationLength, 1.0f, 1.0f);

            if (m_teleportAnimationProgress >= m_teleportAnimationLength)
            {
                FinishTeleport();
            }
        }
        else
        {
            bool inRange = IsOnTileWith(m_player.GetPlayerMidPoint());
            if (inRange && !m_alreadyInRange)
            {
                TeleporterPanelBehaviour.Show(this);
            }

            if (!inRange && m_alreadyInRange)
            {
                TeleporterPanelBehaviour.Hide();
            }
            m_alreadyInRange = inRange;
        }
    }

    public void TeleportToDestination(TeleporterBehaviour destination)
    {
        m_teleportDestination = destination;
        m_teleportAnimationProgress = 0.0f;
        MidiUtil.PlayClipAtPoint(m_teleportSound, transform.position, 0.5f, 1.4f);
    }

    void FinishTeleport()
    {
        Vector3 toDestination = m_teleportDestination.transform.position - transform.position;
        m_player.Move(toDestination, true, MoveStorage.MoveType.Walk, m_player.GetState().m_lastDirection);
        m_player.SetIgnoreWhiteInputs(false);

        ParticleSystem.MainModule main = m_particleSystem.main;
        main.gravityModifierMultiplier = 0.0f;
        m_catGraphics.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        m_catGraphics.transform.position = m_teleportDestination.transform.position;

        m_teleportDestination.m_alreadyInRange = true; // So the menu doesn't just pop up instantly on the other end
        m_teleportDestination = null;
        m_alreadyInRange = false;
    }
}
