﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/** A base-class for all Tiled2Unity objects, so I can store their mid point transforms */
public class TileObject : MonoBehaviour
{
    protected Transform m_midpointChild;

    public virtual void HandleCustomProperties(IDictionary<string, string> props)
    {
        Debug.Log("Base TileObject HandleCustomProperties");
    }

    public Vector3 GetMidPosition()
    {
        return m_midpointChild.transform.position;
    }

    public bool IsOnTileWith(Vector3 position)
    {
        float sqrdDistance = Vector3.SqrMagnitude(position - GetMidPosition());
        return sqrdDistance <= 0.01f;
    }

    public bool IsOnTileWithOrHasCrossed(Vector3 position, Vector3 lastPostion)
    {
        Vector3 midPosition = GetMidPosition();
        bool isOnTile = IsOnTileWith(position);
        bool isNewlyOnTile = (isOnTile && !IsOnTileWith(lastPostion));
        bool crossedX = (lastPostion.x < midPosition.x && position.x > midPosition.x)
                     || (lastPostion.x > midPosition.x && position.x < midPosition.x);
        bool atSameHeight = Mathf.Abs(midPosition.y - lastPostion.y) < 0.5f && Mathf.Abs(midPosition.y - position.y) < 0.5f;
        return isNewlyOnTile || (crossedX && atSameHeight);
    }

    public bool IsInRangeOf(Vector3 position, float range)
    {
        float rangeSqrd = range * range;
        return Vector3.SqrMagnitude(position - m_midpointChild.transform.position) <= rangeSqrd;
    }

    protected void Awake()
    {
        m_midpointChild = (transform.childCount == 0 ? transform : transform.GetChild(0));
    }
}
