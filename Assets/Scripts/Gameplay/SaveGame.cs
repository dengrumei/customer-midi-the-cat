﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

[Serializable]
public class DestroyableState<T, AddressType>
{
    public AddressType m_address;
    public T m_savedState;
}

[Serializable]
public class LevelData
{
    public int m_levelID;
    public LiftBehaviour.State[] m_liftStates;
    public ButtonBehaviour.State[] m_buttonStates;
    public NPCBehaviour.State[] m_npcStates;
    public DestroyableState<CollectableBehaviour.State, Vector3>[] m_collectableStates;

    public void Save()
    {
        SaveStateList<LiftBehaviour.State, LiftBehaviour>(ref m_liftStates);
        SaveStateList<ButtonBehaviour.State, ButtonBehaviour>(ref m_buttonStates);
        SaveStateList<NPCBehaviour.State, NPCBehaviour>(ref m_npcStates);
        SaveDestroyableStateList<CollectableBehaviour.State, CollectableBehaviour, Vector3>(ref m_collectableStates);
    }

    public void Load()
    {
        LoadStateList<LiftBehaviour.State, LiftBehaviour>(m_liftStates);
        LoadStateList<ButtonBehaviour.State, ButtonBehaviour>(m_buttonStates);
        LoadStateList<NPCBehaviour.State, NPCBehaviour>(m_npcStates);
        LoadDestroyableStateList<CollectableBehaviour.State, CollectableBehaviour, Vector3>(m_collectableStates);
    }

    void SaveStateList<StateT, StateHolderT>(ref StateT[] saveTarget) where StateHolderT : Component, StateHolder<StateT>
    {
        StateHolderT[] objectsInLevel = GameObject.FindObjectsOfType<StateHolderT>();
        saveTarget = new StateT[objectsInLevel.Length];
        for (int i = 0; i < saveTarget.Length; i++)
        {
            saveTarget[i] = objectsInLevel[i].GetState();
        }
        Debug.Log("Saving " + saveTarget.Length + " " + typeof(StateT).ToString() + " objects");
    }

    void LoadStateList<StateT, StateHolderT>(StateT[] savedStates) where StateHolderT : Component, StateHolder<StateT>
    {
        if (savedStates == null)
        {
            Debug.Log("Missing state list in level save");
            return;
        }
        StateHolderT[] objectsInLevel = GameObject.FindObjectsOfType<StateHolderT>();
        if (savedStates.Length != objectsInLevel.Length)
        {
            // The level must have changed...
            Debug.Log("Saved object count doesn't match level object count");
            return;
        }
        for (int i = 0; i < savedStates.Length; i++)
        {
            objectsInLevel[i].SetState(savedStates[i]);
        }
        Debug.Log("Loading " + savedStates.Length + " " + typeof(StateT).ToString() + " objects");
    }

    void SaveDestroyableStateList<StateT, StateHolderT, AddressType>(ref DestroyableState<StateT, AddressType>[] saveTarget) where StateHolderT : Component, DestroyableStateHolder<StateT, AddressType>
    {
        StateHolderT[] objectsInLevel = GameObject.FindObjectsOfType<StateHolderT>();
        saveTarget = new DestroyableState<StateT, AddressType>[objectsInLevel.Length];
        for (int i = 0; i < saveTarget.Length; i++)
        {
            saveTarget[i] = new DestroyableState<StateT, AddressType>();
            saveTarget[i].m_address = objectsInLevel[i].GetAddress();
            saveTarget[i].m_savedState = objectsInLevel[i].GetState();
        }
        Debug.Log("Saving " + saveTarget.Length + " " + typeof(StateT).ToString() + " addressed objects");
    }

    void LoadDestroyableStateList<StateT, StateHolderT, AddressType>(DestroyableState<StateT, AddressType>[] savedStates) where StateHolderT : Component, DestroyableStateHolder<StateT, AddressType>
    {
        if (savedStates == null)
        {
            Debug.Log("Missing addressable state list in level save");
            return;
        }
        StateHolderT[] objectsInLevel = GameObject.FindObjectsOfType<StateHolderT>();
        bool[] accountedForObjects = new bool[objectsInLevel.Length];
        for (int i = 0; i < savedStates.Length; i++)
        {
            AddressType address = savedStates[i].m_address;
            bool foundHolder = false;
            for(int o = 0; o < objectsInLevel.Length; o++)
            {
                StateHolderT holder = objectsInLevel[o];
                if (EqualityComparer<AddressType>.Default.Equals(holder.GetAddress(), address))
                {
                    holder.SetState(savedStates[i].m_savedState);
                    foundHolder = true;
                    if (accountedForObjects[o])
                    {
                        Debug.Log("Woops!  We've already accounted for object " + o);
                    }
                    accountedForObjects[o] = true;
                    break;
                }
            }
            if (!foundHolder)
            {
                Debug.Log("LoadAddressableStateList is missing a holder for state at address " + address);
            }
        }

        // Remove any objects not in the save list
        for (int o = 0; o < accountedForObjects.Length; o++)
        {
            if (!accountedForObjects[o])
            {
                GameObject.DestroyImmediate(objectsInLevel[o].gameObject);
            }
        }

        Debug.Log("Loading " + savedStates.Length + " " + typeof(StateT).ToString() + " addressed objects");
    }
}


[Serializable]
public class SaveGame
{
    public static int s_latestVersion = 1;

    public int m_version;
    public int m_saveCount;
    public int m_levelID;
    public PlayerControl.State m_playerState;
    public InventoryBehaviour.State m_inventoryState;
    public TutorialPanelManager.State m_tutorialState;
    public List<LevelData> m_levelData;

    public SaveGame()
    {
        m_version = s_latestVersion;
        m_levelID = 0;
        m_playerState = new PlayerControl.State();
        m_levelData = new List<LevelData>();
        m_inventoryState = new InventoryBehaviour.State();
        m_tutorialState = new TutorialPanelManager.State();
    }

    public void Save(int currentLevelID)
    {
        m_levelID = currentLevelID;
        m_saveCount++;
        Debug.Log("SaveCount = " + m_saveCount);

        PlayerControl player = GameObject.FindObjectOfType<PlayerControl>();
        m_playerState = player.GetState();

        InventoryBehaviour inventory = GameObject.FindObjectOfType<InventoryBehaviour>();
        m_inventoryState = inventory.GetState();

        TutorialPanelManager tutorial = GameObject.FindObjectOfType<TutorialPanelManager>();
        m_tutorialState = tutorial.GetState();

        LevelData level = GetLevelData(currentLevelID);
        level.Save();
    }

    public void Load(int currentLevelID)
    {
        PlayerControl player = GameObject.FindObjectOfType<PlayerControl>();
        player.SetState(m_playerState);

        InventoryBehaviour inventory = GameObject.FindObjectOfType<InventoryBehaviour>();
        inventory.SetState(m_inventoryState);

        TutorialPanelManager tutorial = GameObject.FindObjectOfType<TutorialPanelManager>();
        tutorial.SetState(m_tutorialState);

        Debug.Log("Number of deserialised levels is " + m_levelData.Count);
        LevelData level = GetLevelData(currentLevelID);
        level.Load();
    }

    LevelData GetLevelData(int levelID)
    {
        LevelData level = m_levelData.Find(x => x.m_levelID == levelID);
        if (level == null)
        {
            level = new LevelData();
            level.m_levelID = levelID;
            m_levelData.Add(level);
        }
        return level;
    }
}
