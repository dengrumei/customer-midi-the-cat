﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointBehaviour : TileObject
{
    PlayerControl m_player;
    Canvas m_canvas;
    ParticleSystem m_particles;

    public GameObject m_gameSavedTextPrefab;

    Vector3 m_lastPlayerPosition;
    float m_cooldown;

    void Start()
    {
        m_player = FindObjectOfType<PlayerControl>();
        m_canvas = GameObject.FindWithTag("WorldCanvas").GetComponent<Canvas>();
        m_particles = GetComponentInChildren<ParticleSystem>();
        m_lastPlayerPosition = m_player.GetPlayerMidPoint();
    }

	void Update ()
	{
        Vector3 playerPosition = m_player.GetPlayerMidPoint();

        if (m_cooldown > 0.0f)
        {
            m_cooldown -= Time.deltaTime;
            if (m_cooldown <= 0.0f)
            {
                m_particles.Play();
            }
        }
        else
        {
            if (IsOnTileWithOrHasCrossed(playerPosition, m_lastPlayerPosition))
            {
                PlayerControl.State preJigState = m_player.GetState().Clone();
                m_player.transform.position = GetMidPosition() + new Vector3(0.0f, -0.5f);
                m_player.GetState().m_stepState = PlayerControl.StepState.Off;
                m_player.GetState().m_wallState = PlayerControl.WallState.Off;
                m_player.GetState().m_momentum = 1;
                
                SaveGameSystem.SaveCurrentSaveGame();
                m_player.SetState(preJigState);

                GameObject newText = GameObject.Instantiate(m_gameSavedTextPrefab, m_canvas.transform);
                newText.transform.position = transform.position;

                m_particles.Stop();
                m_cooldown = 5.0f;
            }
        }
        m_lastPlayerPosition = playerPosition;
	}
}
