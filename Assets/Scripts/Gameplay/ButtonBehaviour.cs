﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonBehaviour : TileObject, StateHolder<ButtonBehaviour.State>
{
    public GameObject m_buttonTarget;
    public int[] m_notesWanted;

    [Serializable]
    public class State
    {
        public bool m_triggered;
    }

    State m_state;

    public void SetState(State s)
    {
        m_state = s;
    }

    public State GetState()
    {
        return m_state;
    }

    bool m_alreadyInRange;

    PlayerControl m_player;

    // Use this for initialization
    void Start ()
    {
        m_player = FindObjectOfType<PlayerControl>();
        if (m_state == null)
        {
            m_state = new State();
        }
        else
        {
            Debug.Log("Button starting loaded");
            if (m_state.m_triggered) // So retrigger the button if it loaded triggered
            {
                PushButton();
            }
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!m_state.m_triggered)
        {
            bool inRange = IsOnTileWith(m_player.GetPlayerMidPoint());
            if (inRange && !m_alreadyInRange)
            {
                if (m_notesWanted != null && m_notesWanted.Length > 0)
                {
                    ButtonPanelBehaviour.Show(this);
                }
                else
                {
                    PushButton();
                }
            }

            if (!inRange && m_alreadyInRange)
            {
                ButtonPanelBehaviour.Hide();
            }
            m_alreadyInRange = inRange;
        }
	}

    public void PushButton()
    {
        m_buttonTarget.SendMessage("SetActive", false);
        m_state.m_triggered = true;
    }
}
