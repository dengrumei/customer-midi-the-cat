﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using System.Runtime.Serialization;
using UnityEngine.SceneManagement;

public static class SaveGameSystem
{
    public static string s_currentSaveFile = "Slot1";
    public static SaveGame s_currentSaveGame = null;

    public static bool WriteGameToFile(SaveGame saveGame, string name)
    {
        BinaryFormatter formatter = GetBinaryFormatter();

        bool worked = false;
        Debug.Log("Saving to " + GetSavePath(name));
        using (FileStream stream = new FileStream(GetSavePath(name + "backup"), FileMode.Create))
        {
            try
            {
                formatter.Serialize(stream, saveGame);
                File.Copy(GetSavePath(name + "backup"), GetSavePath(name), true); // Copy the backup to the actual savefile if it succeeded
                Debug.Log("Successfully saved to " + GetSavePath(name));
                worked = true;
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                worked = false;
            }
        }

        if (!worked)
        {
            DeleteSaveGame(name + "backup");
        }

        return worked;
    }

    private static BinaryFormatter GetBinaryFormatter()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        SurrogateSelector ss = new SurrogateSelector();
        ss.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), new Vector3SerializationSurrogate());
        ss.AddSurrogate(typeof(Vector2), new StreamingContext(StreamingContextStates.All), new Vector2SerializationSurrogate());
        formatter.SurrogateSelector = ss;
        return formatter;
    }

    public static bool SaveCurrentSaveGame()
    {
        if (s_currentSaveGame == null)
        {
            Debug.Log("Creating new save game object");
            s_currentSaveGame = new SaveGame();
        }
        s_currentSaveGame.Save(GetCurrentLevelID());
        bool saved = SaveGameSystem.WriteGameToFile(s_currentSaveGame, SaveGameSystem.s_currentSaveFile);
        return saved;
    }

    public static void SetLevelTransition(int targetLevel)
    {
        s_currentSaveGame.m_levelID = targetLevel;
    }

    private static void LoadSaveGame(SaveGame game)
    {
        int levelID = game.m_levelID;
        s_currentSaveGame = game;
        AutoFade.LoadLevel(levelID, 1.0f, 1.0f, Color.black);
    }

    public static int GetCurrentLevelID()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }

    public static bool LoadCurrentSaveGame()
    {
        SaveGameSystem.s_currentSaveGame = new SaveGame();
        if (SaveGameSystem.DoesSaveGameExist(SaveGameSystem.s_currentSaveFile))
        {
            SaveGame saveGame = SaveGameSystem.ReadGameFromFile(SaveGameSystem.s_currentSaveFile);
            if (saveGame != null)
            {
                SaveGameSystem.LoadSaveGame(saveGame);
                return true;
            }
        }
        return false;
    }

    public static SaveGame ReadGameFromFile(string name)
    {
        if (!DoesSaveGameExist(name))
        {
            return null;
        }

        BinaryFormatter formatter = GetBinaryFormatter();

        using (FileStream stream = new FileStream(GetSavePath(name), FileMode.Open))
        {
            try
            {
                return formatter.Deserialize(stream) as SaveGame;
            }
            catch(Exception e)
            {
                Debug.LogError(e.Message);
                return null;
            }
        }
    }

    public static bool DeleteSaveGame(string name)
    {
        try
        {
            File.Delete(GetSavePath(name));
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
            return false;
        }
        return true;
    }

    public static bool DoesSaveGameExist(string name)
    {
        return File.Exists(GetSavePath(name));
    }

    private static string GetSavePath(string name)
    {
        return Path.Combine(Application.persistentDataPath, name + ".sav");
    }
}
