﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicManager : MonoBehaviour
{
    AudioSource[] m_audioSources;
    float[] m_targetVolumes;
    float[] m_fadeTimesInBeats;

    public double m_beatsPerMinute = 100f;
    // Could compute with some left-right bouncy ball configuration menu
    public double m_audioLatency = 0.1f;
    double m_bufferTime = 0.0;
    double m_fftBufferTime = 0.0;
    public AudioMixer m_musicMixer;
    double m_audioStart;
    double m_audioStartBeat;
    double m_tempoMultiplier = 1.0;

    // Smoothing of the DSP time
    double m_lastReportedDSPTime;
    double m_smoothedDSPTime;
    double m_lastLoopTime = 0.0;

    bool m_linearMode = false;
    public double m_targetDSPTime = 0.0;

    private void Awake()
    {
        m_audioSources = GetComponents<AudioSource>();
        m_targetVolumes = new float[m_audioSources.Length];
        m_fadeTimesInBeats = new float[m_audioSources.Length];
        for (int i = 0; i < m_audioSources.Length; i++)
        {
            AudioSource source = m_audioSources[i];
            m_targetVolumes[i] = source.volume;
            m_fadeTimesInBeats[i] = 1.0f;
        }
        m_lastReportedDSPTime = 0.0;
        m_smoothedDSPTime = m_lastReportedDSPTime;

        int bufferLength;
        int numberOfBuffers;
        AudioSettings.GetDSPBufferSize(out bufferLength, out numberOfBuffers);
        int outputRate = AudioSettings.outputSampleRate;
        m_bufferTime = bufferLength / (double)outputRate;

        float FFTSize;
        m_musicMixer.GetFloat("FFT Size", out FFTSize);
        m_fftBufferTime = FFTSize / (double)outputRate;
    }
    void Start()
    {
        if (!m_linearMode)
        {
            ScheduleMusic();
        }
    }

    public void SetPlaybackTargetBeat(double beat)
    {
        if (m_linearMode)
        {
            double targetDSPTime = BeatToSecondsUnadjusted(beat);
            m_targetDSPTime = targetDSPTime;
            for (int i = 0; i < m_audioSources.Length; i++)
            {
                AudioSource source = m_audioSources[i];
                if (source.time > 0)
                {
                    source.UnPause();
                }
                else
                {
                    source.Play();
                }
                //source.SetScheduledEndTime(targetDSPTime);
            }
            //SetMusicSpeed(1.0f);
        }
    }

    public void SetMusicSpeed(float speed)
    {
       /* double beatBefore = GetCurrentBeat();
        m_audioStartBeat = beatBefore;
        
        //m_audioStart = AudioSettings.dspTime;// - bufferTime;
        //double beatBefore = GetCurrentBeat();*/
        m_tempoMultiplier = speed;
        /*
        double beatAfter = GetCurrentBeat();
        m_audioStartBeat += beatBefore - beatAfter;*/
        //Debug.Log("BeatBefore = " + beatBefore + ", beatAfter = " + beatAfter + ", audiostart = " + m_audioStartBeat);
        
        m_musicMixer.SetFloat("Master Pitch", speed);
        m_musicMixer.SetFloat("Pitch Shifter Pitch", 1.0f / speed);
    }

    public double GetAbsoluteDSPTime()
    {
        return AudioSettings.dspTime;
    }

    //public void SetBeat(double beat)
    //{
    //    double currentBeat = GetCurrentBeat();
    //    m_audioStartBeat -= currentBeat - beat;
    //}

    void ScheduleMusic()
    {
        //m_audioStart = AudioSettings.dspTime + 1.0;
        m_audioStart = 0.0;
        m_audioStartBeat = 0.0;

        for (int i = 0; i < m_audioSources.Length; i++)
        {
            AudioSource source = m_audioSources[i];
            source.PlayScheduled(m_audioStart);
        }
    }

    void Update()
    {
        m_smoothedDSPTime += Time.deltaTime * m_tempoMultiplier;

        if (m_linearMode)
        {
            // Don't run over the end
            double endTime = m_targetDSPTime + (m_audioLatency + m_bufferTime + m_fftBufferTime) * 0.5;
            if (m_smoothedDSPTime > endTime)
            {
                //SetMusicSpeed(0.0f);
                m_smoothedDSPTime = endTime;
                for (int i = 0; i < m_audioSources.Length; i++)
                {
                    m_audioSources[i].Pause();
                }
            }
        }

        double dspTime = m_audioSources[0].time; //AudioSettings.dspTime;
        if (dspTime + m_lastLoopTime < m_lastReportedDSPTime) // Audio has looped
        {
            Debug.Log("Audio looped");
            m_lastLoopTime += m_audioSources[0].clip.length;
        }
        dspTime += m_lastLoopTime;
        if (dspTime != m_lastReportedDSPTime)
        {
            m_lastReportedDSPTime = dspTime;
            //Debug.Log("Smooth difference = " + Mathf.Abs((float)(m_smoothedDSPTime - dspTime)));
            m_smoothedDSPTime = Utils.Lerp(m_smoothedDSPTime, dspTime, 0.125); // Average out slowly
        }

        /*if (m_audioStart > dspTime)
        {
            return;
        }*/
        FadeAudioSources();

        if (Input.GetKeyDown(KeyCode.PageUp))
        {
            SetMusicSpeed((float)m_tempoMultiplier * 1.1f);
        }
        if (Input.GetKeyDown(KeyCode.PageDown))
        {
            SetMusicSpeed((float)m_tempoMultiplier / 1.1f);
        }
    }

    void FadeAudioSources()
    {
        float deltaTime = Time.deltaTime;
        float deltaBeatTime = (float)SecondsToBeat(deltaTime);
        for (int i = 0; i < m_targetVolumes.Length; i++)
        {
            AudioSource source = m_audioSources[i];
            if (source.volume != m_targetVolumes[i])
            {
                float fadeSpeed = deltaBeatTime / m_fadeTimesInBeats[i];
                source.volume = Mathf.MoveTowards(source.volume, m_targetVolumes[i], fadeSpeed);
            }
        }
    }

   /* public void SetSpeed(float worldSpeed)
    {
        float beatTime = (float)GetTimePerBeat();
        float speed = worldSpeed * beatTime;
        for (int i = 0; i < m_audioSources.Length; i++)
        {
            m_audioSources[i].pitch = speed;
        }
    }*/

    public void MuteAllTracks()
    {
        for(int i = 0; i < m_targetVolumes.Length; i++)
        {
            m_targetVolumes[i] = 0.0f;
            m_fadeTimesInBeats[i] = 0.0f;
            m_audioSources[i].volume = 0.0f;
        }
    }

    public void PlayBaseTrack()
    {
        //m_targetVolumes[0] = 1.0f;
        //SetTrackVolume(0, 1.0f, 1.0f);
    }

    public void SetTrackVolume(int trackID, float volume, float fadeTimeInBeats = 1.0f)
    {
        m_targetVolumes[trackID] = volume;
        m_fadeTimesInBeats[trackID] = fadeTimeInBeats;
    }

    public bool IsTrackFading(int trackID)
    {
        return m_targetVolumes[trackID] != m_audioSources[trackID].volume;
    }

    public float GetTrackVolume(int trackID)
    {
        return m_audioSources[trackID].volume;
    }

    public double GetCurrentBeat()
    {
        double currentTime = (m_smoothedDSPTime - m_audioStart) - (m_audioLatency + m_bufferTime + m_fftBufferTime);
        return SecondsToBeatUnadjusted(currentTime) + m_audioStartBeat;
    }

    public double GetCurrentBeatUncorrected()
    {
        double currentTime = m_smoothedDSPTime - m_audioStart;
        return SecondsToBeatUnadjusted(currentTime) + m_audioStartBeat;
    }

    public double GetTempoMultiplier()
    {
        return m_tempoMultiplier;
    }

    public double GetDSPScheduleTime(double beat)
    {
        double beatsAway = beat - GetCurrentBeatUncorrected();
        double secondsAway = BeatToSeconds(beatsAway) + m_audioLatency;
        //double seconds = BeatToSeconds(beat);
        //double dspTime = seconds + m_audioStart;
        double dspTime = AudioSettings.dspTime + secondsAway;
        //m_audioSources[0].time
        return dspTime;
    }

    public double GetTimePerBeat()
    {
        return 60.0 / (m_beatsPerMinute * m_tempoMultiplier);
    }

    public int GetLatestBeatID(double offset)
    {
        return (int)(GetCurrentBeat() + offset);
    }

    public double BeatToSeconds (double beat)
    {
        return (beat * 60.0) / (m_beatsPerMinute * m_tempoMultiplier);
    }

    public double BeatToSecondsUnadjusted(double beat)
    {
        return (beat * 60) / m_beatsPerMinute;
    }

    public double SecondsToBeat (double seconds)
    {
        return (seconds * m_beatsPerMinute * m_tempoMultiplier) / 60.0;
    }

    public double SecondsToBeatUnadjusted(double seconds)
    {
        return (seconds * m_beatsPerMinute) / 60.0;
    }
}
