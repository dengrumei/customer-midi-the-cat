﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShakeBehaviour : MonoBehaviour
{
    [Serializable]
    public class ShakeConfig
    {
        public float m_amplitude;
        public float m_frequency;
        public float m_length;
        public AnimationCurve m_falloffCurve;

        public Vector2 Evaluate(float timer)
        {
            float t = timer / m_length;
            float x = Mathf.PerlinNoise(Time.time * m_frequency, 0.0f) * m_amplitude * m_falloffCurve.Evaluate(t);
            float y = Mathf.PerlinNoise(0.0f, Time.time * m_frequency) * m_amplitude * m_falloffCurve.Evaluate(t);
            return new Vector2(x, y);
        }
    }

    public class ShakeInstance
    {
        public ShakeConfig m_config;
        public float m_timer;

        public ShakeInstance(ShakeConfig config)
        {
            m_config = config;
            m_timer = 0.0f;
        }
    }
    List<ShakeInstance> m_shakes;
    List<ShakeInstance> m_toRemove;

    public ShakeConfig m_startShake;

    void Start()
    {
        m_shakes = new List<ShakeInstance>();
        m_toRemove = new List<ShakeInstance>();

        AddShake(m_startShake);
    }

    public void AddShake(ShakeConfig shake)
    {
        m_shakes.Add(new ShakeInstance(shake));
    }

	void LateUpdate ()
	{
        Vector2 offset = Vector2.zero;
        foreach(ShakeInstance shake in m_shakes)
        {
            shake.m_timer += Time.deltaTime;
            if (shake.m_timer > shake.m_config.m_length)
            {
                m_toRemove.Add(shake);
            }
            offset += shake.m_config.Evaluate(shake.m_timer);
        }
        foreach(ShakeInstance shake in m_toRemove)
        {
            m_shakes.Remove(shake);
        }
        m_toRemove.Clear();

        transform.localPosition += new Vector3(offset.x, offset.y, 0.0f);
	}
}
