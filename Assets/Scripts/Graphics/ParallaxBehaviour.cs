﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
    using UnityEditor;
#endif
using UnityEngine;

[ExecuteInEditMode]
public class ParallaxBehaviour : MonoBehaviour
{
    public float m_parallaxDistance;
    public Vector3 m_myStartPosition;

    public static float s_horizonDistance = 100.0f;

    Camera m_toFollow;


	void OnEnable ()
	{
        m_toFollow = Camera.main;
#if UNITY_EDITOR
        EditorApplication.update += LateUpdate;
#endif
    }
    void OnDisable()
    {
#if UNITY_EDITOR
        EditorApplication.update -= LateUpdate;
#endif
    }

    void LateUpdate ()
	{
#if UNITY_EDITOR
        if (Application.isEditor && !Application.isPlaying)
        {
            if (SceneView.lastActiveSceneView == null)
            {
                //Debug.Log("SceneView is null");
                return;
            }
            m_toFollow = SceneView.lastActiveSceneView.camera;
            if (m_toFollow == null)
            {
                //Debug.Log("SceneView camera is null");
                return;
            }
        }
#endif
        float parallaxSpeed = m_parallaxDistance / s_horizonDistance;
        Vector3 offset = m_toFollow.transform.position;
        Vector3 parallaxedPosition = m_myStartPosition + (offset * parallaxSpeed);
        parallaxedPosition.z = transform.position.z;
        transform.position = Utils.RoundVector3(parallaxedPosition);
        //Debug.Log("To follow position = " + m_toFollow.transform.position);
	}
}
