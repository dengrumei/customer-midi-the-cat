﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisBehaviour : MonoBehaviour
{
    public float m_lifeTime;
    public float m_fadeTime;

    public AudioClip m_startClip;
    public AudioClip m_impactClip;

    public GameObject m_collisionCallback;

    GameObject m_startClipObject;
    SpriteRenderer m_renderer;

	void Start ()
	{
        m_renderer = GetComponent<SpriteRenderer>();
        if (m_startClip != null)
        {
            m_startClipObject = MidiUtil.PlayClipAtPoint(m_startClip, Camera.main.transform.position, 1.0f, 1.0f);
        }
	}
	
	void Update ()
	{
        m_lifeTime -= Time.deltaTime;

        if (m_lifeTime < m_fadeTime)
        {
            float a = m_lifeTime / m_fadeTime;
            m_renderer.color = new Color(m_renderer.color.r, m_renderer.color.g, m_renderer.color.b, a);
        }

        if (m_lifeTime < 0.0f)
        {
            Destroy(gameObject);
        }
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ButtonBehaviour hitButton = collision.collider.gameObject.GetComponent<ButtonBehaviour>();
        if (hitButton != null)
        {
            hitButton.PushButton();
            Destroy(m_startClipObject);
            m_startClipObject = null;
            if (m_collisionCallback != null)
            {
                m_collisionCallback.GetComponent<Fungus.Flowchart>().SendFungusMessage("OnCollision");
            }
            MidiUtil.PlayClipAtPoint(m_impactClip, Camera.main.transform.position, 0.5f, 1.0f);
            Destroy(collision.collider);
        }
    }
}
