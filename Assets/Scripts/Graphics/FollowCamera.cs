﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour {

    private Transform m_target;
    public float m_smoothTime = 0.3f;
    public int m_pixelsPerUnit = 64;
    public float m_zoom = 1.0f;
    float m_zDistance;
    float m_gridSize;
    private Vector3 m_velocity = Vector3.zero;
    private Vector3 m_position;
    private float m_orthoSizeVelocity = 0.0f;

    bool m_trackingEnabled = true;

    // For pixel offsets
    public Vector2 m_pixelOffset;

	// Use this for initialization
	void Start ()
    {
        if (m_target == null)
        {
            m_target = FindObjectOfType<CatGraphicsControl>().transform;
        }
        m_zDistance = transform.position.z;
        Vector3 targetPosition = m_target.transform.position;
        m_position = new Vector3(targetPosition.x, targetPosition.y, m_zDistance);
        MakeCameraPixelPerfect();

    }

    public void DisableTracking()
    {
        m_trackingEnabled = false;
    }

    public void EnableTracking()
    {
        m_trackingEnabled = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!m_trackingEnabled)
        {
            return;
        }
        MakeCameraPixelPerfect();

        Vector3 targetCameraPosition = new Vector3(m_target.position.x, m_target.position.y, m_zDistance);
        m_position = Vector3.SmoothDamp(m_position, targetCameraPosition, ref m_velocity, m_smoothTime);

        Vector3 roundedPosition = Utils.RoundVector3(m_position);
        roundedPosition += m_gridSize * new Vector3(m_pixelOffset.x, m_pixelOffset.y, 0.0f); // For some reason, unity needs a half pixel offset but ONLY for y? :S
        transform.position = roundedPosition;
	}

    void MakeCameraPixelPerfect()
    {
        m_gridSize = 1.0f / m_pixelsPerUnit;
        Camera camera = GetComponent<Camera>();

        if (camera.orthographic)
        {
            float unitsY = ((Screen.height * m_gridSize) / m_zoom);
            float targetOrthoSize = unitsY;
            camera.orthographicSize = Mathf.SmoothDamp(camera.orthographicSize, targetOrthoSize, ref m_orthoSizeVelocity, m_smoothTime);
        }
        else
        {
            // Change the field of view to suit the camera position
            //float unitsY = ((Screen.height * m_gridSize) / m_zoom);
            //float unitsX = Mathf.Abs(m_zDistance);
            //float screenAngle = Mathf.Atan(unitsY / unitsX);
            //camera.fieldOfView = screenAngle * Mathf.Rad2Deg;

            // Move the camera to fit the field view
            float unitsY = (Screen.height) * m_gridSize;
            float frustumInnerAngles = (180.0f - camera.fieldOfView) / 2.0f * Mathf.PI / 180.0f;
            float newCamDist = Mathf.Tan(frustumInnerAngles) * (unitsY * 0.5f);
            m_zDistance = -newCamDist / m_zoom;
        }
    }
}
