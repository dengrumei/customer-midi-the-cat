﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RhythmRingControl : MonoBehaviour
{
    public GameObject m_rhythmRingPrefab;

    SpriteRenderer[] m_rhythmRings;
    PlayerControl m_player;
    MusicManager m_musicManager;

	// Use this for initialization
	void Start ()
    {
        m_rhythmRings = new SpriteRenderer[2];
        for (int i = 0; i < m_rhythmRings.Length; i++)
        {
            GameObject newRhythmRing = GameObject.Instantiate(m_rhythmRingPrefab, transform);
            m_rhythmRings[i] = newRhythmRing.GetComponent<SpriteRenderer>();
            m_rhythmRings[i].enabled = false;
        }
        m_player = FindObjectOfType<PlayerControl>();
        m_musicManager = FindObjectOfType<MusicManager>();
	}

    public void HideRings()
    {
        foreach (SpriteRenderer renderer in m_rhythmRings)
        {
            renderer.enabled = false;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        double beatTime = m_musicManager.GetCurrentBeat();
        double intervalLength = m_player.GetCurrentIntervalLength();
        double latestAllowableBeat = MidiUtil.RoundBeatToLatestInterval(beatTime, intervalLength);
        double nextAllowableBeat = latestAllowableBeat + intervalLength;
        if (beatTime < 0)
        {
            return;
        }

        double lastNoteAllocatedBeat = m_player.GetLastNoteAllocatedBeatTime();
        SpriteRenderer ring = m_rhythmRings[0];
        if (lastNoteAllocatedBeat < nextAllowableBeat) // If we haven't already used the next beat
        {
            float proportionToNextBeat = (float)((beatTime - latestAllowableBeat) / intervalLength);
            float poweredProportionToNextBeat = proportionToNextBeat * proportionToNextBeat; // Animate with a power

            Vector3 ringPosition = m_player.transform.position;
            bool ringEnabled = true;

            // If the player is going to fall (but has a wall to grab onto), predict the player position at the next beat
            RaycastHit2D gapPreTestBelow = m_player.DebuggableRaycast(m_player.GetPlayerPosition(), new Vector2(0.0f, -1f));
            float distanceWillFall = 0.0f;
            if (m_player.GetWallState() == PlayerControl.WallState.Off)
            {
                if (gapPreTestBelow.collider == null)
                {
                    distanceWillFall = 1.0f;
                }
                else if (gapPreTestBelow.collider != null && gapPreTestBelow.distance > 0.5f)
                {
                    distanceWillFall = 0.5f;
                }
            }

            if (distanceWillFall > 0.0f)
            {
                RaycastHit2D wallNearbyTest = m_player.DebuggableRaycast(m_player.GetPlayerPosition() + new Vector2(-1.0f, -distanceWillFall), new Vector2(2.0f, 0.0f));
                if (wallNearbyTest.collider == null) // Though if there won't be a wall to grab onto, probably not worth showing a ring
                {
                    ringEnabled = false;
                }
                else
                {
                    double timeToNextBeat = nextAllowableBeat - beatTime;
                    if (timeToNextBeat >= 0.5)
                    {
                        ringPosition += new Vector3(0.0f, -distanceWillFall, 0.0f);
                    }
                }
            }
            
            if (lastNoteAllocatedBeat <= latestAllowableBeat - intervalLength)
            {
                ringEnabled = false;
            }

            ring.transform.position = ringPosition;
            ring.enabled = ringEnabled;
            float scale = 1.0f - poweredProportionToNextBeat;
            ring.transform.localScale = new Vector3(scale, scale, scale);
            ring.color = new Color(1.0f, 1.0f, 1.0f, poweredProportionToNextBeat);
        }
        else
        {
            ring.enabled = false;
        }
	}
}
