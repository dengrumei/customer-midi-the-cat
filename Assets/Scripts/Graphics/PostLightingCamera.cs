﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostLightingCamera : MonoBehaviour
{
    Camera m_toCopy;
    Camera m_myCamera;

	void Start ()
	{
        m_toCopy = transform.parent.GetComponent<Camera>();
        m_myCamera = GetComponent<Camera>();
        DoCopy();
	}
	
	void LateUpdate ()
	{
        DoCopy();
	}

    void DoCopy()
    {
        m_myCamera.orthographicSize = m_toCopy.orthographicSize;
    }
}
