﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightNearPlaneOverride : MonoBehaviour
{
    public float m_nearPlane;
    public float m_range;

	void Start ()
	{
        Light light = GetComponent<Light>();
        light.shadowNearPlane = m_nearPlane;
        light.range = m_range; // Seems nearplane is based on range of light
	}
}
