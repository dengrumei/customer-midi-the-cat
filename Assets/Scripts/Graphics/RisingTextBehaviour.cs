﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RisingTextBehaviour : MonoBehaviour
{
    public float m_lifetime = 1.0f;
    public AnimationCurve m_yPositionCurve;

    Text m_text;

    float m_lifeLeft;
    Vector3 m_startPosition;
    Vector3 m_offsetToAdd;
    public float m_offsetApplicationProportionPerSecond = 1.0f;

	void Start ()
	{
        m_lifeLeft = m_lifetime;
        m_startPosition = transform.position;
        m_text = GetComponent<Text>();
	}

    public void Move(Vector3 amount)
    {
        m_offsetToAdd += amount;
    }
	
	void Update ()
	{
        m_lifeLeft -= Time.deltaTime;

        float proportionToDo = m_offsetApplicationProportionPerSecond * Time.deltaTime;
        if (proportionToDo > 1.0f)
        {
            proportionToDo = 1.0f;
        }
        Vector3 offsetThisFrame = m_offsetToAdd * proportionToDo;
        m_offsetToAdd -= offsetThisFrame;
        m_startPosition += offsetThisFrame;

        float t = 1.0f - (m_lifeLeft / m_lifetime);
        transform.position = m_startPosition + new Vector3(0.0f, m_yPositionCurve.Evaluate(t), 0.0f);


        Color colour = m_text.color;
        m_text.color = new Color(colour.r, colour.g, colour.b, 1.0f - t); // Fade out

        if (m_lifeLeft < 0.0f)
        {
            Destroy(gameObject);
        }
	}
}
