﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostCatControl : MonoBehaviour
{
    PlayerControl m_player;
    MoveStorage m_moveStore;
    MusicManager m_musicManager;

    Transform m_innerCat;

    Animator m_animator;
    SkinnedMeshRenderer m_renderer;
    float m_startAlpha;

    public float m_proximityFadeSpeed = 1.0f;

	void Start ()
	{
        m_player = FindObjectOfType<PlayerControl>();
        m_moveStore = m_player.GetComponent<MoveStorage>();
        m_musicManager = FindObjectOfType<MusicManager>();

        m_innerCat = transform.GetChild(1);
        m_animator = GetComponent<Animator>();
        m_animator.speed = 0.0f;
        m_renderer = GetComponentInChildren<SkinnedMeshRenderer>();
        m_renderer.sortingLayerName = "UI";
        m_startAlpha = m_renderer.material.color.a;
    }
	
	void Update ()
	{
        transform.position = m_player.transform.position;

        double currentBeatTime = m_musicManager.GetCurrentBeat();
        MoveStorage.Step latestStep = m_moveStore.GetLatestStep(currentBeatTime);
        MoveStorage.Step lastStep = m_moveStore.GetLastStep();
        Vector3 toDestination = lastStep.m_endPosition - latestStep.GetLerpedPosition(currentBeatTime);
        float distanceSqrd = toDestination.sqrMagnitude;
        if (lastStep.m_moveType == MoveStorage.MoveType.Fall)
        {
            distanceSqrd = 0.0f;
        }

        m_renderer.material.color = new Color(1.0f, 1.0f, 1.0f, Mathf.Clamp01(distanceSqrd * m_proximityFadeSpeed) * m_startAlpha);

        PlayerControl.State state = m_player.GetState();
        Quaternion innerRotation = CatGraphicsControl.GetTargetYRotation(state.m_lastDirection);
        m_innerCat.localRotation = innerRotation;

        Quaternion outerRotation = CatGraphicsControl.GetTargetZRotation(lastStep.m_moveType);
        transform.rotation = outerRotation;

        if (state.m_wallState == PlayerControl.WallState.Hanging)
        {
            m_animator.Play("HangWall");
        }
        else
        {
            m_animator.Play("Idle");
        }
    }
}
