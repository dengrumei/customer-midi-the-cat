﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipToFaceCamera : MonoBehaviour
{
	void LateUpdate ()
	{
		if (transform.forward.z > 0)
        {
            transform.rotation = Quaternion.Euler(0.0f, 0.0f, 180.0f);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
        }
	}
}
