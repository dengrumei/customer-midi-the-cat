﻿using Light2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapPulsationControl : MonoBehaviour
{
    // The tag of the Tiled layer that we want to pulsate (toggle on and off with the music)
    public string m_targetTag;
    public AnimationCurve m_pulsationCurve;

    GameObject[] m_targetLayerObjects;
    Color?[] m_startEmissionColours;
    float?[] m_startLightIntensities;
    Color?[] m_start2DLightColours;

    MusicManager m_musicManager;
    bool m_pulseOn;

	// Use this for initialization
	void Start ()
    {
        m_targetLayerObjects = GetApplicableObjects();
        m_musicManager = FindObjectOfType<MusicManager>();
        if (m_targetLayerObjects == null || m_targetLayerObjects.Length == 0)
        {
            Destroy(this);
        }
        else
        {
            m_startEmissionColours = new Color?[m_targetLayerObjects.Length];
            m_startLightIntensities = new float?[m_targetLayerObjects.Length];
            m_start2DLightColours = new Color?[m_targetLayerObjects.Length];

            for (int i = 0; i < m_targetLayerObjects.Length; i++)
            {
                LightSprite light2D = m_targetLayerObjects[i].GetComponent<LightSprite>();
                if (light2D != null)
                {
                    m_start2DLightColours[i] = light2D.Color;
                }
                else
                {
                    MeshRenderer renderer = m_targetLayerObjects[i].GetComponent<MeshRenderer>();
                    if (renderer == null)
                    {
                        Light targetLight = m_targetLayerObjects[i].GetComponent<Light>();
                        if (targetLight != null)
                        {
                            m_startLightIntensities[i] = targetLight.intensity;
                        }
                    }
                    else if (renderer.material.HasProperty("_EmissionColor"))
                    {
                        m_startEmissionColours[i] = renderer.material.GetColor("_EmissionColor");
                    }
                }
            }
        }
	}

    GameObject[] GetApplicableObjects()
    {
        GameObject[] taggedObjects = GameObject.FindGameObjectsWithTag(m_targetTag);
        List<GameObject> m_relevantObjects = new List<GameObject>();
        foreach (GameObject obj in taggedObjects)
        {
            if (obj.GetComponent<Light>() != null || (obj.GetComponent<Renderer>() != null && obj.transform.childCount == 0))
            {
                m_relevantObjects.Add(obj);
            }
            else
            {
                // Add all grandchildren (so spawned prefabs under each quad)
                int children = obj.transform.childCount;
                for (int i = 0; i < children; i++)
                {
                    int grandChildren = obj.transform.GetChild(i).childCount;
                    for (int j = 0; j < grandChildren; j++)
                    {
                        m_relevantObjects.Add(obj.transform.GetChild(i).GetChild(j).gameObject);
                    }
                }
            }
        }
        return m_relevantObjects.ToArray();
    }
	
	// Update is called once per frame
	void Update ()
    {
        double musicBeat = m_musicManager.GetCurrentBeat();
        int mostRecentBeat = (int)musicBeat;

        float proportionToNextBeat = (float)(musicBeat - mostRecentBeat);
        bool pulseShouldBeOn = (mostRecentBeat % 2) == 0;

        float intensity = m_pulsationCurve.Evaluate(proportionToNextBeat);
        if (pulseShouldBeOn)
        {
            intensity = 1.0f - intensity;
        }

        for (int i = 0; i < m_targetLayerObjects.Length; i++)
        {
            if (m_start2DLightColours[i] != null)
            {
                LightSprite lightSprite = m_targetLayerObjects[i].GetComponent<LightSprite>();
                lightSprite.Color = Color.Lerp(Color.black, m_start2DLightColours[i].Value, intensity);
            }
            else if (m_startLightIntensities[i] != null)
            {
                Light targetLight = m_targetLayerObjects[i].GetComponent<Light>();
                if (targetLight != null)
                {
                    targetLight.intensity = m_startLightIntensities[i].Value * intensity;
                }
            }
            else if (m_startEmissionColours[i] != null)
            {
                MeshRenderer renderer = m_targetLayerObjects[i].GetComponent<MeshRenderer>();
                renderer.material.SetColor("_EmissionColor", Color.Lerp(Color.black, m_startEmissionColours[i].Value, intensity));
            }
            else if (pulseShouldBeOn != m_pulseOn)
            {
                m_targetLayerObjects[i].SetActive(pulseShouldBeOn);
            }
        }
        m_pulseOn = pulseShouldBeOn;
	}
}
