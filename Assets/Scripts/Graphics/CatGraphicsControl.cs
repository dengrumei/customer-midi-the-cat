﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatGraphicsControl : MonoBehaviour
{
    PlayerControl m_target;
    Animator m_animator;
    MusicManager m_musicManager;
    MoveStorage m_moveStore;

    Transform m_innerCat;
    ParticleSystem m_smokeParticles;
    SkinnedMeshRenderer m_skinnedRenderer;

    public float m_originalAnimationWalkSpeed = 1.0f;
    public float m_originalAnimationClimbSpeed = 2.0f;

    public float m_beatYRotateTime = 0.25f; // To rotate 180 degrees
    float m_yRotateSpeed;
    public float m_beatZRotateTime = 0.25f; // To rotate 25 degrees
    float m_zRotateSpeed;

    MoveStorage.Step m_previousStep;

    public void Start()
    {
        m_animator = GetComponent<Animator>();
        m_target = FindObjectOfType<PlayerControl>();
        m_musicManager = FindObjectOfType<MusicManager>();
        m_moveStore = m_target.GetComponent<MoveStorage>();

        m_innerCat = transform.GetChild(1);
        m_smokeParticles = GetComponentInChildren<ParticleSystem>();

        m_skinnedRenderer = transform.GetChild(0).GetComponent<SkinnedMeshRenderer>();

        {
            float timeToRotate = (float)m_musicManager.GetTimePerBeat() * m_beatYRotateTime;
            m_yRotateSpeed = 180.0f / timeToRotate;
        }
        {
            float timeToRotate = (float)m_musicManager.GetTimePerBeat() * m_beatZRotateTime;
            m_zRotateSpeed = 25.0f / timeToRotate;
        }

        SetPosition(m_target.transform.position);
    }

    public Transform GetInnerCatTransform()
    {
        return m_innerCat;
    }

    public void SetCatColour(Color c)
    {
        m_skinnedRenderer.material.color = c;
    }

    public float GetAnimSpeedNeeded(MoveStorage.Step step, float originalAnimSpeed)
    {
        double beatLength = m_musicManager.GetTimePerBeat();
        double timeToMove = beatLength * (step.m_endBeat - step.m_startBeat);
        float distance = Vector2.Distance(step.m_endPosition, step.m_startPosition);
        float moveSpeed = distance / (float)timeToMove;
        return moveSpeed / originalAnimSpeed;
    }

    void ReadMoveStore()
    {
        double currentBeatTime = m_musicManager.GetCurrentBeat();
        //Debug.Log("Current beat = " + currentBeatTime);
        MoveStorage.Step latestStep = m_moveStore.GetLatestStep(currentBeatTime);

        if (latestStep != null)
        {
            Vector2 lerpedPosition = latestStep.GetLerpedPosition(currentBeatTime);
            transform.position = lerpedPosition;
            HandleRotation(latestStep.m_direction, latestStep.m_moveType);

            bool newStep = latestStep != m_previousStep;
            HandleWalkAnimations(latestStep, newStep, currentBeatTime);
            HandleJumpingAnimations(latestStep, newStep, currentBeatTime);
            HandleWallGrabAnimations(latestStep, newStep, currentBeatTime);
            HandleWallClimbAnimations(latestStep, newStep, currentBeatTime);
            m_previousStep = latestStep;
        }
        else
        {
            Debug.Log("No latest step!");
        }
    }

    void Update()
    {
        ReadMoveStore();
    }

    void HandleWallClimbAnimations(MoveStorage.Step step, bool newStep, double currentBeatTime)
    {
        bool shouldClimb = step.m_moveType == MoveStorage.MoveType.Climb;
        if (step.m_endBeat < currentBeatTime)
        {
            shouldClimb = false;
        }

        // Change the animation
        if (newStep)
        {
            if (shouldClimb)
            {
                ActuallySetTrigger("StartClimbWall");
            }
        }
        else
        {
            if (m_currentAnimation != null && m_currentAnimation.Equals("StartClimbWall"))
            {
                m_animator.SetFloat("ClimbSpeed", GetAnimSpeedNeeded(step, m_originalAnimationClimbSpeed));

                if (!shouldClimb)
                {
                    ActuallySetTrigger("StopClimbWall");
                }
            }
        }
    }

    void HandleWallGrabAnimations(MoveStorage.Step step, bool newStep, double currentBeatTime)
    {
        if (currentBeatTime < step.m_endBeat)
        {
            if (step.m_moveType == MoveStorage.MoveType.WallGrab)
            {
                if (newStep)
                {
                    StartJumpwall();
                }
            }
            else if (step.m_moveType == MoveStorage.MoveType.WallRelease)
            {
                if (newStep)
                {
                    StopHangWall();
                }
            }
        }
    }

    void HandleJumpingAnimations(MoveStorage.Step step, bool newStep, double currentBeatTime)
    {
        if (currentBeatTime < step.m_endBeat)
        {
            if (step.m_moveType == MoveStorage.MoveType.Jump)
            {
                if (newStep)
                {
                    m_animator.SetFloat("JumpSpeed", 1.0f);
                    StartJump();
                }

                float jump01 = step.GetT(currentBeatTime);
                float jumpOffset = Mathf.Sin(jump01 * Mathf.PI) * 0.6f;
                transform.position += new Vector3(0.0f, jumpOffset, 0.0f);
            }
            else if (step.m_moveType == MoveStorage.MoveType.JumpToWall)
            {
                if (newStep)
                {
                    m_animator.SetFloat("JumpSpeed", 0.5f);
                    StartJumpwall();
                }
                // We want a jump to wall to look like it's jumping "through" the wall, but then latch when it hits
                Vector2 flattenedPosition = transform.position;
                flattenedPosition.y = step.m_startPosition.y;

                float jumpHeight = step.m_endPosition.y - step.m_startPosition.y;

                float jump01 = step.GetT(currentBeatTime);
                if (jump01 > 0.5f)
                {
                    jump01 = 0.5f; // Only want the first half of the jump arc
                }
                float jumpOffset = Mathf.Sin(jump01 * Mathf.PI) * jumpHeight;

                transform.position = flattenedPosition + new Vector2(0.0f, jumpOffset);
            }
            else if (step.m_moveType == MoveStorage.MoveType.JumpIntoCeiling)
            {
                if (newStep)
                {
                    m_animator.SetFloat("JumpSpeed", 2.0f);
                    StartJump();
                }
            }
        }
    }

    void HandleWalkAnimations(MoveStorage.Step step, bool newStep, double currentBeatTime)
    {
        bool shouldWalk = step.m_moveType == MoveStorage.MoveType.Walk || step.m_moveType == MoveStorage.MoveType.WalkRightDownwards || step.m_moveType == MoveStorage.MoveType.WalkRightUpwards;
        if (step.m_endBeat < currentBeatTime || step.m_startPosition == step.m_endPosition)
        {
            shouldWalk = false;
        }

        // Change the animation
        if (newStep)
        {
            if (shouldWalk)
            {
                StartWalk(step.m_direction);
            }
        }
        else
        {
            if (m_currentAnimation != null && m_currentAnimation.Equals("StartWalk"))
            {
                m_animator.SetFloat("WalkSpeed", GetAnimSpeedNeeded(step, m_originalAnimationWalkSpeed));

                if (!shouldWalk)
                {
                    StopWalk();
                }
            }
        }
    }

    public static Quaternion GetTargetYRotation(int direction)
    {
        if (direction == 1)
        {
            return Quaternion.Euler(0.0f, 90.0f, 0.0f);
        }
        else
        {
            return Quaternion.Euler(0.0f, 270.0f, 0.0f);
        }
    }

    public static Quaternion GetTargetZRotation(MoveStorage.MoveType moveType)
    {
        float targetZRotation = 0.0f;
        if (moveType == MoveStorage.MoveType.WalkRightUpwards)
        {
            targetZRotation = 25;
        }
        else if (moveType == MoveStorage.MoveType.WalkRightDownwards)
        {
            targetZRotation = -25;
        }
        return Quaternion.Euler(0.0f, 0.0f, targetZRotation);
    }

    void HandleRotation(int direction, MoveStorage.MoveType moveType)
    {
        // Inner rotation for y axis
        Quaternion targetInnerRotation = m_innerCat.transform.localRotation;
        if (direction != 0)
        {
            targetInnerRotation = GetTargetYRotation(direction);
        }
        m_innerCat.transform.localRotation = Quaternion.RotateTowards(m_innerCat.transform.localRotation, targetInnerRotation, m_yRotateSpeed * Time.deltaTime);

        // Outer rotation for z axis

        Quaternion targetOuterRotation = GetTargetZRotation(moveType);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetOuterRotation, m_zRotateSpeed * Time.deltaTime);
    }

    public void SetPosition(Vector3 catPosition)
    {
        transform.position = catPosition;
    }

    public void StartWalk(int direction)
    {
        if (m_currentAnimation != "StartWalk")
        {
            ActuallySetTrigger("StartWalk");
        }
        //if (m_target.GetLastDirection() == direction)
        {
            int momentum = m_target.GetMomentum();
            m_smokeParticles.Emit(20 * momentum);
        }
    }
    void StopWalk()
    {
        ActuallySetTrigger("StopWalk");
    }

    public void StartClimb()
    {
        ActuallySetTrigger("StartClimbWall");
    }

    string m_currentAnimation = null;
    public bool ActuallySetTrigger(string name)
    {
        if (name != m_currentAnimation)
        {
            if (m_currentAnimation != null)
            {
                m_animator.ResetTrigger(m_currentAnimation);
            }
            m_animator.SetTrigger(name);
            m_currentAnimation = name;
            return true;
        }
        return false;
    }

    public void PlayTrigger(string triggerName, string animationName)
    {
        if (!ActuallySetTrigger(triggerName))
        {
            m_animator.Play(animationName, -1, 0.0f);
        }
    }

    public void StopHangWall()
    {
        ActuallySetTrigger("StopHangWall");
    }

    public void StartJumpwall()
    {
        if (m_currentAnimation != "StartJumpwall")
        {
            ActuallySetTrigger("StartJumpwall");
        }
        else
        {
            m_animator.Play("Jumpwall", -1, 0.0f);
        }
    }

    public void StartJump()
    {
        if (m_currentAnimation != "StartJump")
        {
            ActuallySetTrigger("StartJump");
        }
        else
        {
            m_animator.Play("Jump", -1, 0.0f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == MovementUtils.s_deathLayer)
        {
            Debug.Log("Cat collision");
            m_target.Damage();
        }
        else if (collision.gameObject.layer == MovementUtils.s_enemyLayer && !m_target.GetComponent<SneakBehaviour>().IsSneaking())
        {
            Debug.Log("Cat caught by enemy");
            m_target.Damage();
        }
    }
}
