﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatTailControl : MonoBehaviour
{
    PlayerControl m_player;
    MusicManager m_musicManager;

    public Transform[] m_tailJoints;

    public float m_rotationAmount = 1.0f;
    public float m_extraRotationPerBone = 1.0f;
    public float m_intervalLengthMultiplier = 1.0f;

    int m_direction = 1;
    float m_lastBeatProportion;

    void Start()
    {
        m_player = FindObjectOfType<PlayerControl>();
        m_musicManager = FindObjectOfType<MusicManager>();
    }

    void LateUpdate()
    {
        if (!m_player.isActiveAndEnabled)
        {
            return;
        }
        double beatTime = m_musicManager.GetCurrentBeat();
        double intervalLength = m_player.GetCurrentIntervalLength() * m_intervalLengthMultiplier;
        double latestAllowableBeat = MidiUtil.RoundBeatToLatestInterval(beatTime, intervalLength);

        float proportionToNextBeat = (float)((beatTime - latestAllowableBeat) / intervalLength);
        if (proportionToNextBeat < m_lastBeatProportion)
        {
            m_direction = -m_direction;
        }

        for (int i = 1; i < m_tailJoints.Length; i++)
        {
            float rotationAmount = (m_rotationAmount + (m_extraRotationPerBone * i)) * m_direction * (float)intervalLength; // Do smaller amplitude when wavelength is smaller
            float angle = Mathf.Sin(proportionToNextBeat * Mathf.PI) * rotationAmount;
            m_tailJoints[i].localRotation = Quaternion.Euler(0.0f, 0.0f, angle);
        }
        m_lastBeatProportion = proportionToNextBeat;
    }
}
