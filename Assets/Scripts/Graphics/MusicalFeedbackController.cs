﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicalFeedbackController : MonoBehaviour
{
    public GameObject m_feedbackTextPrefab;
    public Vector3 m_perFeedbackOffset = new Vector3(0.0f, 0.5f, 0.0f);

    Canvas m_canvas;
    CatGraphicsControl m_cat;
    List<GameObject> m_existingFeedback;
    List<GameObject> m_toDestroy;

	void Start ()
	{
        m_canvas = GameObject.FindWithTag("WorldCanvas").GetComponent<Canvas>();
        m_cat = FindObjectOfType<CatGraphicsControl>();

        m_existingFeedback = new List<GameObject>();
        m_toDestroy = new List<GameObject>();
    }

    void ClearOldObjects()
    {
        for (int i = 0; i < m_existingFeedback.Count; i++)
        {
            if (m_existingFeedback[i] == null)
            {
                m_toDestroy.Add(m_existingFeedback[i]);
            }
        }
        foreach (GameObject toDestroy in m_toDestroy)
        {
            m_existingFeedback.Remove(toDestroy);
        }
        m_toDestroy.Clear();
    }
	
	public void EmitFeedbackText (string text)
	{
        ClearOldObjects();

        GameObject newText = GameObject.Instantiate(m_feedbackTextPrefab, m_canvas.transform);
        newText.transform.position = m_cat.transform.position;// + new Vector3(0.0f, 0.5f);
        Text textBit = newText.GetComponent<Text>();
        textBit.text = text;

        for (int i = 0; i < m_existingFeedback.Count; i++)
        {
            //Debug.Log("Setting transform from " + m_existingFeedback[i].transform.position + " and adding " + m_perFeedbackOffset);
            m_existingFeedback[i].GetComponent<RisingTextBehaviour>().Move(m_perFeedbackOffset);
        }
        m_existingFeedback.Add(newText);
    }
}
