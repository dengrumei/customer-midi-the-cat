﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnToTag : MonoBehaviour
{
    public string m_tagName = "WorldCanvas";
    public GameObject m_prefab;

	void Awake ()
	{
        if (m_tagName.Length == 0)
        {
            GameObject.Instantiate(m_prefab);
            return;
        }
        GameObject target = GameObject.FindWithTag(m_tagName);
        GameObject.Instantiate(m_prefab, target.transform);
    }
}
