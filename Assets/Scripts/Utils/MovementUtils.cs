﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementUtils
{
    public static int s_raycastMask;
    public static int s_stepsLayer;
    public static int s_stepsDownLayer;
    public static int s_deathLayer;
    public static int s_enemyLayer;
    public static int s_obstacleLayer;
    public static int s_obstacleOnlyMask;
    public static int s_obstaclesAndStepsMask;

    public static void InitialiseMasks()
    {
        s_stepsLayer = LayerMask.NameToLayer("Steps");
        s_stepsDownLayer = LayerMask.NameToLayer("StepsDown");
        s_deathLayer = LayerMask.NameToLayer("Death");
        s_enemyLayer = LayerMask.NameToLayer("Enemy");
        s_obstacleLayer = LayerMask.NameToLayer("Default");
        s_raycastMask = ~(LayerMask.GetMask("HittableButtons") | LayerMask.GetMask("Player") | LayerMask.GetMask("Enemy"));
        s_obstacleOnlyMask = LayerMask.GetMask("Default");
        s_obstaclesAndStepsMask = LayerMask.GetMask("Default", "Steps", "StepsDown");
    }

    public static bool IsStepsLayer(int layerID)
    {
        return layerID == s_stepsLayer || layerID == s_stepsDownLayer;
    }
    public static bool IsDeathLayer(int layerID)
    {
        return layerID == s_deathLayer;
    }
    public static bool IsObstacleLayer(int layerID)
    {
        return layerID == s_obstacleLayer;
    }

    public static void HandleJumping(int momentum, PlayerControl.State state, MoveStorage moveStore)
    {
        bool wouldHitHead = MovementUtils.CheckJumpHeadbang(state);
        if (wouldHitHead)
        {
            Move(state, Vector3.zero, moveStore, MoveStorage.MoveType.JumpIntoCeiling, state.m_lastDirection);
            state.m_wallState = PlayerControl.WallState.Off;
        }
        else
        {
            DoJumpPath(state.m_lastDirection, momentum, state, moveStore);
        }
        state.m_momentum = 1;
    }

    static void DoJumpPath(int direction, int momentum, PlayerControl.State state, MoveStorage moveStore)
    {
        // If a wall is directly in-front of the player, then reduce momentum to 1 so we just jump on top of it
        RaycastHit2D ahead = DebuggableRaycast(state.GetRaycastPosition(), new Vector2(direction, 0.0f), Color.blue, MovementUtils.s_obstaclesAndStepsMask);
        if (ahead.collider != null)
        {
            momentum = 1;
        }

        // Search along the path of the jump to see if we'll grab a wall
        for (int i = 0; i < momentum; i++)
        {
            RaycastHit2D across = DebuggableRaycast(state.GetRaycastPosition() + new Vector2(i * direction, 1.0f), new Vector2(direction, 0.0f), Color.magenta, MovementUtils.s_obstaclesAndStepsMask);
            if (across.collider != null)
            {
                Move(state, new Vector3(i * direction, 1.0f, 0.0f), moveStore, MoveStorage.MoveType.JumpToWall, direction);
                state.m_wallState = PlayerControl.WallState.Hanging;
                return;
            }
        }
        RaycastHit2D down = DebuggableRaycast(state.GetRaycastPosition() + new Vector2(momentum * direction, 1.0f), new Vector2(0.0f, -1.0f));
        if (down.collider != null)
        {
            Move(state, new Vector3(momentum * direction, 1.0f, 0.0f), moveStore, MoveStorage.MoveType.Jump);
        }
        else
        {
            Move(state, new Vector3(momentum * direction, 0.0f, 0.0f), moveStore, MoveStorage.MoveType.Jump);
        }
        state.m_wallState = PlayerControl.WallState.Off;
    }

    public static bool WouldHitFloorOrCeiling(int verticalDirection, PlayerControl.State state)
    {
        RaycastHit2D wouldHitFloorOrCeiling = DebuggableRaycast(state.GetRaycastPosition(), new Vector2(0.0f, 1f * verticalDirection));
        return wouldHitFloorOrCeiling.collider != null;
    }

    public static void HandleWallMovement(int verticalDirection, PlayerControl.State state, MoveStorage moveStore)
    {
        int direction = state.m_lastDirection;
        bool moved = false;
        int moves = Mathf.Min(state.m_momentum, 3);

        for (int i = 0; i < moves; i++)
        {
            if (WouldHitFloorOrCeiling(verticalDirection, state))
            {
                break;
            }

            moved = true;
            RaycastHit2D wallInFront = DebuggableRaycast(state.GetRaycastPosition() + new Vector2(0.0f, verticalDirection), new Vector2(direction, 0.0f), Color.blue, MovementUtils.s_obstacleOnlyMask);

            if (wallInFront.collider != null)
            {
                Move(state, new Vector3(0.0f, verticalDirection, 0.0f), moveStore, MoveStorage.MoveType.Climb, direction);
            }
            else
            {
                state.m_wallState = PlayerControl.WallState.Off;

                if (verticalDirection == 1) // Climb onto the top of the wall
                {
                    Move(state, new Vector3(direction, 1.0f, 0.0f), moveStore, MoveStorage.MoveType.Jump, direction);
                    moves = 0;
                }
                else
                {
                    Move(state, Vector3.zero, moveStore, MoveStorage.MoveType.WallRelease, direction);
                }
                break;
            }
            RaycastHit2D wallAhead = DebuggableRaycast(state.GetRaycastPosition() + new Vector2(0.0f, verticalDirection), new Vector2(direction, 0.0f), Color.green, MovementUtils.s_obstacleOnlyMask);
            if (wallAhead.collider == null) // Slow down to the top or bottom of climbable wall (might want to peek over the top)
            {
                break;
            }
        }

        if (moved)
        {
            state.m_momentum = Mathf.Min(moves + 1, 4);
        }
    }

    public static bool CheckJumpHeadbang(PlayerControl.State state)
    {
        RaycastHit2D up = DebuggableRaycast(state.GetRaycastPosition(), new Vector2(0.0f, 1.0f), Color.black, MovementUtils.s_obstaclesAndStepsMask);
        if (up.collider != null)
        {
            return true; // You hit your head!  Can't jump
        }
        return false;
    }

    public static bool IsOnGround(PlayerControl.State state)
    {
        if (state.m_stepState != PlayerControl.StepState.Off)
        {
            return true;
        }
        RaycastHit2D floorBelow = DebuggableRaycast(state.GetRaycastPosition(), new Vector2(0.0f, -0.5f), Color.red, MovementUtils.s_obstacleOnlyMask);

        if (floorBelow.collider == null) // In mid-air
        {
            return false;
        }
        return state.m_wallState == PlayerControl.WallState.Off;
    }

    public static void HandleGroundMovement(int direction, int verticalDirection, PlayerControl.State state, MoveStorage moveStore)
    {
        bool moved = false;
        int moves = Mathf.Min(state.m_momentum, 3);
        for (int i = 0; i < moves; i++)
        {
            if (MovementUtils.Step(direction, verticalDirection, state, moveStore))
            {
                moved = true;
            }

            RaycastHit2D gapTestBelow = DebuggableRaycast(state.GetRaycastPosition(), new Vector2(0.0f, -0.5f));
            if (state.m_wallState == PlayerControl.WallState.Off && state.m_stepState == PlayerControl.StepState.Off)
            {
                RaycastHit2D gapTestAhead = DebuggableRaycast(state.GetRaycastPosition() + new Vector2(direction, 0.0f), new Vector2(0.0f, -0.5f));
                // Stop if there's a gap ahead or below
                if (gapTestBelow.collider == null || gapTestAhead.collider == null)
                {
                    break;
                }
            }
        }

        if (moved)
        {
            state.m_momentum = Mathf.Min(moves + 1, 4);
        }
    }

    public static bool WillHitBranchingSteps(int direction, PlayerControl.State state)
    {
        RaycastHit2D forwardHit = DebuggableRaycast(state.GetRaycastPosition(), new Vector2(direction, 0.0f));
        RaycastHit2D aheadDownHit = DebuggableRaycast(state.GetRaycastPosition() + new Vector2(direction * (0.75f), -0.2f), new Vector2(0.0f, -0.25f));
        if (forwardHit.collider != null)
        {
            int forwardHitLayer = forwardHit.transform.gameObject.layer;
            if (MovementUtils.IsStepsLayer(forwardHitLayer))
            {
                if (aheadDownHit.collider != null && MovementUtils.IsStepsLayer(aheadDownHit.transform.gameObject.layer) && aheadDownHit.transform.gameObject.layer != forwardHitLayer)
                {
                    return true;
                }
            }
        }
        return false;
    }

    static bool Step(int direction, int verticalDirection, PlayerControl.State state, MoveStorage moveStore)
    {
        switch (state.m_stepState)
        {
            case PlayerControl.StepState.Off:
                RaycastHit2D forwardHit = DebuggableRaycast(state.GetRaycastPosition(), new Vector2(direction, 0.0f));
                RaycastHit2D aheadDownHit = DebuggableRaycast(state.GetRaycastPosition() + new Vector2(direction * (0.75f), -0.2f), new Vector2(0.0f, -0.25f));
                if (forwardHit.collider != null)
                {
                    int forwardHitLayer = forwardHit.transform.gameObject.layer;
                    if (MovementUtils.IsStepsLayer(forwardHitLayer))
                    {
                        if (aheadDownHit.collider != null && MovementUtils.IsStepsLayer(aheadDownHit.transform.gameObject.layer) && aheadDownHit.transform.gameObject.layer != forwardHitLayer)
                        {
                            if (verticalDirection > 0)
                            {
                                StartGoingUpwards(direction, state, moveStore);
                            }
                            else if (verticalDirection < 0)
                            {
                                StartGoingDownwards(direction, state, moveStore);
                            }
                            else
                            {
                                Debug.Log("Hitting branching steps!");
                                Move(state, Vector3.zero, moveStore, MoveStorage.MoveType.Walk, direction);
                                return false;
                            }
                        }
                        else
                        {
                            Debug.Log("Hitting steps up");
                            StartGoingUpwards(direction, state, moveStore);
                        }
                    }
                    else
                    {
                        Debug.Log("Hitting wall");
                        //Move(state, Vector3.zero, moveStore, MoveStorage.MoveType.Walk, direction);
                        return false;
                    }
                }
                else
                {
                    if (aheadDownHit.collider != null && IsStepsLayer(aheadDownHit.transform.gameObject.layer))
                    {
                        StartGoingDownwards(direction, state, moveStore);
                    }
                    else
                    {
                        Move(state, new Vector3(direction, 0.0f, 0.0f), moveStore);
                    }
                }
                break;

            case PlayerControl.StepState.GoingUp:
                SetStepPosition(state.m_stepBottom, state.m_stepID + direction, state.m_stepType, state.m_stepCount, direction, state, moveStore);
                SearchForEndOfStepsUp(direction, state);
                break;

            case PlayerControl.StepState.GoingDown:
                SetStepPosition(state.m_stepBottom, state.m_stepID + direction, state.m_stepType, state.m_stepCount, direction, state, moveStore);
                SearchForEndOfStepsDown(direction, state);
                break;

            case PlayerControl.StepState.UpEnd:
            case PlayerControl.StepState.DownEnd:
                Vector2 nextGridSquare = state.m_stepBottom + new Vector2(direction, 0.0f);
                if ((state.m_stepType == PlayerControl.StepType.Upwards && direction == 1) || (state.m_stepType == PlayerControl.StepType.Downwards && direction == -1)) // If we're going up...
                {
                    nextGridSquare.y += 0.5f;
                }

                Vector3 toDestination = new Vector3(nextGridSquare.x, nextGridSquare.y, 0.0f) - state.m_position;
                Move(state, toDestination, moveStore, MoveStorage.MoveType.Walk, direction);
                state.m_stepState = PlayerControl.StepState.Off;
                break;
        }
        return true;
    }

    static Vector2 GetStepSize(PlayerControl.State state)
    {
        int stepsOnATile = state.m_stepCount;
        float tileWidth = 1.0f;
        float tileHeight = 0.5f;
        return new Vector2(tileWidth / stepsOnATile, tileHeight / stepsOnATile);
    }

    public static void TurnAroundOnSteps(PlayerControl.State state, MoveStorage moveStore)
    {
        int direction = state.m_lastDirection;
        if (state.m_stepState == PlayerControl.StepState.GoingDown || state.m_stepState == PlayerControl.StepState.DownEnd)
        {
            state.m_stepState = PlayerControl.StepState.GoingUp;
            SearchForEndOfStepsUp(direction, state);
        }
        else if (state.m_stepState == PlayerControl.StepState.GoingUp || state.m_stepState == PlayerControl.StepState.UpEnd)
        {
            state.m_stepState = PlayerControl.StepState.GoingDown;
            SearchForEndOfStepsDown(direction, state);
        }

        MoveStorage.MoveType moveType = state.m_stepType == PlayerControl.StepType.Upwards ? MoveStorage.MoveType.WalkRightUpwards : MoveStorage.MoveType.WalkRightDownwards;
        Move(state, Vector3.zero, moveStore, moveType, state.m_lastDirection);
    }

    static void SearchForEndOfStepsUp(int direction, PlayerControl.State state)
    {
        RaycastHit2D postStepForwardHit = DebuggableRaycast(state.GetRaycastPosition() + new Vector2(GetStepSize(state).x * direction * 0.75f, 0.0f), new Vector2(direction * 0.5f, 0.0f));
        if (postStepForwardHit.collider == null)
        {
            state.m_stepState = PlayerControl.StepState.UpEnd;
            Debug.Log("At end of stairs up");
        }
    }

    static void SearchForEndOfStepsDown(int direction, PlayerControl.State state)
    {
        RaycastHit2D postStepAheadDownHit = DebuggableRaycast(state.GetRaycastPosition() + new Vector2(GetStepSize(state).x * direction * 0.75f, 0.0f), new Vector2(0.0f, -1.0f), Color.green);
        if (postStepAheadDownHit.collider == null || !IsStepsLayer(postStepAheadDownHit.transform.gameObject.layer))
        {
            state.m_stepState = PlayerControl.StepState.DownEnd;
            Debug.Log("At end of stairs down");
        }
    }

    static Vector2 GetPositionOnSteps(Vector2 stepBottom, int stepID, PlayerControl.StepType stepType, PlayerControl.State state)
    {
        Vector2 stepSize = GetStepSize(state);
        Vector2 bottomLeft = stepBottom + new Vector2(-(0.5f + stepSize.x * 0.5f), 0.0f);
        Vector2 stepOffset = stepSize * stepID;
        if (stepType == PlayerControl.StepType.Downwards)
        {
            stepOffset.y = (0.5f + stepSize.y) - stepOffset.y;
        }
        return bottomLeft + stepOffset;
    }

    static void SetStepPosition(Vector2 stepBottom, int stepID, PlayerControl.StepType stepType, int stepCount, int direction, PlayerControl.State state, MoveStorage moveStore)
    {
        if (stepID < 1)
        {
            stepID += stepCount;
            float bottomOfLeftStep = (stepType == PlayerControl.StepType.Upwards ? -0.5f : 0.5f);
            stepBottom = stepBottom + new Vector2(-1.0f, bottomOfLeftStep);
        }
        else if (stepID > stepCount)
        {
            stepID -= stepCount;
            float bottomOfRightStep = (stepType == PlayerControl.StepType.Upwards ? 0.5f : -0.5f);
            stepBottom = stepBottom + new Vector2(1.0f, bottomOfRightStep);
        }
        state.m_stepBottom = stepBottom;
        state.m_stepID = stepID;
        state.m_stepType = stepType;
        state.m_stepCount = stepCount;
        Vector2 stepPosition = GetPositionOnSteps(stepBottom, stepID, stepType, state);

        MoveStorage.MoveType moveType = MoveStorage.MoveType.WalkRightUpwards;
        if (stepType == PlayerControl.StepType.Downwards)
        {
            moveType = MoveStorage.MoveType.WalkRightDownwards;
        }
        Move(state, new Vector3(stepPosition.x, stepPosition.y, 0.0f) - state.m_position, moveStore, moveType, direction);
    }

    static void StartGoingUpwards(int direction, PlayerControl.State state, MoveStorage moveStore)
    {
        int stepCount = state.m_linearMode ? 1 : 2;
        SetStepPosition(state.m_position + new Vector3(direction, 0.0f, 0.0f), (direction == 1 ? 1 : stepCount), (direction == 1 ? PlayerControl.StepType.Upwards : PlayerControl.StepType.Downwards), stepCount, direction, state, moveStore);

        state.m_stepState = PlayerControl.StepState.GoingUp;

        SearchForEndOfStepsUp(direction, state);
    }

    static void StartGoingDownwards(int direction, PlayerControl.State state, MoveStorage moveStore)
    {
        int stepCount = state.m_linearMode ? 1 : 2;
        SetStepPosition(state.m_position + new Vector3(direction, -0.5f, 0.0f), (direction == 1 ? 1 : stepCount), (direction == 1 ? PlayerControl.StepType.Downwards : PlayerControl.StepType.Upwards), stepCount, direction, state, moveStore);

        state.m_stepState = PlayerControl.StepState.GoingDown;

        SearchForEndOfStepsDown(direction, state);
    }

    public static RaycastHit2D DebuggableRaycast(Vector2 position, Vector2 direction, Color? colour = null, int? mask = null)
    {
        return Utils.DebuggableRaycast(position, direction, colour, (mask != null ? mask.Value : s_raycastMask));
    }

    public static void Move(PlayerControl.State newState, Vector3 movement, MoveStorage moveStore, MoveStorage.MoveType moveType = MoveStorage.MoveType.Walk, int direction = 0)
    {
        MoveStorage.Step newStep = new MoveStorage.Step(newState.m_position, newState.m_position + movement, moveType);
        if (direction != 0)
        {
            newStep.m_direction = direction;
        }
        moveStore.AddStep(newStep);
        newState.m_position += movement;
    }
}
