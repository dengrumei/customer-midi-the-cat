﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Net.Mail;
using UnityEngine.Networking;

namespace Fiftytwo
{
    public class MailChimpSubscriber : MonoBehaviour
    {
        private const string UrlFormat = "https://{0}.api.mailchimp.com/3.0/lists/{1}/members";

        [SerializeField]
        private string _apiKey;
        [SerializeField]
        private string _listId;

        [SerializeField]
        private UnityEvent _subscribeSuccess;
        [SerializeField]
        private UnityEvent _subscribeError;

        public void Subscribe(string email, string feedback)
        {
            if (IsValidEmail(email))
            {
                var www = BuildWWW(email, feedback);

                if (www != null)
                {
                    StartCoroutine(SendToMailChimp(www));
                }
                else
                {
                    _subscribeError.Invoke();
                }
            }
            else
            {
                Debug.Log("MailChimp — Invalid email");
                _subscribeError.Invoke();
            }
        }

        private bool IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                return false;

            try
            {
                var mailAdress = new MailAddress(email);
                if (mailAdress == null)
                {
                    Debug.LogError("Mail Address object is null");
                }
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private IEnumerator SendToMailChimp(UnityWebRequest request)
        {
            yield return request.SendWebRequest();

            if (string.IsNullOrEmpty(request.error))
            {
                Debug.Log("MailChimp — Subscribe success - code " + request.responseCode);
                _subscribeSuccess.Invoke();
            }
            else
            {
                Debug.LogError("MailChimp — Subscribe error: " + request.responseCode + ", " + request.error);
                Debug.LogError(request.downloadHandler.text);
                _subscribeError.Invoke();
            }
        }

        private UnityWebRequest BuildWWW(string email, string feedback)
        {
            var splittedApiKey = _apiKey.Split('-');

            if (splittedApiKey.Length != 2)
            {
                Debug.LogError("MailChimp — Invalid API Key format");
                return null;
            }

            var urlPrefix = splittedApiKey[1];

            var url = string.Format(UrlFormat, urlPrefix, _listId);
            string data = "{\"email_address\": \"" + email + "\",\"status\": \"pending\",\"merge_fields\": {\"FEEDBACK\": \"" + feedback + "\"}}";

            UnityWebRequest www = UnityWebRequest.Post(url, data);
            www.uploadHandler = new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(data));
            www.uploadHandler.contentType = "text/plain";
            www.SetRequestHeader("Authorization", "apikey " + _apiKey);
            //www.SetRequestHeader("Content-Type", "text/plain"); // Doesn't work due to bug in 2017.3 :@
            www.chunkedTransfer = false;
            return www;
        }
    }
}
