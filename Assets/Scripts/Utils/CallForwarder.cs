﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CallForwarder : MonoBehaviour
{
    public UnityEvent m_event;

    void Call()
    {
        m_event.Invoke();
    }
}
