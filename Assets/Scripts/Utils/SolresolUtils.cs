﻿using System.Collections.Generic;
using UnityEngine;

public class SolresolUtils
{
    static TextAsset s_wordsAsset;
    static Dictionary<string, string> s_englishToSolresolTable;
    static Dictionary<string, string> s_solresolToEnglishTable;

    static string[] s_solresolLetters = { " ", "do", "re", "mi", "fa", "sol", "la", "si" };

    static void LoadAsset()
    {
        if (s_wordsAsset == null)
        {
            s_wordsAsset = Resources.Load<TextAsset>("Solresol");
            if (s_wordsAsset != null)
            {
                string[] lines = s_wordsAsset.text.Split('\r');
                s_englishToSolresolTable = new Dictionary<string, string>(lines.Length);
                s_solresolToEnglishTable = new Dictionary<string, string>(lines.Length);
                int wordsTranslated = 0;
                foreach (string line in lines)
                {
                    string[] sections = line.Split(' ');
                    if (sections.Length != 3)
                    {
                        continue;
                    }
                    //string number = sections[0].ToLowerInvariant();
                    string solresol = sections[1].ToLowerInvariant();
                    string english = sections[2].ToLowerInvariant();
                    s_englishToSolresolTable[english] = solresol;
                    s_solresolToEnglishTable[solresol] = english;
                    wordsTranslated++;
                }
            }
        }
    }
    static bool LoadDictionary()
    {
        if (s_englishToSolresolTable == null || s_solresolToEnglishTable == null)
        {
            LoadAsset();
            if (s_englishToSolresolTable == null || s_solresolToEnglishTable == null)
            {
                Debug.Log("Words tables not created");
                return false;
            }
        }
        return true;
    }

    static string TranslateUsingTable(string text, Dictionary<string, string> table)
    {
        string[] words = text.Split(' ');

        string translation = "";
        for (int i = 0; i < words.Length; i++)
        {
            string word = words[i];
            string lookup = null;
            bool wordExists = table.TryGetValue(word, out lookup);
            if (wordExists && lookup != null)
            {
                if (translation.Length > 0)
                {
                    translation += " ";
                }
                translation += lookup;
            }
        }
        return translation;
    }
    public static string TranslateEnglishToSolresol(string text)
    {
        if (!LoadDictionary())
        {
            return "Bleh";
        }
        string unwantedLetters = "?&^$#@!()+-,:;<>’\'-_*";
        string sanitisedText = text.ToLowerInvariant();
        foreach(char c in unwantedLetters)
        {
            sanitisedText = sanitisedText.Replace(c.ToString(), string.Empty);
        }
        
        return TranslateUsingTable(sanitisedText, s_englishToSolresolTable);
    }

    public static string TranslateSolresolToEnglish(string text)
    {
        if (!LoadDictionary())
        {
            return "Bleh";
        }

        return TranslateUsingTable(text, s_solresolToEnglishTable);
    }

    public static string NoteIDsToSolresol(int[] noteIDs, int elementCount = -1)
    {
        string text = "";
        if (elementCount == -1)
        {
            elementCount = noteIDs.Length;
        }
        for(int i = 0; i < elementCount; i++)
        {
            int note = noteIDs[i];
            int scaleNote = MidiUtil.NoteOffsetToMajorScaleNote(note);
            text += s_solresolLetters[scaleNote + 1]; // -1 is a space, 0 is Do
        }
        return text;
    }

    public static int[] SolresolToNoteIds(string solresol)
    {
        List<int> noteIDList = new List<int>();
        string toRead = solresol;
        for (int attempt = 0; attempt < 100; attempt++)
        {
            for (int i = 0; i < s_solresolLetters.Length; i++)
            {
                bool matches = toRead.StartsWith(s_solresolLetters[i]);
                if (matches)
                {
                    toRead = toRead.Substring(s_solresolLetters[i].Length);
                    if (i == 0)
                    {
                        noteIDList.Add(-1); // -1 is a space
                    }
                    else
                    {
                        int noteID = MidiUtil.MajorScaleNoteToNoteOffset(i - 1);
                        noteIDList.Add(noteID);
                    }
                }
            }
        }
        return noteIDList.ToArray();
    }

    public static int[] FindLongestNoteWord(int[] noteIDs)
    {
        int[] longestWord = new int[0];
        List<int> currentWord = new List<int>();
        for (int i = 0; i < noteIDs.Length; i++)
        {
            if (noteIDs[i] >= 0)
            {
                currentWord.Add(noteIDs[i]);
            }
            else
            {
                if (currentWord.Count >= longestWord.Length) // Favour later words in the sentence
                {
                    longestWord = currentWord.ToArray();
                }
                currentWord.Clear();
            }
        }
        if (currentWord.Count >= longestWord.Length)
        {
            longestWord = currentWord.ToArray();
        }
        return longestWord;
    }
}
