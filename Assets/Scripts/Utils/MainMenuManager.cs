﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    //public TextMesh[] m_gameResetText;
    MidiInput m_midiInput;
    List<int> m_blackNotesDown;

    void Start()
    {
        //bool saveGameExists = AnySaveGames();
        //foreach(TextMesh mesh in m_gameResetText)
        //{
        //    mesh.gameObject.SetActive(saveGameExists);
        //}
        m_blackNotesDown = new List<int>();

        m_midiInput = GetComponent<MidiInput>();
        m_midiInput.MidiIOEvent += HandleMidiIOEvent;
    }

    bool AnySaveGames()
    {
        return SaveGameSystem.DoesSaveGameExist("Slot1");
    }

    void Update()
    {
        if (Input.anyKey)
        {
            // Was it any of the pc keys or mouse buttons?
            bool ignoreInput = false;
            foreach (KeyCode key in MidiInput.pcKeys)
            {
                if (Input.GetKey(key))
                {
                    ignoreInput = true;
                    break;
                }
            }
            for (int i = 0; i < 5; i++)
            {
                if (Input.GetMouseButton(i))
                {
                    ignoreInput = true;
                    break;
                }
            }

            if (!ignoreInput)
            {
                MidiInput.s_usingPCKeys = true;
                StartNewGame();
                //ContinueGame();
            }
        }
    }

    void ContinueGame()
    {
        bool loadedSaveGame = SaveGameSystem.LoadCurrentSaveGame();
        if (!loadedSaveGame)
        {
            AutoFade.LoadLevel(1, 1.0f, 1.0f, Color.black);
        }
    }

    void StartNewGame()
    {
        if (SaveGameSystem.DoesSaveGameExist(SaveGameSystem.s_currentSaveFile))
        {
            SaveGameSystem.DeleteSaveGame("Slot1");
        }
        ContinueGame();
    }

    private void HandleMidiIOEvent(int note, bool down, float velocity)
    {
        if (down)
        {
            bool saveGameExists = AnySaveGames();
            if (saveGameExists)
            {
                if (!MidiUtil.IsBlackNote(note))
                {
                    StartNewGame();
                }
                else
                {
                    m_blackNotesDown.Add(note);
                    if (m_blackNotesDown.Count >= 3)
                    {
                        ContinueGame();
                    }
                }
            }
            else
            {
                StartNewGame();
            }
        }
        else
        {
            if (MidiUtil.IsBlackNote(note))
            {
                m_blackNotesDown.Remove(note);
            }
        }
    }
}
