﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugSlowdownHelper : MonoBehaviour
{
#if UNITY_EDITOR
    bool m_slowingDown = false;
    int m_originalVSyncCount;
    int m_originalTargetFrameRate;

    public int m_slowDownFramerate = 10;
	
	void Update ()
	{
        if (Input.GetKeyDown(KeyCode.F2))
        {
            if (!m_slowingDown)
            {
                //m_originalVSyncCount = QualitySettings.vSyncCount;
                //m_originalTargetFrameRate = Application.targetFrameRate;

                //QualitySettings.vSyncCount = 0;  // VSync must be disabled
                //Application.targetFrameRate = m_slowDownFramerate;
                FindObjectOfType<MusicManager>().SetMusicSpeed(0.5f);
            }
            else
            {
                //QualitySettings.vSyncCount = m_originalVSyncCount;
                //Application.targetFrameRate = m_originalTargetFrameRate;
                FindObjectOfType<MusicManager>().SetMusicSpeed(1.0f);
            }
            m_slowingDown = !m_slowingDown;
        }
    }
#endif
}
