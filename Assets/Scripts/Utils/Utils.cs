﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public static class Utils
{
    public static double Lerp(double a, double b, double t)
    {
        return a + ((b - a) * t);
    }

    public static RaycastHit2D DebuggableRaycast(Vector2 position, Vector2 direction, Color? colour = null, int layerMask = -1)
    {
        Vector2 lineEnd = (position + direction);
        Debug.DrawLine(new Vector3(position.x, position.y), new Vector3(lineEnd.x, lineEnd.y), (colour == null ? Color.white : colour.Value));
        float length = direction.magnitude;
        return Physics2D.Raycast(position, direction, length, layerMask);
    }

    public static Vector2 Rotate90Clockwise(Vector2 value)
    {
        return new Vector2(value.y, -value.x);
    }

    // From https://support.unity3d.com/hc/en-us/articles/206486626-How-can-I-get-pixels-from-unreadable-textures-
    public static Color32[] GetUnreadableTextureData(Texture texture)
    {
        // Create a temporary RenderTexture of the same size as the texture
        RenderTexture tmp = RenderTexture.GetTemporary(
                            texture.width,
                            texture.height,
                            0,
                            RenderTextureFormat.Default,
                            RenderTextureReadWrite.Linear);

        // Blit the pixels on texture to the RenderTexture
        Graphics.Blit(texture, tmp);
        // Backup the currently set RenderTexture
        RenderTexture previous = RenderTexture.active;
        // Set the current RenderTexture to the temporary one we created
        RenderTexture.active = tmp;
        // Create a new readable Texture2D to copy the pixels to it
        Texture2D myTexture2D = new Texture2D(texture.width, texture.height);
        // Copy the pixels from the RenderTexture to the new Texture
        myTexture2D.ReadPixels(new Rect(0, 0, tmp.width, tmp.height), 0, 0);
        myTexture2D.Apply();
        // Reset the active RenderTexture
        RenderTexture.active = previous;
        // Release the temporary RenderTexture
        RenderTexture.ReleaseTemporary(tmp);

        // "myTexture2D" now has the same pixels from "texture" and it's readable.
        return myTexture2D.GetPixels32();
    }

    public static void FlipMeshNormalsZ(Mesh mesh)
    {
        Vector3[] meshNormals = mesh.normals;
        for (int i = 0; i < meshNormals.Length; i++)
        {
            meshNormals[i].z = -meshNormals[i].z;
        }
        mesh.normals = meshNormals;
        mesh.UploadMeshData(false);
    }

    public static void CreateAndAssignShadowMesh(GameObject gameObject, string shadowMeshAssetPath, string shadowMeshLevelName, string shadowMeshFilename, List<Vector3> verts, List<int> tris)
    {
        MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();
        MeshRenderer meshRenderer = gameObject.GetComponent<MeshRenderer>();
        if (meshFilter == null)
        {
            meshFilter = gameObject.AddComponent<MeshFilter>();
        }
        if (meshRenderer == null)
        {
            meshRenderer = gameObject.AddComponent<MeshRenderer>();
        }
        Mesh mesh = new Mesh();
        mesh.name = "New_shadow_mesh";
        mesh.SetVertices(verts);
        mesh.SetTriangles(tris, 0);
        mesh.RecalculateBounds();

#if UNITY_EDITOR
        string folderPath = shadowMeshAssetPath + Path.DirectorySeparatorChar + shadowMeshLevelName;
        if (!AssetDatabase.IsValidFolder(folderPath))
        {
            AssetDatabase.CreateFolder(shadowMeshAssetPath, shadowMeshLevelName);
        }
        string fullPath = folderPath + Path.DirectorySeparatorChar + shadowMeshFilename;
        //Debug.Log("Mesh = " + mesh + " asset path = " + fullPath);
        AssetDatabase.CreateAsset(mesh, fullPath);

        meshFilter.sharedMesh = mesh;
        meshRenderer.sharedMaterial = AssetDatabase.GetBuiltinExtraResource<Material>("Default-Diffuse.mat");
        meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
#endif
    }

    private static float s_pixelsPerUnit = 64.0f;
    private static float s_gridSize = 1.0f / s_pixelsPerUnit;

    public static float RoundElement(float value)
    {
        return Mathf.Round(value * s_pixelsPerUnit) * s_gridSize;
    }

    public static Vector3 RoundVector3(Vector3 value)
    {
        return new Vector3(RoundElement(value.x), RoundElement(value.y), RoundElement(value.z));
    }

    public static Dictionary<TValue, TKey> Reverse<TKey, TValue>(this IDictionary<TKey, TValue> source)
    {
        var dictionary = new Dictionary<TValue, TKey>();
        foreach (var entry in source)
        {
            if (!dictionary.ContainsKey(entry.Value))
                dictionary.Add(entry.Value, entry.Key);
        }
        return dictionary;
    }
}
