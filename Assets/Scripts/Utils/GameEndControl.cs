﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEndControl : MonoBehaviour
{
    public GameObject[] m_endGameText;
    public GameObject[] m_choiceText;
    Vector3[] m_choiceStartPositions;
    float m_timer = 0.0f;

    public float m_scrollStartTime = 5.0f;
    public float m_scrollSpeed = 1.0f;
    public float m_offset = 20.0f;
    public float m_rectTransformMultiplier = 100.0f;

	void Start ()
	{
        for (int i = 0; i < m_endGameText.Length; i++)
        {
            m_endGameText[i].gameObject.SetActive(true);
        }
        m_choiceStartPositions = new Vector3[m_choiceText.Length];
		for (int i = 0; i < m_choiceText.Length; i++)
        {
            m_choiceStartPositions[i] = m_choiceText[i].transform.position;
            float multiplier = m_choiceText[i].GetComponent<RectTransform>() != null ? m_rectTransformMultiplier : 1.0f;
            m_choiceText[i].transform.position += new Vector3(0.0f, m_offset * multiplier, 0.0f);
        }
	}
	
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.F3))
        {
            Debug.Log("Resetting game");
            AutoFade.LoadLevel(0, 1.0f, 1.0f, Color.black);
        }
        m_timer += Time.deltaTime;

        if (m_timer > m_scrollStartTime)
        {
            foreach(GameObject go in m_endGameText)
            {
                go.transform.position += new Vector3(0.0f, m_scrollSpeed * Time.deltaTime, 0.0f);
            }
            for (int i = 0; i < m_choiceText.Length; i++)
            {
                float multiplier = m_choiceText[i].GetComponent<RectTransform>() != null ? m_rectTransformMultiplier : 1.0f;
                m_choiceText[i].transform.position = Vector3.MoveTowards(m_choiceText[i].transform.position, m_choiceStartPositions[i], m_scrollSpeed * Time.deltaTime * multiplier);
            }
        }
	}
}
