﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tiled2Unity;
using System.IO;

[CustomTiledImporter]
public class CustomImporterSpawnPrefabs : ICustomTiledImporter
{
    public void HandleCustomProperties(GameObject gameObject, IDictionary<string, string> props)
    {
        if (props.ContainsKey("PrefabSpawn"))
        {
            string prefabName = props["PrefabSpawn"];
            Debug.Log("PrefabSpawn key = " + prefabName);

            GameObject toSpawn = Resources.Load<GameObject>("Prefabs" + Path.DirectorySeparatorChar + prefabName);
            MeshFilter[] meshFilters = gameObject.GetComponentsInChildren<MeshFilter>();

            Vector3[] quadVerts = new Vector3[6];
            foreach (MeshFilter filter in meshFilters)
            {
                Mesh mesh = filter.sharedMesh;
                // Find every quad and add a prefab in the middle, parenting to the gameObject with the meshfilter
                int[] triangles = mesh.triangles;
                Vector3[] verts = mesh.vertices;
                for (int i = 0; i < triangles.Length; i += 6)
                {
                    Vector3 minCorner = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
                    Vector3 maxCorner = new Vector3(float.MinValue, float.MinValue, float.MinValue);
                    for (int qv = 0; qv < 6; qv++)
                    {
                        quadVerts[qv] = verts[triangles[i + qv]];
                        minCorner = Vector3.Min(minCorner, quadVerts[qv]);
                        maxCorner = Vector3.Max(maxCorner, quadVerts[qv]);
                    }
                    Vector3 center = (minCorner + maxCorner) * 0.5f;

                    GameObject newObject = GameObject.Instantiate(toSpawn, filter.transform, false);
                    newObject.transform.position += center;
                }
            }
        }
    }
    public void CustomizePrefab(GameObject prefab)
    {

    }
}
