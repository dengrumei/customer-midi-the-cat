﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Fungus.EditorUtils;

[CustomEditor(typeof(MusicMenu))]
public class MusicMenuEditor : CommandEditor
{
    protected SerializedProperty textProp;
    protected SerializedProperty descriptionProp;
    protected SerializedProperty targetBlockProp;
    protected SerializedProperty choiceNotesProp;
    protected SerializedProperty overriddenNotesProp;

    protected virtual void OnEnable()
    {
        if (NullTargetCheck()) // Check for an orphaned editor instance
            return;

        textProp = serializedObject.FindProperty("text");
        descriptionProp = serializedObject.FindProperty("description");
        targetBlockProp = serializedObject.FindProperty("targetBlock");
        choiceNotesProp = serializedObject.FindProperty("choiceNotes");
        overriddenNotesProp = serializedObject.FindProperty("overriddenNotes");
    }

    public override void DrawCommandGUI()
    {
        var flowchart = FlowchartWindow.GetFlowchart();
        if (flowchart == null)
        {
            return;
        }

        serializedObject.Update();

        EditorGUILayout.PropertyField(textProp);

        EditorGUILayout.PropertyField(descriptionProp);

        BlockEditor.BlockField(targetBlockProp,
            new GUIContent("Target Block", "Block to call when option is selected"), 
            new GUIContent("<None>"), 
            flowchart);

        string translation = SolresolUtils.TranslateEnglishToSolresol(textProp.stringValue);
        EditorGUILayout.LabelField("Full Translation: " + translation);

        int[] noteIDs = new int[choiceNotesProp.arraySize];
        for (int i = 0; i < noteIDs.Length; i++)
        {
            noteIDs[i] = choiceNotesProp.GetArrayElementAtIndex(i).intValue;
        }
        if (!overriddenNotesProp.boolValue)
        {
            noteIDs = SolresolUtils.SolresolToNoteIds(translation);

            int[] longestWordNotes = SolresolUtils.FindLongestNoteWord(noteIDs);
            string longestWordEnglish = SolresolUtils.TranslateSolresolToEnglish(SolresolUtils.NoteIDsToSolresol(longestWordNotes));
            EditorGUILayout.LabelField("Longest Solresol Word: " + longestWordEnglish);


            noteIDs = longestWordNotes;
        }

        string noteIDsString = "";
        for (int i = 0; i < noteIDs.Length; i++)
        {
            if (i > 0)
            {
                noteIDsString += ", ";
            }
            noteIDsString += noteIDs[i];
        }
        EditorGUILayout.LabelField("NoteIDs: " + noteIDsString);
        overriddenNotesProp.boolValue = EditorGUILayout.Toggle("Overridden Notes", overriddenNotesProp.boolValue);
        //EditorGUILayout.PrefixLabel("Translation: " + translation);
        choiceNotesProp.ClearArray();
        choiceNotesProp.arraySize = noteIDs.Length;
        for (int i = 0; i < noteIDs.Length; i++)
        {
            choiceNotesProp.GetArrayElementAtIndex(i).intValue = noteIDs[i];
        }
        EditorGUILayout.PropertyField(choiceNotesProp, true);

        serializedObject.ApplyModifiedProperties();
    }
}
