﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MusicalKeyConfig))]
public class MusicalKeyConfigEditor : Editor
{
    SerializedProperty m_keyNotesFromC;
    SerializedProperty m_scaleNoteOffsets;

    void OnEnable()
    {
        m_keyNotesFromC = serializedObject.FindProperty("m_keyNotesFromC");
        m_scaleNoteOffsets = serializedObject.FindProperty("m_scaleNoteOffsets");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        //EditorGUILayout.PropertyField(m_keyNotesFromC);
        EditorGUILayout.BeginHorizontal();
        bool[] usedSet = new bool[12];

        int noteInScale = 0;
        for (int i = 0; i < m_keyNotesFromC.arraySize; i++)
        {
            usedSet[i] = m_keyNotesFromC.GetArrayElementAtIndex(i).intValue >= 0;
            if (MidiUtil.IsBlackNote(i))
            {
                GUI.color = new Color(0.25f, 0.25f, 0.25f);
            }
            else
            {
                GUI.color = Color.white;
            }
            bool value = EditorGUILayout.Toggle(usedSet[i]);
            if (value)
            {
                m_keyNotesFromC.GetArrayElementAtIndex(i).intValue = noteInScale;
                noteInScale++;
            }
            else
            {
                m_keyNotesFromC.GetArrayElementAtIndex(i).intValue = -1;
            }
        }
        
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        int keyNoteID = 0;
        m_scaleNoteOffsets.arraySize = noteInScale;
        string values = "";
        for (int i = 0; i < m_keyNotesFromC.arraySize; i++)
        {
            if (usedSet[i])
            {
                m_scaleNoteOffsets.GetArrayElementAtIndex(keyNoteID++).intValue = i;
                values += i + " ";
            }
        }
        EditorGUILayout.LabelField(values);
        EditorGUILayout.EndHorizontal();
        serializedObject.ApplyModifiedProperties();
    }
}
