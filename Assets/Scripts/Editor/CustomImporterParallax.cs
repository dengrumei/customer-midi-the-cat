﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tiled2Unity;
using System;

[CustomTiledImporter]
public class CustomImporterParallax : ICustomTiledImporter, GamingGarrison.ITilemapImportOperation
{
    public void HandleCustomProperties(GameObject gameObject, IDictionary<string, string> props)
    {
        if (props.ContainsKey("Parallax"))
        {
            string value = props["Parallax"];
            string[] sections = value.Split(':');

            if (sections.Length > 0)
            {
                ParallaxBehaviour parallax = gameObject.GetComponent<ParallaxBehaviour>();
                if (parallax == null)
                {
                    parallax = gameObject.AddComponent<ParallaxBehaviour>();
                }

                float parallaxValue;
                if (float.TryParse(sections[0], out parallaxValue))
                {
                    parallax.m_parallaxDistance = parallaxValue;
                }

                if (sections.Length > 1)
                {
                    float xOffsetValue;
                    if (float.TryParse(sections[1], out xOffsetValue))
                    {
                        parallax.m_myStartPosition = new Vector3(xOffsetValue, parallax.m_myStartPosition.y, parallax.m_myStartPosition.z);
                    }
                }

                if (sections.Length > 2)
                {
                    float yOffsetValue;
                    if (float.TryParse(sections[2], out yOffsetValue))
                    {
                        parallax.m_myStartPosition = new Vector3(parallax.m_myStartPosition.x, yOffsetValue, parallax.m_myStartPosition.z);
                    }
                }
            }
        }
    }
    public void CustomizePrefab(GameObject prefab)
    {
        
    }
}
