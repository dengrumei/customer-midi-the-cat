﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CustomImporterSpawnPrefabsOnTiles : GamingGarrison.ITilemapImportOperation
{
    public void HandleCustomProperties(GameObject gameObject, IDictionary<string, string> customProperties)
    {
        if (customProperties.ContainsKey("PrefabSpawn"))
        {
            string prefabName = customProperties["PrefabSpawn"];

            GameObject toSpawn = Resources.Load<GameObject>("Prefabs" + Path.DirectorySeparatorChar + prefabName);
            Tilemap tilemap = gameObject.GetComponent<Tilemap>();

            // Look for tile instances, and spawn a prefab on each tile
            for (int x = tilemap.cellBounds.xMin; x < tilemap.cellBounds.xMax; x++)
            {
                for (int y = tilemap.cellBounds.yMin; y < tilemap.cellBounds.yMax; y++)
                {
                    Vector3Int tileCoord = new Vector3Int(x, y, 0);
                    if (tilemap.HasTile(tileCoord))
                    {
                        Vector3 worldCoord = tilemap.layoutGrid.GetCellCenterWorld(tileCoord);
                        GameObject newObject = GameObject.Instantiate(toSpawn, gameObject.transform, false);
                        newObject.transform.position += worldCoord;
                    }
                }
            }
        }
    }
}
