﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tiled2Unity;
using System;

[CustomTiledImporter]
public class CustomImporterAddComponent : ICustomTiledImporter, GamingGarrison.ITilemapImportOperation
{
    public void HandleCustomProperties(GameObject gameObject, IDictionary<string, string> props)
    {
        // Simply add a component to our GameObject
        if (props.ContainsKey("AddComp"))
        {
            string typeName = props["AddComp"];
            try
            {
                //Debug.Log("Trying to add component of type " + typeName);
                Type componentType = Type.GetType(typeName + ",Assembly-CSharp");
                //Type componentType = typeof(LiftBehaviour); // Type.GetType("Assembly-CSharp." + typeName);
                //Debug.Log("Found type " + componentType.AssemblyQualifiedName);
                Component newComponent = gameObject.AddComponent(componentType);

                Debug.Log("Added " + componentType +" from Tiled! Yey!");
                if (newComponent is TileObject)
                {
                    TileObject tile = (TileObject)newComponent;
                    tile.HandleCustomProperties(props);
                }
            }
            catch(Exception e)
            {
                Debug.Log(e.Message);
            }
        }
    }

    public void CustomizePrefab(GameObject prefab)
    {
        // Do nothing
    }
}
