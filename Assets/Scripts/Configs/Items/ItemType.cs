﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemType : ScriptableObject
{
    public string m_name;
    public Sprite m_image;
}
