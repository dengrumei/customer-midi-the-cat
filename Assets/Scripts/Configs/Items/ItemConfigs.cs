﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemConfigs : ScriptableObject
{
    public ItemType[] m_itemTypes;

    public ItemType GetItem(string name)
    {
        for(int i = 0; i < m_itemTypes.Length; i++)
        {
            if (m_itemTypes[i].m_name.Equals(name))
            {
                return m_itemTypes[i];
            }
        }
        return null;
    }
}
