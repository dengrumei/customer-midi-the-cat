﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActionConfig : ScriptableObject
{
    public string m_name;
    public string m_tutorialDescription;
    public InputPattern m_inputPattern;
    public bool m_rematchOnCompletion;
    public string m_animationName;
    public bool m_startsLearnt = false;

    public abstract void DoAction(ref PlayerControl.State state, MoveStorage moveStore);
    public abstract bool IsValidInState(PlayerControl.State state);
    public virtual bool ShouldShowTutorial(PlayerControl.State state) { return false; }


    public void Move(PlayerControl.State newState, Vector3 movement, MoveStorage moveStore, MoveStorage.MoveType moveType = MoveStorage.MoveType.Walk, int direction = 0)
    {
        MovementUtils.Move(newState, movement, moveStore, moveType, direction);
    }

    public RaycastHit2D DebuggableRaycast(Vector2 position, Vector2 direction, Color? colour = null, int? mask = null)
    {
        return MovementUtils.DebuggableRaycast(position, direction, colour, mask);
    }
}
