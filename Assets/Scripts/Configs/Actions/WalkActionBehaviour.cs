﻿using UnityEngine;

public class WalkActionBehaviour : ActionConfig
{
    public override void DoAction(ref PlayerControl.State state, MoveStorage moveStore)
    {
        MovementUtils.HandleGroundMovement(state.m_lastDirection, 0, state, moveStore);
    }

    public override bool IsValidInState(PlayerControl.State state)
    {
        return state.m_stepState != PlayerControl.StepState.Off || MovementUtils.IsOnGround(state);
        //return (state.m_stepState == PlayerControl.StepState.UpEnd
        //    || state.m_stepState == PlayerControl.StepState.DownEnd
        //    || state.m_stepState == PlayerControl.StepState.Off) && MovementUtils.IsOnGround(state);
    }

    public override bool ShouldShowTutorial(PlayerControl.State state)
    {
        return true;
    }
}
