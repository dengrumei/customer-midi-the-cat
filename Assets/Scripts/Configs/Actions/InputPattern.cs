﻿using System;

[Serializable]
public class InputPattern
{
    public NoteInput[] m_inputs;
    //public bool m_listenForNoteUp;

    bool[] m_matchedAlready; // YOU HAVE TO INITIALISE THESE BECAUSE UNITY WON'T DO IT FOR ScriptableObject internals!
    int m_matchProgress;
    InputEvent? m_startEvent;

    public void Initialise()
    {
        if (m_inputs != null)
        {
            m_matchedAlready = new bool[m_inputs.Length];
            for (int i = 0; i < m_matchedAlready.Length; i++)
            {
                m_matchedAlready[i] = false;
            }
            m_matchProgress = 0;
            m_startEvent = null;
        }
    }

    /// <summary>
    /// Returns if the input progressed the match
    /// </summary>
    /// <param name="newEvent"></param>
    /// <returns></returns>
    public bool AddEvent(InputEvent newEvent)
    {
        /*if (newEvent.m_down == m_listenForNoteUp)
        {
            return false;
        }*/
        int progress = m_matchProgress;
        bool shouldResetSearch = DoSearchStep(newEvent);
        int newProgress = m_matchProgress;
        
        if (m_matchProgress >= m_matchedAlready.Length)
        {
            return true;
        }

        if (shouldResetSearch)
        {
            ClearProgress();
            progress = m_matchProgress;
            DoSearchStep(newEvent); // The re-matched search could make progress, so that should count as a match
            newProgress = m_matchProgress;
        }
        return newProgress > progress;
    }
    public bool IsSearchComplete()
    {
        return m_matchProgress >= m_matchedAlready.Length;
    }

    bool DoSearchStep(InputEvent newEvent)
    {
        bool shouldResetSearch = false;
        for (int j = 0; j < m_inputs.Length; j++)
        {
            if (m_matchedAlready[j])
            {
                continue;
            }
            NoteInput expectedInput = m_inputs[j];
            bool matchesPress = expectedInput.MatchesPress(newEvent.m_down);
            /*if (m_startEvent == null)
            {
                if (matchesPress)
                {
                    m_startEvent = newEvent; // Might be a problem to only record the first press of the search, even if it's not valid
                }
                else
                {
                    continue;
                }
            }*/

            // Should only store the first event that fully matches something.  Just need to use dummy until then.
            InputEvent previousEvent = newEvent;
            if (m_startEvent != null)
            {
                previousEvent = m_startEvent.Value;
            }

            bool matchesNote = expectedInput.MatchesNote(newEvent.m_noteID, previousEvent.m_noteID);
            bool matchesTime = expectedInput.MatchesTime(newEvent.m_beatTime, previousEvent.m_beatTime);
            if (matchesPress && matchesNote && matchesTime)
            {
                if (m_startEvent == null)
                {
                    m_startEvent = newEvent;
                }
                m_matchedAlready[j] = true;
                m_matchProgress++;
                shouldResetSearch = false;
                break;
            }
            else if (matchesPress) // If we're expecting a certain press, but we get a totally wrong press, we should start our matching again
            {
                shouldResetSearch = true;
            }
        }
        return shouldResetSearch;
    }

    public void ClearProgress()
    {
        m_matchProgress = 0;
        for (int i = 0; i < m_matchedAlready.Length; i++)
        {
            m_matchedAlready[i] = false;
        }
        m_startEvent = null;
    }

    public bool Matches(CircularBuffer<InputEvent> history)
    {
        if (m_inputs == null || m_inputs.Length == 0)
        {
            return true;
        }

        InputEvent[] events = history.m_history;

        // Only look at the end history to check it matches the inputs
        bool[] matchSuccesses = new bool[m_inputs.Length];
        for (int i = m_inputs.Length - 1; i >= 0; i--)
        {
            InputEvent eventToMatch = events[i];
            InputEvent startEvent = events[0];

            bool matchedToAnything = false;
            for (int j = 0; j < m_inputs.Length; j++)
            {
                if (matchSuccesses[j])
                {
                    continue;
                }
                NoteInput expectedInput = m_inputs[i];
                bool matchesPress = expectedInput.MatchesPress(eventToMatch.m_down);
                bool matchesNote = expectedInput.MatchesNote(eventToMatch.m_noteID, startEvent.m_noteID);
                bool matchesTime = expectedInput.MatchesTime(eventToMatch.m_beatTime, startEvent.m_beatTime);
                if (matchesPress && matchesNote && matchesTime)
                {
                    matchSuccesses[j] = true;
                    matchedToAnything = true;
                    break;
                }
            }

            if (!matchedToAnything)
            {
                return false;
            }
        }

        return true;
    }

    public double GetMaxPatternTimeLength()
    {
        if (m_inputs.Length <= 1)
        {
            return float.MaxValue;
        }
        double totalTime = 0.0f;
        for (int i = 0; i < m_inputs.Length; i++)
        {
            NoteInput expectedInput = m_inputs[i];
            if (expectedInput.m_timeMode == NoteInput.TimeMode.Any)
            {
                return float.MaxValue;
            }
            else if (expectedInput.m_timeMode == NoteInput.TimeMode.Relative)
            {
                totalTime = Math.Max(totalTime, expectedInput.m_timeValue + expectedInput.m_timeValueLeeway);
            }
        }
        return totalTime;
    }
}
