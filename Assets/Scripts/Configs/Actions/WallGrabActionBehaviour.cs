﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallGrabActionBehaviour : ActionConfig
{
    public override void DoAction(ref PlayerControl.State state, MoveStorage moveStore)
    {
        state.m_wallState = PlayerControl.WallState.Hanging;
        Move(state, Vector3.zero, moveStore, MoveStorage.MoveType.WallGrab, state.m_lastDirection);
    }

    public override bool IsValidInState(PlayerControl.State state)
    {
        return state.m_stepState == PlayerControl.StepState.Off
            && state.m_wallState == PlayerControl.WallState.Off
            && !MovementUtils.IsOnGround(state)
            && DebuggableRaycast(state.GetRaycastPosition(), new Vector2(state.m_lastDirection, 0.0f), Color.red, MovementUtils.s_obstacleOnlyMask).collider != null;
    }
    public override bool ShouldShowTutorial(PlayerControl.State state)
    {
        return true;
    }
}
