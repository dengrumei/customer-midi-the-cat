﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnActionBehaviour : ActionConfig
{
    public override void DoAction(ref PlayerControl.State state, MoveStorage moveStore)
    {
        state.m_sneaking = true;
        state.m_lastDirection = -state.m_lastDirection;
        state.m_momentum = 1;

        if (state.m_stepState != PlayerControl.StepState.Off)
        {
            MovementUtils.TurnAroundOnSteps(state, moveStore);
        }
        else
        {
            if (state.m_wallState == PlayerControl.WallState.Hanging)
            {
                Move(state, Vector3.zero, moveStore, MoveStorage.MoveType.WallRelease, state.m_lastDirection);
                state.m_wallState = PlayerControl.WallState.Off;
            }
            else if (!MovementUtils.IsOnGround(state) && DebuggableRaycast(state.GetRaycastPosition(), new Vector2(state.m_lastDirection, 0.0f), Color.red, MovementUtils.s_obstacleOnlyMask).collider != null) // Try and grab wall if you turned towards one
            {
                state.m_wallState = PlayerControl.WallState.Hanging;
                Move(state, Vector3.zero, moveStore, MoveStorage.MoveType.WallGrab, state.m_lastDirection);
            }
            else
            {
                Move(state, Vector3.zero, moveStore, MoveStorage.MoveType.Turn, state.m_lastDirection);
            }
        }
    }

    public override bool IsValidInState(PlayerControl.State state)
    {
        return true;// MovementUtils.IsOnGround(state) || state.m_wallState == PlayerControl.WallState.Hanging;
    }

    public override bool ShouldShowTutorial(PlayerControl.State state)
    {
        if (state.m_stepState != PlayerControl.StepState.Off)
        {
            return false;
        }
        RaycastHit2D ahead = DebuggableRaycast(state.GetRaycastPosition(), new Vector2(state.m_lastDirection, 0.0f), Color.blue, MovementUtils.s_obstacleOnlyMask);
        RaycastHit2D aheadHigh = DebuggableRaycast(state.GetRaycastPosition() + new Vector2(0.0f, 1.0f), new Vector2(state.m_lastDirection, 0.0f), Color.blue, MovementUtils.s_obstacleOnlyMask);
        RaycastHit2D below = DebuggableRaycast(state.GetRaycastPosition(), new Vector2(0.0f, -0.5f), Color.blue, MovementUtils.s_obstacleOnlyMask);
        return ahead.collider != null
            && aheadHigh.collider != null
            && below.collider != null && below.transform.GetComponentInParent<LiftBehaviour>() == null;
    }
}
