﻿using UnityEngine;

public class AscendingStepsActionBehaviour : ActionConfig
{
    public override void DoAction(ref PlayerControl.State state, MoveStorage moveStore)
    {
        MovementUtils.HandleGroundMovement(state.m_lastDirection, 1, state, moveStore);
    }

    public override bool IsValidInState(PlayerControl.State state)
    {
        return false;
        //if (state.m_stepState == PlayerControl.StepState.Off)
        //{
        //    if (MovementUtils.WillHitBranchingSteps(state.m_lastDirection, state))
        //    {
        //        return true;
        //    }
        //}
        //return (
        //    state.m_stepState == PlayerControl.StepState.GoingUp
        //    || state.m_stepState == PlayerControl.StepState.UpEnd
        //    ) && MovementUtils.IsOnGround(state);
    }

    public override bool ShouldShowTutorial(PlayerControl.State state)
    {
        return state.m_stepState == PlayerControl.StepState.GoingUp;
    }
}
