﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class NoteInput
{
    public enum NoteMode
    {
        AnyInKey,
        RelativeInKey,
        RelativeInKeyDirectionless
    }

    public enum TimeMode
    {
        Any,
        Relative
    }

    public enum PressMode
    {
        Any,
        Matching
    }


    public NoteMode m_noteMode;
    public int m_noteValue;
    public MusicalKeyConfig m_keyOverride;

    public TimeMode m_timeMode;
    public double m_timeValue;
    public double m_timeValueLeeway;

    public PressMode m_pressMode;
    public bool m_pressValue = true;

    public bool MatchesPress(bool inputValue)
    {
        switch (m_pressMode)
        {
            case PressMode.Any:
                return true;
            case PressMode.Matching:
                return inputValue == m_pressValue;
            default:
                return true;
        }
    }

    public bool NeedsPreviousInput()
    {
        return !(m_timeMode == TimeMode.Any && m_noteMode == NoteMode.AnyInKey);
    }

    public bool MatchesTime(double inputValue, double lastInputValue)
    {
        switch (m_timeMode)
        {
            case TimeMode.Any:
                return true;
            case TimeMode.Relative:
                double startOfLeeway = (lastInputValue + m_timeValue) - (m_timeValueLeeway * 0.5);
                double endOfLeeway = startOfLeeway + m_timeValueLeeway;
                return inputValue >= startOfLeeway && inputValue <= endOfLeeway;
            default:
                return true;
        }
    }

    public double GetExampleTime(double lastTime)
    {
        switch (m_timeMode)
        {
            case TimeMode.Any:
                return lastTime + 1;
            case TimeMode.Relative:
                return lastTime + m_timeValue;
            default:
                return lastTime;
        }
    }

    public bool MatchesNote(int inputValue, int lastInputValue)
    {
        int[] keyNotes = (m_keyOverride != null ? m_keyOverride.m_keyNotesFromC : MidiUtil.MajorScaleNotes);
        switch (m_noteMode)
        {
            case NoteMode.AnyInKey:
                return MidiUtil.ScaleKeysBetweenNotes(inputValue, inputValue, keyNotes) == 1;
            case NoteMode.RelativeInKey:
            {
                if (Mathf.Sign(inputValue - lastInputValue) != Mathf.Sign(m_noteValue))
                {
                    return false;
                }
                int scaleKeysBetween = MidiUtil.ScaleKeysBetweenNotes(lastInputValue, inputValue, keyNotes);
                return (scaleKeysBetween - 1) == Mathf.Abs(m_noteValue);
            }
            case NoteMode.RelativeInKeyDirectionless:
            {
                int scaleKeysBetween = MidiUtil.ScaleKeysBetweenNotes(lastInputValue, inputValue, keyNotes);
                return (scaleKeysBetween - 1) == m_noteValue;
            }
            default:
                return true;
        }
    }

    public int[] GetNoteToScaleNote()
    {
        return (m_keyOverride != null ? m_keyOverride.m_keyNotesFromC : MidiUtil.MajorScaleNotes);
    }

    public int[] GetScaleNoteToNote()
    {
        return (m_keyOverride != null ? m_keyOverride.m_scaleNoteOffsets : MidiUtil.s_MajorScaleNoteToNoteOffset);
    }

    public int GetExampleNote(int lastNote)
    {
        int[] keyNotes = GetNoteToScaleNote();
        int[] scaleNoteOffsets = GetScaleNoteToNote();
        switch(m_noteMode)
        {
            case NoteMode.AnyInKey:
                return 60 + scaleNoteOffsets[UnityEngine.Random.Range(0, scaleNoteOffsets.Length)];
            case NoteMode.RelativeInKey:
            case NoteMode.RelativeInKeyDirectionless:
                int originalScaleNote = MidiUtil.GetScaleNoteFromNote(lastNote, keyNotes);
                int newScaleNote = originalScaleNote + m_noteValue;
                //if (m_noteMode == NoteMode.RelativeInKeyDirectionless) // Just show directionless as always going up
                //{
                //    if (UnityEngine.Random.value > 0.5f)
                //    {
                //        newScaleNote = originalScaleNote - m_noteValue;
                //    }
                //}
                int newNote = lastNote;
                if (newScaleNote >= scaleNoteOffsets.Length)
                {
                    newScaleNote -= scaleNoteOffsets.Length;
                    newNote += 12;
                }
                else if (newScaleNote < 0)
                {
                    newScaleNote += scaleNoteOffsets.Length;
                    newNote -= 12;
                }
                newNote += (scaleNoteOffsets[newScaleNote] - scaleNoteOffsets[originalScaleNote]);
                return newNote;
            default:
                return lastNote;
        }
    }
}
