﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallAscendActionBehaviour : ActionConfig
{
    public override void DoAction(ref PlayerControl.State state, MoveStorage moveStore)
    {
        int verticalDirection = 1;

        MovementUtils.HandleWallMovement(verticalDirection, state, moveStore);
    }

    public override bool IsValidInState(PlayerControl.State state)
    {
        return state.m_wallState == PlayerControl.WallState.Hanging;
    }

    public override bool ShouldShowTutorial(PlayerControl.State state)
    {
        return !MovementUtils.WouldHitFloorOrCeiling(1, state);
    }
}
