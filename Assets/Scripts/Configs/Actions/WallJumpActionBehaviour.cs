﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallJumpActionBehaviour : ActionConfig
{
    public override void DoAction(ref PlayerControl.State state, MoveStorage moveStore)
    {
        // Walljumping turns you around
        state.m_lastDirection = -state.m_lastDirection;

        MovementUtils.HandleJumping(2, state, moveStore);
    }

    public override bool IsValidInState(PlayerControl.State state)
    {
        return state.m_wallState == PlayerControl.WallState.Hanging;
    }
    public override bool ShouldShowTutorial(PlayerControl.State state)
    {
        return true;
    }
}
