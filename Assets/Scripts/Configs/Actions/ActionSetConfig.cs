﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionSetConfig : ScriptableObject
{
    public ActionConfig[] m_actionConfigs;
}
