﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SneakActionBehaviour : ActionConfig
{
    public override void DoAction(ref PlayerControl.State state, MoveStorage moveStore)
    {
        state.m_sneaking = true;
        state.m_momentum = 1;
        MovementUtils.HandleGroundMovement(state.m_lastDirection, 0, state, moveStore);
        state.m_momentum = 1;
    }

    public override bool IsValidInState(PlayerControl.State state)
    {
        return (state.m_stepState != PlayerControl.StepState.Off || MovementUtils.IsOnGround(state)) && state.m_canSneak;
        //return (state.m_stepState == PlayerControl.StepState.UpEnd
        //    || state.m_stepState == PlayerControl.StepState.DownEnd
        //    || state.m_stepState == PlayerControl.StepState.Off) && MovementUtils.IsOnGround(state) && state.m_canSneak;
    }

    public override bool ShouldShowTutorial(PlayerControl.State state)
    {
        return true;
    }
}
