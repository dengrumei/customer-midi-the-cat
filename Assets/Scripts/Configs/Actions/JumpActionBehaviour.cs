﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpActionBehaviour : ActionConfig
{
    public override void DoAction(ref PlayerControl.State state, MoveStorage moveStore)
    {
        MovementUtils.HandleJumping(Mathf.Min(state.m_momentum + 1, 4), state, moveStore);
    }

    public override bool IsValidInState(PlayerControl.State state)
    {
        return state.m_stepState == PlayerControl.StepState.Off && MovementUtils.IsOnGround(state);
    }

    public override bool ShouldShowTutorial(PlayerControl.State state)
    {
        RaycastHit2D gapTestAhead = DebuggableRaycast(state.GetRaycastPosition() + new Vector2(state.m_lastDirection, 0.0f), new Vector2(0.0f, -0.5f));
        RaycastHit2D gapTest2Ahead = DebuggableRaycast(state.GetRaycastPosition() + new Vector2(state.m_lastDirection + state.m_lastDirection, 0.0f), new Vector2(0.0f, -0.5f));
        RaycastHit2D obstacleInFront = DebuggableRaycast(state.GetRaycastPosition(), new Vector2(state.m_lastDirection, 0.0f), Color.red, MovementUtils.s_obstacleOnlyMask);
        RaycastHit2D obstacle1TileAboveIt = DebuggableRaycast(state.GetRaycastPosition() + new Vector2(0.0f, 1.0f), new Vector2(state.m_lastDirection, 0.0f), Color.red, MovementUtils.s_obstacleOnlyMask);
        return (gapTestAhead.collider == null && gapTest2Ahead.collider != null) || (obstacleInFront.collider != null  && obstacle1TileAboveIt.collider == null);
    }
}
