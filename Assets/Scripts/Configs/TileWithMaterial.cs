﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu]
public class TileWithMaterial : Tile
{
    public Material m_material;

    public override void RefreshTile(Vector3Int position, ITilemap tilemap)
    {
        base.RefreshTile(position, tilemap);
        if (tilemap.GetType().ToString().Equals("UnityEditor.EditorPreviewTilemap"))
        {
            return;
        }

        Debug.Log("Tilemap = " + tilemap + " type = " + tilemap.GetType());
        // Now a material tile needs to be put in it's own tilemap based on material
        if (tilemap.GetComponent<TilemapRenderer>().sharedMaterial == m_material)
        {
            // We're fine being in here
            return;
        }

        Tilemap tm = tilemap.GetComponent<Tilemap>();
        if (tm.GetComponent<TilemapRenderer>().sharedMaterial != m_material)
        {
            return;
        }
        Debug.Log("TM = " + tm + " type = " + tm.GetType());
        tm.SetTile(position, null); // Empty this tile, as we're putting it elsewhere

        GameObject gameObject = tm.gameObject;
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            Tilemap childTM = gameObject.transform.GetChild(i).GetComponent<Tilemap>();
            if (childTM != null)
            {
                TilemapRenderer childTMR = childTM.GetComponent<TilemapRenderer>();
                if (childTMR.sharedMaterial == m_material)
                {
                    //childTM.SetTile(position, this);
                    return;
                }
            }
        }

        // If it can't find a valid timemap, we need to create a new one and set the renderer's material
        GameObject newGO = new GameObject(tm.name + "-" + m_material.name, typeof(Tilemap), typeof(TilemapRenderer));
        newGO.transform.SetParent(gameObject.transform);
        TilemapRenderer newRenderer = newGO.GetComponent<TilemapRenderer>();
        newRenderer.sharedMaterial = m_material;
        newRenderer.sortingOrder = tm.GetComponent<TilemapRenderer>().sortingOrder;
        newGO.GetComponent<Tilemap>().SetTile(position, this);
    }

    public override bool StartUp(Vector3Int position, ITilemap tilemap, GameObject go)
    {
        if (!tilemap.GetType().ToString().Equals("UnityEditor.EditorPreviewTilemap"))
        {
            Debug.Log("Start up in tilemap " + tilemap.ToString());
        }
        return true;
        //return base.StartUp(position, tilemap, go);
    }

    public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
    {
        base.GetTileData(position, tilemap, ref tileData);
        if (!tilemap.GetType().ToString().Equals("UnityEditor.EditorPreviewTilemap"))
        {
            Debug.Log("Get tile data in tilemap " + tilemap.ToString());
        }
    }


    public override bool GetTileAnimationData(Vector3Int position, ITilemap tilemap, ref TileAnimationData tileAnimationData)
    {
        if (!tilemap.GetType().ToString().Equals("UnityEditor.EditorPreviewTilemap"))
        {
            Debug.Log("GetTileAnimationData in tilemap " + tilemap);
        }
        return base.GetTileAnimationData(position, tilemap, ref tileAnimationData);
    }
}
