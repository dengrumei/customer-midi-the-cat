﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicalKeyConfig : ScriptableObject
{
    public int[] m_keyNotesFromC = new int[12] { 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };

    public int[] m_scaleNoteOffsets;
}
