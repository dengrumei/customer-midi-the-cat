﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonPanelBehaviour : MonoBehaviour
{
    PlayerControl m_player;
    MidiInput m_midiInput;

    public Text m_text;

    ButtonBehaviour m_button;
    int m_nextNote;

    static ButtonPanelBehaviour m_instance;

    public static void Show(ButtonBehaviour button)
    {
        if (m_instance == null)
        {
            m_instance = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/ButtonPanel"), GameObject.FindWithTag("WorldCanvas").transform).GetComponent<ButtonPanelBehaviour>();
            m_instance.m_button = button;
        }
    }

    public static void Hide()
    {
        if (m_instance != null)
        {
            GameObject.Destroy(m_instance.gameObject);
            m_instance = null;
        }
    }

    void Start()
    {
        m_player = FindObjectOfType<PlayerControl>();
        m_midiInput = FindObjectOfType<MidiInput>();

        transform.position = m_player.transform.position + new Vector3(0.0f, -2.0f, 0.0f);
        m_midiInput.MidiIOEvent += HandleMidiIOEvent;

        UpdateText();
    }

    void UpdateText()
    {
        string midiString = "";
        for (int i = 0; i < m_button.m_notesWanted.Length; i++)
        {
            int note = m_button.m_notesWanted[i];
            midiString += MidiUtil.NoteToString(note);
        }
        m_text.text = midiString;
    }

    void OnDestroy()
    {
        m_midiInput.MidiIOEvent -= HandleMidiIOEvent;
    }

    void HandleMidiIOEvent(int note, bool down, float velocity)
    {
        Debug.Log("Midi note for ButtonPanelBehaviour");

        if (down)
        {
            if ((note % 12) == (m_button.m_notesWanted[m_nextNote] % 12))
            {
                if (m_nextNote + 1 >= m_button.m_notesWanted.Length)
                {
                    // Finished!
                    m_button.PushButton();
                    Hide();
                }
                else
                {
                    m_nextNote++;
                }
            }
            else
            {
                m_nextNote = 0;
            }
        }
    }
}
