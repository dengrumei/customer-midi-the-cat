﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInfoBehaviour : MonoBehaviour
{
    public string m_levelName;

	void Start ()
    {
        InfoTextBehaviour infoText = FindObjectOfType<InfoTextBehaviour>();
        infoText.ShowText(m_levelName, 5.0f, Color.white);
	}
	
	void Update ()
	{
		
	}
}
