﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoTextBehaviour : MonoBehaviour
{
    Text m_text;

    float m_textTimer = 1.0f;
    float m_textTime = 0.0f;
    public float m_fadeInTime = 0.1f;
    public float m_fadeOutTime = 1.0f;
    Color m_textColour = Color.white;

    public AnimationCurve m_textFadeIn;
    public AnimationCurve m_textFadeOut;

    void Awake()
    {
        m_text = GetComponent<Text>();
    }

    public void ShowText(string text, float time, Color colour)
    {
        m_text.text = text;
        m_textTime = time + m_fadeInTime + m_fadeOutTime;
        m_textTimer = 0.0f;
        m_textColour = colour;
    }
    public void StopText()
    {
        float startFadeOut = m_textTime - m_fadeOutTime;
        if (m_textTimer < startFadeOut)
        {
            m_textTimer = startFadeOut;
        }
    }

    private void Update()
    {
        m_textTimer += Time.deltaTime;

        float alpha = 1.0f;
        float startOfFadeOut = m_textTime - m_fadeOutTime;
        if (m_textTimer < m_fadeInTime)
        {
            float t = m_textTimer / m_fadeInTime;
            alpha = m_textFadeIn.Evaluate(t);
        }
        else if (m_textTimer < startOfFadeOut)
        {
            alpha = 1.0f;
        }
        else if (m_textTimer < m_textTime)
        {
            float t = (m_textTimer - startOfFadeOut) / m_fadeOutTime;
            alpha = m_textFadeOut.Evaluate(t);
        }
        else
        {
            alpha = 0.0f;
        }
        
        m_text.color = new Color(m_textColour.r, m_textColour.g, m_textColour.b, alpha);
        m_text.enabled = alpha > 0;
    }
}
