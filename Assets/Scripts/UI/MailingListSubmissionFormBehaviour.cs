﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Fiftytwo;

public class MailingListSubmissionFormBehaviour : MonoBehaviour
{
    public InputField m_emailInput;
    public InputField m_feedbackInput;

    public GameObject[] m_toDisableWhenComplete;
    public GameObject[] m_toEnableWhenComplete;

    private WWWForm m_form;

    private IEnumerator SendToMailChimp()
    {
        m_form = new WWWForm();
        m_form.AddField("MERGE0", m_emailInput.text);
        m_form.AddField("MERGE1", m_feedbackInput.text);

        m_form.AddField("u", "1f271a9e997836765ea218377");
        m_form.AddField("id", "bef723ffb7");

        using (var w = UnityWebRequest.Post("https://gaminggarrison.us18.list-manage.com/subscribe/post?u=1f271a9e997836765ea218377&id=bef723ffb7", m_form))
        {
            w.chunkedTransfer = false;
            yield return w.SendWebRequest();
            if (w.isNetworkError || w.isHttpError)
            {
                Debug.LogError(w.error);
                OnFailure();
            }
            else
            {
                Debug.Log("Submission successful!");
                OnSuccess();
            }
        }
        //
        //WWW w = new WWW("https://gaminggarrison.us18.list-manage.com/subscribe/post", m_form);
        //yield return w;
        //
        //if (!string.IsNullOrEmpty(w.error))
        //{
        //    Debug.LogError("Error: " + w.error);
        //}
        //else
        //{
        //    Debug.Log("Form submitted successfully!");
        //}
    }

    public void OnSuccess()
    {
        foreach (GameObject go in m_toDisableWhenComplete)
        {
            go.SetActive(false);
        }
        foreach (GameObject go in m_toEnableWhenComplete)
        {
            go.SetActive(true);
        }
    }
    public void OnFailure()
    {
        foreach (GameObject go in m_toDisableWhenComplete)
        {
            go.SetActive(false);
        }
        foreach (GameObject go in m_toEnableWhenComplete)
        {
            go.SetActive(true);
        }
    }

    public void PressedSubmit()
    {
        //StartCoroutine(SendToMailChimp());
        MailChimpSubscriber subscriber = GetComponent<MailChimpSubscriber>();
        subscriber.Subscribe(m_emailInput.text, m_feedbackInput.text);
    }
}
