﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomToInventoryBehaviour : MonoBehaviour
{
    ItemType m_itemType;
    InventoryBehaviour m_inventory;
    Vector3 m_velocity;

    public Vector3 m_startVelocity;
    public float m_speedUp;

    public void SetItemType(ItemType type)
    {
        m_itemType = type;
        GetComponent<SpriteRenderer>().sprite = type.m_image;
    }

    void Start()
    {
        m_inventory = FindObjectOfType<InventoryBehaviour>();
        m_velocity = m_startVelocity;
    }

	void LateUpdate ()
	{
        if (m_itemType != null)
        {
            Vector3 targetPosition = m_inventory.GetItemUIPosition(m_itemType);
            Vector3 toTarget = targetPosition - transform.position;
            float distance = toTarget.magnitude;
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref m_velocity, 1.0f * distance / m_speedUp);
            if (distance < 0.1f)
            {
                Destroy(gameObject);
            }
        }
	}
}
