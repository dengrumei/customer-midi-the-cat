﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterPanelBehaviour : MonoBehaviour
{
    TeleporterBehaviour m_teleporter;

    PlayerControl m_player;
    TeleporterBehaviour[] m_teleporters;
    MidiInput m_midiInput;

    public KeyboardGrid m_keyboard;
    public GameObject m_cancelText;

    static TeleporterPanelBehaviour s_instance;

    public static void Show(TeleporterBehaviour teleporter)
    {
        if (s_instance == null)
        {
            s_instance = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/TeleporterPanel"), GameObject.FindWithTag("WorldCanvas").transform).GetComponent<TeleporterPanelBehaviour>();
            s_instance.m_teleporter = teleporter;
        }
    }

    public static void Hide()
    {
        if (s_instance != null)
        {
            GameObject.Destroy(s_instance.gameObject);
            s_instance = null;
        }
    }

    void Start()
    {
        m_player = FindObjectOfType<PlayerControl>();
        m_player.SetIgnoreWhiteInputs(true);
        m_midiInput = FindObjectOfType<MidiInput>();

        transform.position = m_player.transform.position + new Vector3(0.0f, -0.5f, 0.0f);
        m_midiInput.MidiIOEvent += HandleMidiIOEvent;

        m_teleporters = FindObjectsOfType<TeleporterBehaviour>();
        foreach(TeleporterBehaviour teleporter in m_teleporters)
        {
            if (teleporter == m_teleporter)
            {
                continue;
            }
            int id = teleporter.m_teleporterID;
            m_keyboard.SetNoteHighlighted(60 + MidiUtil.MajorScaleNoteToNoteOffset(id), true);
        }

        m_cancelText.SetActive (m_teleporter.m_cancelable);
    }

    void OnDestroy()
    {
        m_midiInput.MidiIOEvent -= HandleMidiIOEvent;
    }

    void HandleMidiIOEvent(int note, bool down, float velocity)
    {
        if (down)
        {
            foreach (TeleporterBehaviour teleporter in m_teleporters)
            {
                if (teleporter != m_teleporter && MidiUtil.NoteOffsetToMajorScaleNote(note) == teleporter.m_teleporterID)
                {
                    m_teleporter.TeleportToDestination(teleporter);
                    Hide();
                    break;
                }
            }
            if (MidiUtil.IsBlackNote(note) && m_cancelText.activeSelf)
            {
                Hide();
                m_player.SetIgnoreWhiteInputs(false);
            }
        }
    }
}
