﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CommandInfo("", "MusicMenu", "Displays a musical choice")]
public class MusicMenu : Command, ILocalizable
{
    [Tooltip("Text to display on the choice")]
    [SerializeField] protected string text = "Option text";

    [Tooltip("Notes about the option text for other authors, localization, etc.")]
    [SerializeField] protected string description = "";

    [Tooltip("Block to execute when this option is selected")]
    [SerializeField] protected Block targetBlock;

    [Tooltip("Musical notes to select this choice")]
    [SerializeField] protected int[] choiceNotes;

    [Tooltip("Have notes been overridden?")]
    [SerializeField] protected bool overriddenNotes;

    public int[] ChoiceNotes
    {
        get
        {
            return choiceNotes;
        }
    }

    public override void OnEnter()
    {
        DialoguePanelBehaviour.Instance.AddChoice(this);
        Continue();
    }

    public void MakeChoice()
    {
        if (targetBlock != null)
        {
            #if UNITY_EDITOR
                Flowchart flowChart = targetBlock.GetFlowchart();
                flowChart.SelectedBlock = targetBlock;
            #endif
            targetBlock.StartExecution();
        }
    }

    public override void GetConnectedBlocks(ref List<Block> connectedBlocks)
    {
        if (targetBlock != null)
        {
            connectedBlocks.Add(targetBlock);
        }
    }

    public override string GetSummary()
    {
        if (targetBlock == null)
        {
            return "Error: No target block selected";
        }

        if (text == "")
        {
            return "Error: No button text selected";
        }

        return text + " : " + targetBlock.BlockName;
    }

    public override Color GetButtonColor()
    {
        return new Color32(184, 210, 235, 255);
    }

    #region ILocalizable implementation

    public virtual string GetStandardText()
    {
        return text;
    }

    public virtual void SetStandardText(string standardText)
    {
        text = standardText;
    }

    public virtual string GetDescription()
    {
        return description;
    }

    public virtual string GetStringId()
    {
        // String id for MusicMenu commands is MUSICMENU.<Localization Id>.<Command id>
        return "MUSICMENU." + GetFlowchartLocalizationId() + "." + itemId;
    }

    #endregion
}
