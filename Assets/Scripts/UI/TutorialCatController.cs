﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialCatController : MonoBehaviour
{
    public string m_stateName;
    Animator m_animator;
    int m_lastBeat;
    Renderer m_renderer;
    CatGraphicsControl m_catGraphics;
    Transform m_innerCat;
    bool m_flipInnerRotation = false;
    public Transform m_directionArrow;

    private void Awake()
    {
        m_renderer = GetComponentInChildren<SkinnedMeshRenderer>();
        m_renderer.sortingLayerName = "UI";

        m_innerCat = transform.GetChild(1);
    }
    void Start()
    {
        m_animator = GetComponent<Animator>();
        m_animator.Play(m_stateName);

        m_catGraphics = FindObjectOfType<CatGraphicsControl>();
    }
    void Update()
    {
        transform.localRotation = m_catGraphics.transform.localRotation;

        m_innerCat.localRotation = m_catGraphics.GetInnerCatTransform().localRotation;
        if (m_flipInnerRotation)
        {
            m_innerCat.localRotation = m_innerCat.localRotation * Quaternion.Euler(0.0f, 180.0f, 0.0f);
        }
        m_directionArrow.localRotation = m_innerCat.localRotation * Quaternion.Euler(0.0f, -90.0f, 0.0f);
    }
    public void Play()
    {
        m_animator.Play(m_stateName);
        if (m_stateName == null || m_stateName.Length == 0) // If we have no animation, it's probably the turn state (such HACK)
        {
            m_flipInnerRotation = !m_flipInnerRotation;
        }
    }
    public void SetAlpha(float a)
    {
        Color c = m_renderer.material.color;
        m_renderer.material.color = new Color(c.r, c.g, c.b, a);
    }
}
