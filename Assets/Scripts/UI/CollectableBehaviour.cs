﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectableBehaviour : TileObject, DestroyableStateHolder<CollectableBehaviour.State, Vector3>
{
    PlayerControl m_player;
    Canvas m_canvas;

    public GameObject m_collectionFeedbackPrefab;
    public AudioClip m_collectionSound;
    public GameObject m_pickedUpCollectablePrefab;

    public ItemType m_itemType;

    [Serializable]
    public class State
    {

    }

    State m_state;

    public State GetState()
    {
        return m_state;
    }

    public void SetState(State s)
    {
        m_state = s;
    }

    public Vector3 GetAddress()
    {
        return (m_startPosition != null ? m_startPosition.Value : transform.position);
    }

    Vector3? m_startPosition;
    Vector3 m_lastPlayerPosition;

    void Start ()
	{
        m_player = FindObjectOfType<PlayerControl>();
        m_canvas = GameObject.FindWithTag("WorldCanvas").GetComponent<Canvas>();
        GetComponent<SpriteRenderer>().sprite = m_itemType.m_image;
        m_startPosition = transform.position;

        if (m_state == null)
        {
            m_state = new State();
        }
    }
	
	void Update ()
	{
        Vector3 playerPosition = m_player.GetPlayerMidPoint();
		if (IsOnTileWithOrHasCrossed(playerPosition, m_lastPlayerPosition))
        {
            GameObject newText = GameObject.Instantiate(m_collectionFeedbackPrefab, m_canvas.transform);
            newText.transform.position = GetMidPosition();
            newText.GetComponent<Text>().text = m_itemType.m_name + "!";

            int note = m_player.GetLastNote() - 12;
            MidiUtil.PlayKeySound(note + 7, m_collectionSound);
            MidiUtil.PlayKeySound(note + 12, m_collectionSound);

            InventoryBehaviour inventory = m_player.GetComponent<InventoryBehaviour>();
            inventory.AddItemAmount(m_itemType, 1);

            GameObject pickedUpCollectable = GameObject.Instantiate(m_pickedUpCollectablePrefab);
            pickedUpCollectable.transform.position = transform.position;
            pickedUpCollectable.GetComponent<ZoomToInventoryBehaviour>().SetItemType(m_itemType);

            Destroy(gameObject);
        }
        m_lastPlayerPosition = playerPosition;
	}
}
