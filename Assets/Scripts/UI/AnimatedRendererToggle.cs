﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedRendererToggle : MonoBehaviour
{
    public float m_onLength = 0.5f;
    public float m_offLength = 0.5f;
    public Renderer m_component;
    float m_timer = 0.0f;
	
	// Update is called once per frame
	void Update ()
    {
        m_timer += Time.deltaTime;
        if (m_component.enabled)
        {
            if (m_timer > m_onLength)
            {
                m_timer -= m_onLength;
                m_component.enabled = false;
            }
        }
        else
        {
            if (m_timer > m_offLength)
            {
                m_timer -= m_offLength;
                m_component.enabled = true;
            }
        }
	}
}
