﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteCycleAnimation : MonoBehaviour
{
    public Sprite[] m_sprites;
    public float m_timePerSprite;

    float m_timer = 0;
    int m_currentSpriteID = 0;
    Image m_image;

    void Start()
    {
        m_image = GetComponent<Image>();
        for (int i = 0; i < m_sprites.Length; i++)
        {
            if (m_image.sprite == m_sprites[i])
            {
                m_currentSpriteID = i;
                break;
            }
        }
    }

    void Update()
    {
        m_timer += Time.deltaTime;
        if (m_timer >= m_timePerSprite)
        {
            m_timer -= m_timePerSprite;

            m_currentSpriteID = (m_currentSpriteID + 1) % m_sprites.Length;
            m_image.sprite = m_sprites[m_currentSpriteID];
        }
    }
}
