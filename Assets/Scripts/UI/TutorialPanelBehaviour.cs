﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialPanelBehaviour : MonoBehaviour
{
    KeyboardGrid m_keyboard;
    PawBehaviour[] m_paws;
    MusicManager m_musicManager;
    TutorialCatController m_tutorialCat;
    Camera m_cameraToFollow;
    Transform m_toFollow;
    public Vector3 m_followOffset;
    public int m_cornerPositionID = -1;
    public float m_cornerPositionHeight = 2.0f;
    public float m_pawScale = 1.0f;

    public class KeyPress
    {
        public int m_note;
        public double m_startTime;
        public double m_endTime;
        public int m_pawID;

        public KeyPress(int note, double startTime, double length, int pawID = 0)
        {
            m_note = note;
            m_startTime = startTime;
            m_endTime = startTime + length;
            m_pawID = pawID;
        }
    }

    public enum TutorialType
    {
        WalkRight,
        WalkUpStepsRight,
        WalkDownStepsRight,
        JumpRight,
        WalkLeft,
        WalkUpStepsLeft,
        WalkDownStepsLeft
    }

    public TutorialType m_tutorialType;
    public ActionConfig m_actionConfig;
    public GameObject m_pawPrefab;

    public Text m_actionTutorialNameText;
    public Text m_actionTutorialDescriptionText;

    List<KeyPress> m_presses;
    List<KeyPress> m_toRemove;

    private void Awake()
    {
        m_tutorialCat = GetComponentInChildren<TutorialCatController>();
        m_keyboard = GetComponentInChildren<KeyboardGrid>();
        m_cameraToFollow = Camera.main;
        m_toFollow = Camera.main.transform;
    }

    void Start()
    {
        m_musicManager = FindObjectOfType<MusicManager>();
        m_presses = new List<KeyPress>();
        m_toRemove = new List<KeyPress>();

        if (m_actionConfig != null)
        {
            if (m_actionTutorialDescriptionText != null)
            {
                m_actionTutorialNameText.text = "To " + m_actionConfig.m_name;
                m_actionTutorialDescriptionText.text = m_actionConfig.m_tutorialDescription;
            }
            else
            {
                m_actionTutorialNameText.text = m_actionConfig.m_name;
            }
            if (m_tutorialCat != null)
            {
                m_tutorialCat.m_stateName = m_actionConfig.m_animationName;
            }
        }
    }

    int m_lastSequenceNote;
    double m_lastSequenceTime;
    bool m_firstSequence = true;
    bool m_anyPawsDown = false;

    void AddActionBasedSequence(List<KeyPress> presses, double currentTime)
    {
        NoteInput[] inputs = m_actionConfig.m_inputPattern.m_inputs;

        if (!m_actionConfig.m_rematchOnCompletion)
        {
            m_lastSequenceTime = MidiUtil.RoundBeatToLatestInterval(currentTime, 1.0);
            m_lastSequenceNote = 60;
        }

        int pawsNeeded = 0;
        int largestScaleShiftNeeded = 0;
        for (int i = 0; i < inputs.Length; i++)
        {
            NoteInput input = inputs[i];
            if (m_actionConfig.m_rematchOnCompletion && !m_firstSequence && !input.NeedsPreviousInput())
            {
                continue;
            }
            double time = input.GetExampleTime(m_lastSequenceTime);
            int note = input.GetExampleNote(m_lastSequenceNote);
            if (i > 0 && !m_actionConfig.m_rematchOnCompletion)
            {
                int noteOctave = note / 12;
                int lastNoteOctave = m_lastSequenceNote / 12;
                if (noteOctave != lastNoteOctave)
                {
                    int scaleShiftNeeded = 0;
                    if (noteOctave > lastNoteOctave)
                    {
                        scaleShiftNeeded = -(MidiUtil.GetScaleNoteFromNote(note, input.GetNoteToScaleNote()) + 1);
                    }
                    else
                    {
                        scaleShiftNeeded = input.GetScaleNoteToNote().Length - MidiUtil.GetScaleNoteFromNote(note, input.GetNoteToScaleNote());
                    }
                    
                    if (Mathf.Abs(scaleShiftNeeded) > Mathf.Abs(largestScaleShiftNeeded))
                    {
                        largestScaleShiftNeeded = scaleShiftNeeded;
                    }
                }
            }
            if (note < 12) // Try and keep it in sensible range
            {
                note += 12;
            }
            else if (note > 72)
            {
                note -= 12;
            }

            double length = ((inputs.Length > 1 && !m_actionConfig.m_rematchOnCompletion) ? 1.0 : 0.8);
            double endTime = time + length;
            int currentOverlap = KeyPressesOverlappingRange(time, endTime);
            if (currentOverlap + 1 > pawsNeeded)
            {
                pawsNeeded = currentOverlap + 1;
            }
            m_presses.Add(new KeyPress(note, time, length, currentOverlap));

            m_lastSequenceTime = time;
            m_lastSequenceNote = note;
        }

        if (largestScaleShiftNeeded != 0)
        {
            //Debug.Log("Shifting presses by " + largestScaleShiftNeeded);
            for (int i = 0; i < inputs.Length; i++)
            {
                int currentScaleNote = MidiUtil.GetScaleNoteFromNote(m_presses[i].m_note, inputs[i].GetNoteToScaleNote());

                int[] scaleNotes = inputs[i].GetScaleNoteToNote();
                currentScaleNote = ((currentScaleNote + largestScaleShiftNeeded) + scaleNotes.Length) % scaleNotes.Length;
                m_presses[i].m_note = scaleNotes[currentScaleNote];
            }
        }

        if (m_paws == null)
        {
            m_paws = new PawBehaviour[pawsNeeded];
            for (int i = 0; i < pawsNeeded; i++)
            {
                GameObject newPaw = GameObject.Instantiate(m_pawPrefab, transform);
                newPaw.transform.localScale *= m_pawScale;
                m_paws[i] = newPaw.GetComponent<PawBehaviour>();
            }
        }
        m_firstSequence = false;
    }

    int KeyPressesOverlappingRange(double start, double end)
    {
        int overlapCount = 0;
        for (int i = 0; i < m_presses.Count; i++)
        {
            KeyPress press = m_presses[i];
            bool notOverlap = press.m_endTime < start || end < press.m_startTime;
            if (notOverlap)
            {
                continue;
            }
            overlapCount++;
        }
        return overlapCount;
    }

    void AddSequence(List<KeyPress> presses, double currentTime) // Shouldn't need this any more
    {
        float stepsInterval = 1.0f;
        switch (m_tutorialType)
        {
            case TutorialType.WalkRight:
            {
                int randomScaleNote = Random.Range(0, 7); // Exclusive upper bound
                int note = 60 + MidiUtil.MajorScaleNoteToNoteOffset(randomScaleNote); // C Major scale
                m_presses.Add(new KeyPress(note, MidiUtil.RoundBeatToLatestInterval(currentTime, 1.0) + 1.0, 1.0));
                break;
            }
            case TutorialType.WalkUpStepsRight:
            {
                int randomScaleNote = Random.Range(0, 3);
                double firstNoteTime = MidiUtil.RoundBeatToLatestInterval(currentTime, 4.0f * stepsInterval) + (4.0f * stepsInterval);
                for (int i = 0; i < 5; i++)
                {
                    int note = 60 + MidiUtil.MajorScaleNoteToNoteOffset(randomScaleNote + i); // C Major scale (going up)
                    m_presses.Add(new KeyPress(note, firstNoteTime + (i * stepsInterval), stepsInterval * 0.8f));
                }
                break;
            }
            case TutorialType.WalkDownStepsRight:
            {
                int randomScaleNote = Random.Range(4, 7);
                double firstNoteTime = MidiUtil.RoundBeatToLatestInterval(currentTime, 4.0f * stepsInterval) + (4.0f * stepsInterval);
                for (int i = 0; i < 5; i++)
                {
                    int note = 60 + MidiUtil.MajorScaleNoteToNoteOffset(randomScaleNote - i); // C Major scale (going down)
                    m_presses.Add(new KeyPress(note, firstNoteTime + (i * stepsInterval), stepsInterval * 0.8f));
                }
                break;
            }
            case TutorialType.JumpRight:
            {
                int randomScaleNote = Random.Range(0, 5);
                double firstNoteTime = MidiUtil.RoundBeatToLatestInterval(currentTime, 1.0) + 1.0;
                for (int i = 0; i < 2; i++)
                {
                    int note = 60 + MidiUtil.MajorScaleNoteToNoteOffset(randomScaleNote + (i * 2)); // C Major scale (going down)
                    m_presses.Add(new KeyPress(note, firstNoteTime, 1.0, i));
                }
                break;
            }
            case TutorialType.WalkLeft:
            {
                int randomScaleNote = Random.Range(0, 5); // Exclusive upper bound
                int note = 61 + MidiUtil.MajorPentatonicScaleNoteToNoteOffset(randomScaleNote); // C# Major Pentatonic scale
                m_presses.Add(new KeyPress(note, MidiUtil.RoundBeatToLatestInterval(currentTime, 1.0) + 1.0, 1.0));
                break;
            }
            case TutorialType.WalkUpStepsLeft:
            {
                int randomScaleNote = 0;
                double firstNoteTime = MidiUtil.RoundBeatToLatestInterval(currentTime, 2.0) + 2.0;
                for (int i = 0; i < 5; i++)
                {
                    int note = 61 + MidiUtil.MajorPentatonicScaleNoteToNoteOffset(randomScaleNote + i); // C# Major pentatonic scale (going up)
                    m_presses.Add(new KeyPress(note, firstNoteTime + (i * 0.5), 0.4));
                }
                break;
            }
            case TutorialType.WalkDownStepsLeft:
            {
                int randomScaleNote = 4;
                double firstNoteTime = MidiUtil.RoundBeatToLatestInterval(currentTime, 2.0) + 2.0;
                for (int i = 0; i < 5; i++)
                {
                    int note = 61 + MidiUtil.MajorPentatonicScaleNoteToNoteOffset(randomScaleNote - i); // C# Major pentatonic scale (going down)
                    m_presses.Add(new KeyPress(note, firstNoteTime + (i * 0.5), 0.4));
                }
                break;
            }
        }
    }

    public void SetAlpha(float alpha)
    {
        GetComponent<CanvasGroup>().alpha = alpha;
        if (m_tutorialCat != null)
        {
            m_tutorialCat.SetAlpha (alpha);
        }
    }

    void LateUpdate()
    {
        if (m_cornerPositionID < 0)
        {
            transform.position = new Vector3 (m_toFollow.position.x, m_toFollow.position.y, transform.position.z) + m_followOffset;
        }
        else
        {
            transform.position = new Vector3 (m_toFollow.position.x + (m_cameraToFollow.orthographicSize * m_cameraToFollow.aspect), (m_toFollow.position.y + m_cameraToFollow.orthographicSize) - (m_cornerPositionHeight * m_cornerPositionID), transform.position.z) + m_followOffset;
        }
        double currentTime = m_musicManager.GetCurrentBeat();
        if (m_presses.Count == 0)
        {
            if (m_actionConfig != null)
            {
                AddActionBasedSequence(m_presses, currentTime);
            }
            else
            {
                AddSequence(m_presses, currentTime);
            }
        }

        if (m_paws == null)
        {
            return;
        }
        bool anythingDown = false;
        for (int i = 0; i < m_paws.Length; i++)
        {
            KeyPress mostRelevantKeyPress = null;
            double lowestTimeUntilKeyPress = double.MaxValue;
            foreach (KeyPress keyPress in m_presses)
            {
                if (keyPress.m_pawID != i)
                {
                    continue;
                }
                if (keyPress.m_startTime <= currentTime && keyPress.m_endTime > currentTime)
                {
                    mostRelevantKeyPress = keyPress;
                    break;
                }
                else
                {
                    double timeUntilThisKey = keyPress.m_startTime - currentTime;
                    if (timeUntilThisKey >= 0 && timeUntilThisKey < lowestTimeUntilKeyPress)
                    {
                        mostRelevantKeyPress = keyPress;
                        lowestTimeUntilKeyPress = timeUntilThisKey;
                    }
                }
                if (keyPress.m_endTime < currentTime)
                {
                    m_toRemove.Add(keyPress);
                }
            }

            if (mostRelevantKeyPress != null)
            {
                KeyPress keyPress = mostRelevantKeyPress;
                Vector3 toKey = m_keyboard.GetKeyPosition(keyPress.m_note) - m_paws[keyPress.m_pawID].transform.position;
                m_paws[keyPress.m_pawID].transform.position = m_paws[keyPress.m_pawID].transform.position + toKey * 0.5f;

                bool pawIsAboveKey = toKey.sqrMagnitude < 0.5f;
                bool noteShouldPlay = keyPress.m_startTime <= currentTime && keyPress.m_endTime > currentTime;

                if (pawIsAboveKey)
                {
                    m_paws[keyPress.m_pawID].SetDown(noteShouldPlay);
                    m_keyboard.SetKeyDown(keyPress.m_note, noteShouldPlay);
                    if (noteShouldPlay)
                    {
                        anythingDown = true;
                    }
                }
                else
                {
                    m_paws[keyPress.m_pawID].SetDown(false);
                    m_keyboard.SetKeyDown(keyPress.m_note, false);
                }
            }

            foreach (KeyPress toRemove in m_toRemove)
            {
                m_presses.Remove(toRemove);
                m_paws[toRemove.m_pawID].SetDown(false);
                m_keyboard.SetKeyDown(toRemove.m_note, false);
            }
            m_toRemove.Clear();
        }

        if (anythingDown && !m_anyPawsDown)
        {
            if (m_tutorialCat != null)
            {
                m_tutorialCat.Play ();
            }
        }

        m_anyPawsDown = anythingDown;
    }
}
