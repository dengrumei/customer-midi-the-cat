﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaticStaffUI : MonoBehaviour
{
    Text m_text;
    int[] m_notes;
    Color[] m_noteColoursFromA;

    public void SetNotes(int[] notes, Color[] noteColoursFromA)
    {
        m_text = GetComponent<Text>();
        m_notes = notes;
        m_noteColoursFromA = noteColoursFromA;

        UpdateText(notes);
    }

    void UpdateText(int[] notes, int progress = 0)
    {
        string wholeString = "";
        for (int i = 0; i < notes.Length; i++)
        {
            if (i > 0)
            {
                wholeString += "=";
            }
            int fromA = MidiUtil.NotesFromA(notes[i]) % 12;
            Color col = m_noteColoursFromA[fromA];
            if (progress > i)
            {
                col = Color.white;
            }
            wholeString += "<color=#" + ColorUtility.ToHtmlStringRGB(col) + ">" + MidiUtil.NoteToString(notes[i]) + "</color>";
        }
        m_text.text = wholeString;
    }

    public void SetProgress(int progress)
    {
        UpdateText(m_notes, progress);
    }

    public void CrossFadeAlpha(float targetAlpha, float duration, bool ignoreTimeScale)
    {
        m_text.CrossFadeAlpha(targetAlpha, duration, ignoreTimeScale);
    }
}
