﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryBehaviour : MonoBehaviour, StateHolder<InventoryBehaviour.State>
{
    public ItemConfigs m_itemConfigs;
    public GameObject m_itemUIPrefab;

    Canvas m_canvas;

    List<GameObject> m_itemUIPanels;


    [Serializable]
    public class StoredItemType
    {
        public string m_typeName;
        public int m_count;
    }

    [Serializable]
    public class State
    {
        public List<StoredItemType> m_itemCounts;

        public State()
        {
            m_itemCounts = new List<StoredItemType>();
        }
    }

    State m_state;

    public void SetState(State s)
    {
        m_state = s;
    }

    public State GetState()
    {
        return m_state;
    }

    void Start ()
	{
        m_canvas = GameObject.FindWithTag("ScreenCanvas").GetComponent<Canvas>();
        m_itemUIPanels = new List<GameObject>();

        if (m_state == null)
        {
            m_state = new State();
        }
	}

    void LateUpdate()
    {
        for (int i = 0; i < m_state.m_itemCounts.Count; i++)
        {
            if (m_itemUIPanels.Count <= i)
            {
                GameObject newPanel = GameObject.Instantiate(m_itemUIPrefab, m_canvas.transform);
                RectTransform rectTransform = newPanel.GetComponent<RectTransform>();
                Rect rect = rectTransform.rect;
                rectTransform.anchoredPosition = new Vector2((rect.width * i) + 0.5f, -0.5f);

                m_itemUIPanels.Add(newPanel);
            }

            string itemName = m_state.m_itemCounts[i].m_typeName;
            ItemType item = m_itemConfigs.GetItem(itemName);
            if (item != null)
            {
                Image toChange = m_itemUIPanels[i].transform.GetChild(0).GetComponent<Image>();
                toChange.sprite = item.m_image;
                toChange.SetNativeSize();
                Vector2 size = toChange.GetComponent<RectTransform>().sizeDelta;
                size *= toChange.pixelsPerUnit;
                Vector2 pixelPivot = toChange.sprite.pivot;
                Vector2 percentPivot = new Vector2(pixelPivot.x / size.x, pixelPivot.y / size.y);
                toChange.GetComponent<RectTransform>().pivot = percentPivot;
            }

            Text textToChange = m_itemUIPanels[i].transform.GetChild(2).GetComponent<Text>();
            textToChange.text = m_state.m_itemCounts[i].m_count.ToString();
        }
    }

    public void AddItemAmount(ItemType type, int count)
    {
        StoredItemType storedItemType = m_state.m_itemCounts.Find(x => x.m_typeName.Equals(type.m_name));
        if (storedItemType == null)
        {
            storedItemType = new StoredItemType();
            storedItemType.m_typeName = type.m_name;
            m_state.m_itemCounts.Add(storedItemType);
        }
        storedItemType.m_count += count;
    }

    public int GetItemAmount(ItemType type)
    {
        StoredItemType storedItemType = m_state.m_itemCounts.Find(x => x.m_typeName == type.name);
        if (storedItemType == null)
        {
            return 0;
        }
        return storedItemType.m_count;
    }

    public Vector3 GetItemUIPosition(ItemType type)
    {
        int itemIndex = m_state.m_itemCounts.FindIndex(x => x.m_typeName.Equals(type.m_name));
        if (itemIndex >= 0)
        {
            RectTransform rectTransform = m_itemUIPanels[itemIndex].transform.GetChild(0).GetComponent<RectTransform>();
            Vector3 rectPos = rectTransform.transform.position;
            Vector3 worldPoint = Camera.main.ScreenToWorldPoint(new Vector3(rectPos.x, rectPos.y, -Camera.main.transform.position.z));
            return worldPoint;
        }
        return Camera.main.ScreenToWorldPoint(new Vector3(0.0f, Screen.height, 0.0f));
    }
}
