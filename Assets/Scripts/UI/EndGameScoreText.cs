﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameScoreText : MonoBehaviour
{
    public string m_itemName = "Salmon";
    public int m_totalPossible = 10;
    void Start()
    {
        SaveGame saveGame = SaveGameSystem.s_currentSaveGame;
        int salmonCollected = 0;
        if (saveGame != null && saveGame.m_inventoryState != null)
        {
            for (int i = 0; i < saveGame.m_inventoryState.m_itemCounts.Count; i++)
            {
                InventoryBehaviour.StoredItemType storedItem = saveGame.m_inventoryState.m_itemCounts[i];
                if (string.Equals(storedItem.m_typeName, m_itemName))
                {
                    salmonCollected += storedItem.m_count;
                }
            }
        }
        Text text = GetComponent<Text>();
        text.text = string.Format(text.text, salmonCollected, m_totalPossible);
    }
}
