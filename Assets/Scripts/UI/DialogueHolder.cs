﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueHolder : MonoBehaviour
{

    Canvas m_canvas;

    public GameObject m_dialoguePrefab;
    public AudioClip[] m_voices;
    GameObject m_currentDialogue;
    AudioSource m_audioSource;
    string m_lastText;
    int[] m_lastTextSingingNotes;
    int m_lastSingingPosition;

    float m_displayTime;
    float m_displayCounter;
    bool m_skipping;

    public delegate void LineFinishedHandler();

    public event LineFinishedHandler LineFinishedEvent;

    void Start()
    {
        m_canvas = GameObject.FindWithTag("WorldCanvas").GetComponent<Canvas>();
        m_audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (m_currentDialogue != null)
        {
            if (m_displayCounter > 0.0f)
            {
                /*bool anyKeysDown = false;
                for (int i = 0; i < MidiInput.Instance.GetKeyCount(); i++)
                {
                    if (MidiInput.Instance.GetKeyDown(i) && !MidiUtil.IsBlackNote(i))
                    {
                        anyKeysDown = true;
                        Debug.Log("Key down " + i);
                        break;
                    }
                }*/
                //Debug.Log("Here");

                if (m_skipping)
                {
                    m_displayCounter -= Time.deltaTime * 8.0f;
                }
                m_displayCounter -= Time.deltaTime;

                UpdateCounterRelated();
            }
        }
    }

    void UpdateCounterRelated()
    {
        if (m_displayCounter <= 0.0f)
        {
            //MidiInput.Instance.MidiIOEvent -= HandleMidiIOEvent;
            m_skipping = false;
            LineFinishedEvent();
        }
        float extraEndTime = 1.0f;
        float scrollTime = (m_displayTime - extraEndTime);
        if (scrollTime <= extraEndTime)
        {
            scrollTime = extraEndTime;
        }
        SetWorldSayProportion(1.0f - Mathf.Clamp01((m_displayCounter - extraEndTime) / scrollTime));
    }

    public void Sing(int note)
    {
        int scaleNote = MidiUtil.NoteOffsetToMajorScaleNote(note);
        SingScaleNote(scaleNote);
    }

    void SingScaleNote(int scaleNote)
    {
        if (m_voices.Length <= 0)
        {
            return;
        }
        if (scaleNote == -1)
        {
            return;
        }
        if (scaleNote >= m_voices.Length)
        {
            Debug.Log("WTF! ScaleNote is " + scaleNote);
            return;
        }
        m_audioSource.clip = m_voices[scaleNote];
        m_audioSource.Play();
    }

    public void WorldSay(string text, int bubbleDirection, bool firstLine, bool instant = false)
    {
        float guessTime = GuessTextDisplayTime(text, firstLine);
        m_displayTime = guessTime;
        m_displayCounter = guessTime;

        if (instant)
        {
            m_displayCounter = 0.0f;
            m_lastTextSingingNotes = null;
        }
        else
        {
            string solresol = SolresolUtils.TranslateEnglishToSolresol(text);
            Debug.Log("Solresol = " + solresol);
            m_lastTextSingingNotes = SolresolUtils.SolresolToNoteIds(solresol);
            m_lastSingingPosition = 0;
        }

        if (m_currentDialogue == null)
        {
            m_currentDialogue = GameObject.Instantiate(m_dialoguePrefab, m_canvas.transform);
        }
        m_currentDialogue.transform.position = transform.position + new Vector3(0.0f, 1.0f, 0.0f);
        m_currentDialogue.transform.localScale = new Vector3(bubbleDirection * Mathf.Abs(m_currentDialogue.transform.localScale.x), m_currentDialogue.transform.localScale.y, m_currentDialogue.transform.localScale.z);
        Text textObject = m_currentDialogue.GetComponentInChildren<Text>();
        textObject.transform.localScale = new Vector3(bubbleDirection * Mathf.Abs(textObject.transform.localScale.x), textObject.transform.localScale.y, textObject.transform.localScale.z);
        DialogueBubbleBehaviour bubble = m_currentDialogue.GetComponent<DialogueBubbleBehaviour>();
        RectTransform skipDisplayRect = bubble.m_skipDisplay.GetComponent<RectTransform>();
        if (bubbleDirection > 0.0f)
        {
            skipDisplayRect.anchorMin = new Vector2(1.0f, 0.0f);
            skipDisplayRect.anchorMax = new Vector2(1.0f, 0.0f);
        }
        else
        {
            skipDisplayRect.anchorMin = new Vector2(0.0f, 0.0f);
            skipDisplayRect.anchorMax = new Vector2(0.0f, 0.0f);
        }
        skipDisplayRect.anchoredPosition = new Vector2(0.0f, -25.0f);
        //textObject.text = text;
        m_lastText = text;
        SetWorldSayProportion(instant ? 1.0f : 0.0f);
        m_skipping = false;

        //MidiInput.Instance.MidiIOEvent += HandleMidiIOEvent;
    }

    public void HandleMidiIOEvent(int note, bool down, float velocity)
    {
        if (down && !MidiUtil.IsBlackNote(note))
        {
            m_skipping = true;
        }
    }

    void SetWorldSayProportion(float proportion)
    {
        if (m_currentDialogue == null)
        {
            return;
        }
        proportion = Mathf.Clamp01(proportion);

        int textProgress = (int)(m_lastText.Length * proportion);
        string doneText = m_lastText.Substring(0, textProgress);
        string futureText = m_lastText.Substring(textProgress, m_lastText.Length - textProgress);
        
        Text textObject = m_currentDialogue.GetComponentInChildren<Text>();
        textObject.text = doneText + "<color=#888888ff>" + futureText + "</color>";

        DialogueBubbleBehaviour bubble = m_currentDialogue.GetComponent<DialogueBubbleBehaviour>();
        bubble.m_skipDisplay.SetActive(proportion < 1.0f && !m_skipping);

        SetSingingProportion(proportion);
    }

    void SetSingingProportion(float proportion)
    {
        if (m_lastTextSingingNotes != null)
        {
            int currentSingingPosition = (int)(m_lastTextSingingNotes.Length * proportion);
            if (currentSingingPosition >= m_lastTextSingingNotes.Length)
            {
                currentSingingPosition = m_lastTextSingingNotes.Length - 1;
            }

            if (currentSingingPosition >= m_lastSingingPosition)
            {
                Sing(m_lastTextSingingNotes[m_lastSingingPosition]);
                m_lastSingingPosition++;
            }
        }
    }

    public static float GuessTextDisplayTime(string text, bool firstLine)
    {
        float waitTime = text.Length * 0.15f;
        if (firstLine)
        {
            waitTime *= 1.5f;
        }
        return Mathf.Max(waitTime, 1.0f);
    }

    public void StopWorldSay()
    {
        if (m_currentDialogue != null)
        {
            Destroy(m_currentDialogue);
            m_currentDialogue = null;
        }
    }
}
