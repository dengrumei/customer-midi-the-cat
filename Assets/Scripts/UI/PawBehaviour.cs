﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PawBehaviour : MonoBehaviour
{
    Image m_image;
    public Sprite m_pawUp;
    public Sprite m_pawDown;

    void Awake()
    {
        m_image = GetComponent<Image>();
    }

    public void SetDown(bool down)
    {
        m_image.sprite = (down ? m_pawDown : m_pawUp);
    }
}
