﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DimmerBehaviour : MonoBehaviour {

    float m_requestedAlpha;
    Image m_dimmerUIImage;

    public float m_dimmingSpeed = 1.0f;

	// Use this for initialization
	void Awake ()
    {
        m_dimmerUIImage = GetComponent<Image>();
        transform.transform.SetAsFirstSibling();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 trackPos = Camera.main.transform.position;
        transform.position = new Vector3(trackPos.x, trackPos.y, 0.0f);
        Color c = m_dimmerUIImage.color;
        float currentAlpha = c.a;
        currentAlpha = Mathf.MoveTowards(currentAlpha, m_requestedAlpha, m_dimmingSpeed * Time.deltaTime);
        m_dimmerUIImage.color = new Color(c.r, c.g, c.b, currentAlpha);

        m_dimmerUIImage.enabled = currentAlpha > 0.0f;
    }

    public void FadeToAlpha(float alpha)
    {
        m_requestedAlpha = alpha;
    }
}
