﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialoguePanelBehaviour : MonoBehaviour
{
    public GameObject m_choicePanelPrefab;
    public Text m_cancelText;

    List<GameObject> m_choicePanels;

    PlayerControl m_player;
    MidiInput m_midiInput;

    static DialoguePanelBehaviour m_instance;

    MusicMenu m_choiceTaken;
    int m_bestProgress;

    public static DialoguePanelBehaviour Instance
    {
        get
        {
            if (m_instance == null)
            {
                m_instance = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/DialoguePanel"), GameObject.FindWithTag("WorldCanvas").transform).GetComponent<DialoguePanelBehaviour>();
                m_instance.m_player = FindObjectOfType<PlayerControl>();
                m_instance.m_midiInput = FindObjectOfType<MidiInput>();
                m_instance.m_midiInput.MidiIOEvent += m_instance.HandleMidiIOEvent;
            }
            m_instance.transform.position = m_instance.m_player.transform.position + new Vector3(0.0f, -2.0f, 0.0f);
            return m_instance;
        }
    }

    public void ResetChoices()
    {
        if (m_choicePanels != null)
        {
            foreach(GameObject panel in m_choicePanels)
            {
                Destroy(panel);
            }
            m_choicePanels = null;
        }
        m_midiInput.m_muteKeys = false;
        m_cancelText.enabled = false;
    }

    public void AddChoice(MusicMenu choice)
    {
        if (m_choicePanels == null)
        {
            m_choicePanels = new List<GameObject>();
        }
        GameObject newPanel = GameObject.Instantiate(m_choicePanelPrefab) as GameObject;
        newPanel.transform.SetParent(transform);
        RectTransform parentRectTransform = GetComponent<RectTransform>();

        RectTransform rectTransform = newPanel.GetComponent<RectTransform>();
        newPanel.transform.localPosition = new Vector3(0.0f, (parentRectTransform.rect.height * 0.5f) - (m_choicePanels.Count * rectTransform.rect.height), 0.0f);
        //newPanel.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        rectTransform.sizeDelta = new Vector2(1.0f, 1.0f);
        rectTransform.offsetMin = new Vector2(0.0f, rectTransform.offsetMin.y);
        rectTransform.offsetMax = new Vector2(0.0f, rectTransform.offsetMax.y);
        //rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0.0f, 1.0f);
        //rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0.0f, 1.0f);

        newPanel.GetComponent<ChoicePanelBehaviour>().SetupChoice(choice, this);
        m_choicePanels.Add(newPanel);
        m_cancelText.transform.localPosition = newPanel.transform.localPosition + new Vector3(0.0f, -(m_choicePanelPrefab.GetComponent<RectTransform>().rect.height + m_cancelText.rectTransform.rect.height), 0.0f);
        m_cancelText.enabled = true;
    }

    public bool IsWaitingForPlayer()
    {
        return (m_choicePanels != null && m_choicePanels.Count > 0) || m_choiceTaken != null;
    }

    void HandleMidiIOEvent (int note, bool down, float velocity)
    {
        if (m_choiceTaken != null)
        {
            //m_choiceDisplayCounter = 0.0f;
            m_player.GetComponent<DialogueHolder>().HandleMidiIOEvent(note, down, velocity);
        }

        if (down)
        {
            if (!MidiUtil.IsBlackNote(note))
            {
                HandleChoicesInput(note, down, velocity);
            }
            else
            {
                NPCBehaviour npc = GetConversingNPC();
                if (npc != null)
                {
                    npc.EndDialogue();
                }
            }
        }
    }

    void HandleChoicesInput(int note, bool down, float velocity)
    {
        if (m_choicePanels == null)
        {
            return;
        }

        m_player.GetComponent<DialogueHolder>().Sing(note);

        int bestProgress = CalculateBestProgress();
        int[] progresses = new int[m_choicePanels.Count];
        int[] textProgresses = new int[m_choicePanels.Count];
        for (int i = 0; i < m_choicePanels.Count; i++)
        {
            ChoicePanelBehaviour choicePanel = m_choicePanels[i].GetComponent<ChoicePanelBehaviour>();
            progresses[i] = choicePanel.GetChoiceProgress();
            textProgresses[i] = choicePanel.GetTextProgress();
            if (progresses[i] == bestProgress)
            {
                MusicMenu choice = choicePanel.HandleMidiIOEvent(note, down, velocity);
                if (choice != null)
                {
                    MakeChoice(choice);
                    return;
                }
            }
        }

        int newBestProgress = CalculateBestProgress();
        if (newBestProgress > bestProgress)
        {
            for (int i = 0; i < m_choicePanels.Count; i++)
            {
                ChoicePanelBehaviour choicePanel = m_choicePanels[i].GetComponent<ChoicePanelBehaviour>();
                if (choicePanel.GetChoiceProgress() == newBestProgress)
                {
                    string solresol = SolresolUtils.NoteIDsToSolresol(choicePanel.GetChoiceNotes(), choicePanel.GetTextProgress());
                    int lastSpace = solresol.LastIndexOf(' ');
                    if (lastSpace > 0)
                    {
                        string firstPiece = solresol.Substring(0, lastSpace);
                        string secondPiece = solresol.Substring(lastSpace, solresol.Length - lastSpace);

                        string englishFirstPiece = SolresolUtils.TranslateSolresolToEnglish(firstPiece);
                        string englishSecondPiece = SolresolUtils.TranslateSolresolToEnglish(secondPiece);

                        SetThinkingText(englishFirstPiece, " " + englishSecondPiece);
                    }
                    else
                    {
                        string english = SolresolUtils.TranslateSolresolToEnglish(solresol);
                        SetThinkingText("", english);
                    }
                    break;
                }
            }
        }
        else if (newBestProgress <= bestProgress)
        {
            for (int i = 0; i < m_choicePanels.Count; i++)
            {
                ChoicePanelBehaviour choicePanel = m_choicePanels[i].GetComponent<ChoicePanelBehaviour>();
                if (progresses[i] == bestProgress)
                {
                    int textProgress = textProgresses[i];
                    int[] playedNotes = new int[textProgress + 1];
                    for (int n = 0; n < textProgress; n++)
                    {
                        playedNotes[n] = choicePanel.GetChoiceNotes()[n];
                    }
                    playedNotes[textProgress] = note % 12;
                    string solresol = SolresolUtils.NoteIDsToSolresol(playedNotes);
                    string english = SolresolUtils.TranslateSolresolToEnglish(solresol);
                    SetThinkingText(english, "");
                    break;
                }
            }
        }
    }

    void Update()
    {
        if (m_choiceTaken != null)
        {
            /*m_choiceDisplayCounter -= Time.deltaTime;
            if (m_choiceDisplayCounter <= 0.0f)
            {
                m_player.GetComponent<DialogueHolder>().StopWorldSay();
                m_choiceTaken.MakeChoice();
                m_choiceTaken = null;
            }
            else
            {
                m_player.GetComponent<DialogueHolder>().SetWorldSayProportion(1.0f - Mathf.Clamp01((m_choiceDisplayCounter - 1.0f) / m_choiceDisplayShowTime));
            }*/
        }
        else
        {
            m_bestProgress = CalculateBestProgress();
            if (m_choicePanels != null)
            {
                m_midiInput.m_muteKeys = true;
            }
        }
    }

    public NPCBehaviour GetConversingNPC()
    {
        if (m_choicePanels != null && m_choicePanels.Count > 0)
        {
            ChoicePanelBehaviour behaviour = m_choicePanels[0].GetComponent<ChoicePanelBehaviour>();
            NPCBehaviour npc = behaviour.GetChoice().ParentBlock.GetFlowchart().GetComponent<NPCBehaviour>();
            return npc;
        }
        return null;
    }

    void OnLineFinished()
    {
        if (m_choiceTaken != null)
        {
            m_choiceTaken.MakeChoice();
            m_choiceTaken = null;
        }
    }

    public void SetThinkingText(string doneText, string thinkingText)
    {
        if (m_choicePanels != null)
        {
            string text = doneText + "<color=#888888ff>" + thinkingText + "</color>";
            m_player.GetComponent<DialogueHolder>().WorldSay(text, -1, true, true);
        }
    }

    public int GetBestProgress()
    {
        return m_bestProgress;
    }

    int CalculateBestProgress()
    {
        int bestProgress = 0;
        if (m_choicePanels != null)
        {
            for (int i = 0; i < m_choicePanels.Count; i++)
            {
                ChoicePanelBehaviour behaviour = m_choicePanels[i].GetComponent<ChoicePanelBehaviour>();
                int progress = behaviour.GetChoiceProgress();
                if (progress > bestProgress)
                {
                    bestProgress = progress;
                }
            }
        }
        return bestProgress;
    }

    public void MakeChoice(MusicMenu choice)
    {
        if (m_choicePanels == null) // We must've already made a choice
        {
            Debug.Log("Multiple dialogue choices on the same frame...");
            return;
        }
        ResetChoices();
        // Stop the WorldSay of the flowchart character
        choice.ParentBlock.GetFlowchart().GetComponent<DialogueHolder>().StopWorldSay();

        DialogueHolder holder = m_player.GetComponent<DialogueHolder>();
        holder.WorldSay(choice.GetStandardText(), -1, true);
        holder.LineFinishedEvent += OnLineFinished;
        
        m_choiceTaken = choice;
        m_midiInput.m_muteKeys = false;
    }
}
