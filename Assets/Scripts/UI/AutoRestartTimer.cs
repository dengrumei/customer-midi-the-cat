﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoRestartTimer : MonoBehaviour
{
    string m_startText;
    Text m_text;

    public float m_secondsTillAppears = 30.0f;
    public float m_secondsTillReset = 60.0f;
    float m_timer = 0.0f;

	// Use this for initialization
	void Start ()
    {
        m_text = GetComponent<Text>();
        m_startText = m_text.text;
        MidiInput.Instance.MidiIOEvent += OnMidiIOEvent;
	}

    private void OnMidiIOEvent(int note, bool down, float velocity)
    {
        m_timer = 0.0f;
    }

    // Update is called once per frame
    void Update ()
    {
        m_timer += Time.deltaTime;
        m_text.enabled = m_timer >= m_secondsTillAppears;

        if (m_text.enabled)
        {
            float timeTillReset = m_secondsTillReset - m_timer;
            int secondsTillReset = (int)(timeTillReset + 0.9f);
            m_text.text = string.Format(m_startText, secondsTillReset);
        }
        if (m_timer >= m_secondsTillReset)
        {
            this.enabled = false;
            AutoFade.LoadLevel(0, 1.0f, 1.0f, Color.black);
        }
	}
}
