﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaffUIControl : MonoBehaviour
{
    public Vector3 m_positionOffset;
    public GameObject[] m_notePrefabs;
    public float m_noteGrowInSpeed = 4.0f;
    public float m_noteFadeOutBeats = 1.0f;
    public float m_noteFadeOutBeatOffset = 1.0f;
    public double m_beatsAcross = 8.0;
    public float m_firstNoteFadeInSpeed = 1.0f;
    public AnimationCurve m_growCurve;
    public AnimationCurve m_shrinkCurve;
    public GameObject[] m_beatLines;

    Transform m_trackTarget;
    MusicManager m_musicManager;

    //float m_lineDistance;
    float m_lineOrGapDistance;
    RectTransform m_rectTransform;
    //Image m_image;
    float m_fadeTarget;
    float m_fadeValue;

    class Note
    {
        public int m_noteID;
        public double m_startBeat;
        public GameObject m_spawnedNote;
        public GameObject m_spawnedSharp;
        public bool m_ignored;
    }

    List<Note> m_notes;
    List<Note> m_toDestroy;

	void Start ()
	{
        //m_trackTarget = FindObjectOfType<CatGraphicsControl>().transform;
        m_trackTarget = Camera.main.transform;
        m_musicManager = FindObjectOfType<MusicManager>();

        m_rectTransform = GetComponent<RectTransform>();
        //m_image = GetComponent<Image>();

        float staffHeight = m_rectTransform.rect.height;
        //m_lineDistance = staffHeight / 4.0f;
        m_lineOrGapDistance = staffHeight / 8.0f;

        m_notes = new List<Note>();
        m_toDestroy = new List<Note>();
        /*Note testNote = new Note();
        testNote.m_noteID = 60;
        testNote.m_startBeat = 2.0;
        testNote.m_spawnedNote = GameObject.Instantiate(m_notePrefabs[0], transform);

        m_notes.Add(testNote);*/
	}

    public void AddNote(int noteID, double beatTime, bool ignored)
    {
        Note newNote = new Note();
        newNote.m_noteID = noteID;
        newNote.m_startBeat = beatTime;
        newNote.m_spawnedNote = GameObject.Instantiate(m_notePrefabs[0], transform);
        if (MidiUtil.IsBlackNote(noteID))
        {
            newNote.m_spawnedSharp = GameObject.Instantiate(m_notePrefabs[1], transform);
        }
        newNote.m_ignored = ignored;

        m_notes.Add(newNote);
        m_fadeTarget = 1.0f;
    }

    public void SetFadeTarget(float fadeTarget)
    {
        m_fadeTarget = fadeTarget;
    }
	
	void LateUpdate ()
	{
        transform.position = m_trackTarget.transform.position + m_positionOffset;

        //double currentBeat = m_musicManager.GetCurrentBeat();

        m_fadeValue = Mathf.MoveTowards(m_fadeValue, m_fadeTarget, m_firstNoteFadeInSpeed * (float)m_musicManager.SecondsToBeat(Time.deltaTime));

        CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
        canvasGroup.alpha = m_fadeValue;

        //float initialFade = 0.0f;
        /*if (m_firstNoteReceivedBeat < currentBeat)
        {
            float fade = (float)(currentBeat - m_firstNoteReceivedBeat) * m_firstNoteFadeInSpeed;
            fade = Mathf.Clamp01(fade);
            initialFade = fade;
        }
        //m_image.color = new Color(1.0f, 1.0f, 1.0f, initialFade);
        .alpha = initialFade;*/

        PositionNotes();
        PositionGhosts();
	}

    int GetTrebleStaffLinePosition(int note)
    {
        int closestWhiteNote = MidiUtil.ClosestWhiteNote(note);
        int notesFromMiddleC = (closestWhiteNote + 12) % 24; // Middle C is 60, so need to offset by an octave for this to mod correctly
        int whiteNotesFromMiddleC = MidiUtil.WhiteKeysBetweenNotes(0, notesFromMiddleC) - 1; // Need the -1 so we don't count ourself
        int whiteNotesFromBottomE = whiteNotesFromMiddleC - 2; // (Middle C is 2 underneath the bottom E of the treble staff)

        //Debug.Log("Closest white note = " + closestWhiteNote + ", notes from middle C = " + notesFromMiddleC + ", white notes from middle C = " + whiteNotesFromMiddleC + ", whiteNotes from E = " + whiteNotesFromBottomE);
        return whiteNotesFromBottomE;
    }

    float GetStaffNoteHeight(int note)
    {
        int linePos = GetTrebleStaffLinePosition(note);
        return m_rectTransform.rect.yMin + (linePos * m_lineOrGapDistance);
    }

    double GetCurrentOriginBeat()
    {
        double currentOriginBeat = m_musicManager.GetCurrentBeat() - m_beatsAcross * 0.5;
        return currentOriginBeat;
    }

    float GetStaffXProportion(double time)
    {
        double noteBeatFromLeft = time - GetCurrentOriginBeat();
        float proportionAlong = (float)(noteBeatFromLeft / m_beatsAcross);
        return proportionAlong;
    }

    float GetStaffNoteX(float proportionAlong)
    {
        return m_rectTransform.rect.xMin + proportionAlong * m_rectTransform.rect.width;
    }

    void PositionNotes()
    {
        double currentBeatTime = m_musicManager.GetCurrentBeat();
        double currentOriginBeat = GetCurrentOriginBeat();
        foreach (Note note in m_notes)
        {
            float proportionAcross = GetStaffXProportion(note.m_startBeat);
            float noteX = GetStaffNoteX(proportionAcross);
            float noteHeight = GetStaffNoteHeight(note.m_noteID);
            note.m_spawnedNote.transform.localPosition = new Vector3(noteX, noteHeight, 0.0f);
            if (note.m_spawnedSharp != null)
            {
                note.m_spawnedSharp.transform.localPosition = new Vector3(noteX, noteHeight, 0.0f);
            }


            float finalSize = 1.0f;

            float noteAge = (float)(currentBeatTime - note.m_startBeat);
            float fadeIn = Mathf.Clamp(noteAge * m_noteGrowInSpeed, 0.0f, 1.0f);
            if (fadeIn < 1.0f)
            {
                finalSize *= m_growCurve.Evaluate(fadeIn);
            }

            float noteTimeToLive = ((float)(note.m_startBeat - currentOriginBeat)) + m_noteFadeOutBeatOffset;
            if (note.m_ignored)
            {
                noteTimeToLive = (m_noteFadeOutBeats * 2.0f) - noteAge;
                note.m_spawnedNote.GetComponent<Image>().color = (noteTimeToLive % 0.25f > 0.125 ? Color.red : Color.white);
            }

            if (noteTimeToLive < m_noteFadeOutBeats)
            {
                float fadeOut = 1.0f - (noteTimeToLive / m_noteFadeOutBeats);
                finalSize *= m_shrinkCurve.Evaluate(fadeOut);
            }

            note.m_spawnedNote.transform.localScale = new Vector3(finalSize, finalSize, finalSize);
            if (note.m_spawnedSharp != null)
            {
                note.m_spawnedSharp.transform.localScale = new Vector3(finalSize, finalSize, finalSize);
            }

            if (noteTimeToLive < 0.0f)
            {
                m_toDestroy.Add(note);
            }
        }

        foreach (Note note in m_toDestroy)
        {
            Destroy(note.m_spawnedNote);
            if (note.m_spawnedSharp != null)
            {
                Destroy(note.m_spawnedSharp);
            }
            m_notes.Remove(note);
        }
        m_toDestroy.Clear();
    }

    void PositionGhosts()
    {
        double currentBeatTime = m_musicManager.GetCurrentBeat();
        //double currentOriginBeat = GetCurrentOriginBeat();

        for (int i = 0; i < m_beatLines.Length; i++)
        {
            double lineTime = MidiUtil.RoundBeatToLatestInterval(currentBeatTime, 1.0) + (1.0 + i);
            float proportionAlong = GetStaffXProportion(lineTime);
            float nextBeatX = GetStaffNoteX(proportionAlong);
            m_beatLines[i].transform.localPosition = new Vector3(nextBeatX, 0.0f);
            m_beatLines[i].GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f - proportionAlong);
        }
    }
}
