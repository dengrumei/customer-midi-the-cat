﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoicePanelBehaviour : MonoBehaviour
{
    public Text m_choiceText;
    public StaticStaffUI m_staffUI;
    KeyboardGrid m_keyboardGrid;
    DialoguePanelBehaviour m_parentPanel;
    Image m_background;
    Color m_startBackgroundColour;
    CanvasGroup m_keyCanvasGroup;

    MusicMenu m_choice;
    /// <summary>
    /// How many notes we've correctly pressed
    /// </summary>
    int m_choiceProgress;

    /// <summary>
    /// How far along our choice notes are we
    /// </summary>
    int m_textProgress;

    //MidiInput midiIO;

    string GetColouredChoiceString(MusicMenu choice, float progress = 0)
    {
        string notesToPlayInEnglish = SolresolUtils.TranslateSolresolToEnglish(SolresolUtils.NoteIDsToSolresol(choice.ChoiceNotes));
        string choiceString = choice.GetStandardText();
        int foundPos = choiceString.IndexOf(notesToPlayInEnglish, 0, System.StringComparison.InvariantCultureIgnoreCase);
        if (foundPos >= 0)
        {
            string wordString = choiceString.Substring(foundPos, notesToPlayInEnglish.Length);
            string colouredWordString = "";

            for (int i = 0; i < wordString.Length; i++)
            {
                float wordProgress = (float)i / (float)wordString.Length;
                Color noteColour = Color.white;
                if (progress <= wordProgress)
                {
                    int choiceNote = (int)(choice.ChoiceNotes.Length * wordProgress);
                    if (choiceNote >= choice.ChoiceNotes.Length)
                    {
                        choiceNote = choice.ChoiceNotes.Length - 1;
                    }
                    int notesFromA = MidiUtil.NotesFromA(choice.ChoiceNotes[choiceNote]) % 12;
                    noteColour = m_keyboardGrid.m_noteColoursFromA[notesFromA];
                }
                colouredWordString += "<color=#" + ColorUtility.ToHtmlStringRGB(noteColour) + ">" + wordString[i] + "</color>";
            }

            choiceString = choiceString.Substring(0, foundPos) + colouredWordString + choiceString.Substring(foundPos + notesToPlayInEnglish.Length);
        }
        return choiceString;
    }

    public void SetupChoice(MusicMenu choice, DialoguePanelBehaviour dialoguePanel)
    {
        m_keyboardGrid = GetComponentInChildren<KeyboardGrid>();


        m_staffUI.SetNotes(choice.ChoiceNotes, m_keyboardGrid.m_noteColoursFromA);

        string choiceString = GetColouredChoiceString(choice);
        m_choiceText.text = choiceString;
        m_keyCanvasGroup = m_keyboardGrid.GetComponent<CanvasGroup>();
        m_choice = choice;
        m_parentPanel = dialoguePanel;
    }

    void OnEnable()
    {
        //midiIO = FindObjectOfType<MidiInput>();
        //midiIO.MidiIOEvent += HandleMidiIOEvent;
        m_background = GetComponent<Image>();
        m_startBackgroundColour = m_background.color;
    }

    void OnDisable()
    {
        //midiIO.MidiIOEvent -= HandleMidiIOEvent;
    }

    public int GetChoiceProgress()
    {
        return m_choiceProgress;
    }

    public int GetTextProgress()
    {
        return m_textProgress;
    }

    public int[] GetChoiceNotes()
    {
        return m_choice.ChoiceNotes;
    }

    public MusicMenu GetChoice()
    {
        return m_choice;
    }

    public MusicMenu HandleMidiIOEvent (int note, bool down, float velocity)
    {
        if (down)
        {
            int nextChoiceNote = m_choice.ChoiceNotes[m_textProgress];
            if (nextChoiceNote == -1)
            {
                Debug.Log("We have a -1 note choice :(");
            }
            if (((note % 12) == (nextChoiceNote % 12)))
            {
                if (m_textProgress + 1 >= m_choice.ChoiceNotes.Length)
                {
                    Debug.Log("Made choice");
                    return m_choice;
                }
                else
                {
                    //Debug.Log("Best Progress = " + m_parentPanel.GetBestProgress() + ", choice progress = " + m_choiceProgress);
                    m_choiceProgress++;
                    m_textProgress++;
                    if (m_choice.ChoiceNotes[m_textProgress] == -1)
                    {
                        m_textProgress++;
                    }
                }
            }
            else
            {
                // Go back a word
                if (m_textProgress > 0 && m_choice.ChoiceNotes[m_textProgress - 1] == -1)
                {
                    m_textProgress--;
                }
                while (m_textProgress > 0)
                {
                    if (m_choice.ChoiceNotes[m_textProgress - 1] == -1)
                    {
                        break;
                    }
                    m_textProgress--;
                    m_choiceProgress--;
                }
            }
            m_keyboardGrid.SetNoteHighlighted(nextChoiceNote, false);
            m_staffUI.SetProgress(m_choiceProgress);
            m_choiceText.text = GetColouredChoiceString(m_choice, (float)m_choiceProgress / (float)m_choice.ChoiceNotes.Length);
        }
        return null;
    }

    void Update()
    {
        // Highlight next key based on choice progress
        if (m_choice.ChoiceNotes != null && m_choice.ChoiceNotes.Length > 0)
        {
            if (m_parentPanel.GetBestProgress() == m_choiceProgress)
            {
                int nextChoiceNote = m_choice.ChoiceNotes[m_textProgress];
                m_keyboardGrid.SetNoteHighlighted(nextChoiceNote, true);
                m_background.CrossFadeColor(m_startBackgroundColour, 1.0f, true, true);
                m_choiceText.CrossFadeAlpha(1.0f, 1.0f, true);
                m_staffUI.CrossFadeAlpha(1.0f, 1.0f, true);
                m_keyCanvasGroup.alpha = Mathf.MoveTowards(m_keyCanvasGroup.alpha, 1.0f, Time.deltaTime);
            }
            else
            {
                m_keyboardGrid.SetAllNoteHighlights(false);
                m_background.CrossFadeColor(new Color(0.5f, 0.5f, 0.5f, 0.5f), 1.0f, true, true);
                m_choiceText.CrossFadeAlpha(0.0f, 1.0f, true);
                m_staffUI.CrossFadeAlpha(0.0f, 1.0f, true);
                m_keyCanvasGroup.alpha = Mathf.MoveTowards(m_keyCanvasGroup.alpha, 0.0f, Time.deltaTime);
            }
        }
    }
}
