﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPanelManager : MonoBehaviour, StateHolder<TutorialPanelManager.State>
{
    public GameObject m_tutorialPanelPrefab;
    public GameObject m_taughtActionPanelPrefab;
    public ActionSetConfig m_actionSet;

    PlayerControl m_player;
    ExitBehaviour[] m_exits;
    TutorialPanelBehaviour m_currentPanel;
    DimmerBehaviour m_dimmer;
    float m_currentPanelFade;
    List<ActionConfig> m_taughtActionConfigs;
    List<TutorialPanelBehaviour> m_taughtActionPanels;
    float m_taughtActionsPanelFade;

    public float m_fadeSpeed = 1.0f;

    [Serializable]
    public class State
    {
        public int[] m_performanceCount;
        public State()
        {
            m_performanceCount = null;
        }
        public State(int actionCount)
        {
            m_performanceCount = new int[actionCount];
        }
    }

    State m_state;

    public void SetState(State s)
    {
        m_state = s;
    }

    public State GetState()
    {
        return m_state;
    }

    // Use this for initialization
    void Start ()
    {
        m_player = FindObjectOfType<PlayerControl>();

        if (m_state == null)
        {
            m_state = new State(m_actionSet.m_actionConfigs.Length);
        }
        m_exits = FindObjectsOfType<ExitBehaviour>();
        m_dimmer = FindObjectOfType<DimmerBehaviour>();
        m_taughtActionConfigs = new List<ActionConfig> ();
        m_taughtActionPanels = new List<TutorialPanelBehaviour> ();
	}

    public void DoneAction(ActionConfig config)
    {
        // Only count actions that match the one we're teaching
        if (m_currentPanel == null || config != m_currentPanel.m_actionConfig)
        {
            return;
        }
        int element = Array.IndexOf(m_actionSet.m_actionConfigs, m_currentPanel.m_actionConfig);
        if (element >= 0)
        {
            if (m_currentPanel.m_actionConfig != config && m_currentPanel.m_actionConfig.m_inputPattern.m_inputs.Length == config.m_inputPattern.m_inputs.Length)
            {
                // Doing other actions of equal length should reset the count
                m_state.m_performanceCount[element] = 0;
                return;
            }
            m_state.m_performanceCount[element]++;
        }
    }

    public bool HasTaughtAction(ActionConfig config)
    {
        if (m_currentPanel != null && m_currentPanel.m_actionConfig == config)
        {
            // Currently teaching it!
            return true;
        }
        int element = Array.IndexOf(m_actionSet.m_actionConfigs, config);
        if (element >= 0)
        {
            // or you've performed it already
            return m_state.m_performanceCount[element] > 0;
        }
        return false;
    }
    public bool HasFinishedTeachingAction(ActionConfig config)
    {
        int element = Array.IndexOf(m_actionSet.m_actionConfigs, config);
        if (element >= 0)
        {
            // or you've performed it already
            return m_state.m_performanceCount[element] >= 3;
        }
        return false;
    }

    bool IsPlayerOverExit()
    {
        foreach(ExitBehaviour exit in m_exits)
        {
            if (exit.IsOnTileWith(m_player.GetPlayerMidPoint()))
            {
                return true;
            }
        }
        return false;
    }
	
	// Update is called once per frame
	void Update ()
    {
		if (m_currentPanel == null && m_player.isActiveAndEnabled && !m_player.GetIgnoreWhiteInputs() && !IsPlayerOverExit())
        {
            ActionConfig actionToTeach = null;
            for(int i = 0; i < m_actionSet.m_actionConfigs.Length; i++)
            {
                if (m_state.m_performanceCount[i] >= 3)
                {
                    continue;
                }
                ActionConfig action = m_actionSet.m_actionConfigs[i];
                if (action.IsValidInState(m_player.GetState()) && action.ShouldShowTutorial(m_player.GetState()) && !action.m_startsLearnt)
                {
                    actionToTeach = action;
                    break;
                }
            }

            if (actionToTeach != null)
            {
                GameObject newPanel = GameObject.Instantiate(m_tutorialPanelPrefab, transform);
                m_currentPanel = newPanel.GetComponent<TutorialPanelBehaviour>();
                m_currentPanel.m_actionConfig = actionToTeach;
                m_dimmer.FadeToAlpha(0.8f);
            }
        }

        if (m_currentPanel != null)
        {
            ActionConfig action = m_currentPanel.m_actionConfig;
            int explainedElement = Array.IndexOf(m_actionSet.m_actionConfigs, action);
            if (m_state.m_performanceCount[explainedElement] >= 3
                || !m_player.isActiveAndEnabled
                || m_player.GetIgnoreWhiteInputs()
                || !action.IsValidInState(m_player.GetState())
                || !action.ShouldShowTutorial(m_player.GetState())
                || IsPlayerOverExit())
            {
                m_currentPanelFade -= Time.deltaTime * m_fadeSpeed;
            }
            else
            {
                m_currentPanelFade += Time.deltaTime * m_fadeSpeed;
            }
        }

        if (m_currentPanel != null && m_currentPanelFade <= 0)
        {
            Destroy(m_currentPanel.gameObject);
            m_dimmer.FadeToAlpha(0.0f);
            m_currentPanel = null;
            m_currentPanelFade = 0.0f;
        }
        if (m_currentPanelFade > 1.0f)
        {
            m_currentPanelFade = 1.0f;
        }

        // Actually set fade
        if (m_currentPanel != null)
        {
            m_currentPanel.SetAlpha(m_currentPanelFade);
        }

        if (Input.GetKeyDown(KeyCode.F4))
        {
            if (m_state != null)
            {
                for(int i = 0; i < m_state.m_performanceCount.Length; i++)
                {
                    m_state.m_performanceCount[i] = 100;
                }
            }
        }
        HandleTaughtActionPanels ();
	}

    void HandleTaughtActionPanels()
    {
        for (int i = m_actionSet.m_actionConfigs.Length - 1; i >= 0; i--)
        {
            ActionConfig action = m_actionSet.m_actionConfigs [i];
            if (!action.m_startsLearnt && m_state.m_performanceCount [i] >= 3)
            {
                if (!m_taughtActionConfigs.Contains(action))
                {
                    GameObject newPanel = GameObject.Instantiate(m_taughtActionPanelPrefab, transform);
                    TutorialPanelBehaviour newPanelBehaviour = newPanel.GetComponent<TutorialPanelBehaviour>();
                    newPanelBehaviour.m_actionConfig = m_actionSet.m_actionConfigs[i];
                    newPanelBehaviour.m_cornerPositionID = m_taughtActionConfigs.Count;
                    m_taughtActionConfigs.Add (action);
                    m_taughtActionPanels.Add (newPanelBehaviour);
                }
            }
        }
        float targetAlpha = 1.0f;

        if (m_currentPanelFade > 0.0f || m_player.GetIgnoreWhiteInputs())
        {
            targetAlpha = 0.0f;
        }
        m_taughtActionsPanelFade = Mathf.MoveTowards (m_taughtActionsPanelFade, targetAlpha, Time.deltaTime * m_fadeSpeed);
        for (int i = 0; i < m_taughtActionPanels.Count; i++)
        {
            m_taughtActionPanels[i].SetAlpha (m_taughtActionsPanelFade);
        }
    }
}
