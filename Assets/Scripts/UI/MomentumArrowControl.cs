﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MomentumArrowControl : MonoBehaviour
{
    public GameObject m_momentumArrowPrefab;
    public GameObject m_momentumParticlesPrefab;
    public Color[] colours;
    public float m_fadeInSpeed = 4.0f;
    public float m_fadeOutSpeed = 4.0f;
    public Vector3 m_fixedOffset;
    public Vector3 m_scaledOffset;

    SpriteRenderer[] m_momentumArrows;
    ParticleSystem[] m_momentumParticles;
    float[] m_arrowVisibleTimer;

    PlayerControl m_player;
    //MoveStorage m_moveStore;
    CatGraphicsControl m_catGraphics;
    MusicManager m_musicManager;

    int m_previousMomentum = 0;

    // Use this for initialization
    void Start ()
    {
        m_momentumArrows = new SpriteRenderer[3];
        m_arrowVisibleTimer = new float[m_momentumArrows.Length];
        m_momentumParticles = new ParticleSystem[m_momentumArrows.Length];
        for (int i = 0; i < m_momentumArrows.Length; i++)
        {
            GameObject newMomentumArrow = GameObject.Instantiate(m_momentumArrowPrefab, transform);
            m_momentumArrows[i] = newMomentumArrow.GetComponent<SpriteRenderer>();
            m_momentumArrows[i].enabled = false;
            m_momentumArrows[i].color = colours[i];
            GameObject newParticlesObject = GameObject.Instantiate(m_momentumParticlesPrefab, transform);
            m_momentumParticles[i] = newParticlesObject.GetComponent<ParticleSystem>();
            ParticleSystem.MainModule main = m_momentumParticles[i].main;
            main.startColor = colours[i];

            // Hacky fix to make particle emission not crash >_<
            m_momentumParticles[i].Emit(1);
            m_momentumParticles[i].Clear();
        }
        m_player = FindObjectOfType<PlayerControl>();
        //m_moveStore = m_player.GetComponent<MoveStorage>();
        m_catGraphics = FindObjectOfType<CatGraphicsControl>();
        m_musicManager = FindObjectOfType<MusicManager>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        int playerMomentum = m_player.GetMomentum();
        
        bool shouldFlip = m_player.GetLastDirection() == -1;

        float offsetDirection = 1.0f;
        if (shouldFlip)
        {
            offsetDirection = -1.0f;
        }

        //Vector3 momentumArrowStart = m_catGraphics.transform.position + new Vector3(0.0f, 1.5f, 0.0f);
        Vector3 momentumArrowStart = m_catGraphics.transform.position + m_fixedOffset + m_scaledOffset * m_player.GetLastDirection();
        //momentumArrowStart = Utils.RoundVector3(momentumArrowStart);
        float beatDeltaTime = (float)m_musicManager.SecondsToBeat(Time.deltaTime);

        for (int i = 0; i < m_momentumArrows.Length; i++)
        {
            SpriteRenderer arrow = m_momentumArrows[i];
            
            arrow.transform.position = momentumArrowStart + new Vector3(i * 0.3f * offsetDirection, 0.0f, -4.0f);
            m_momentumParticles[i].transform.position = arrow.transform.position;

            bool shouldBeVisible = i < playerMomentum - 1;
            if (shouldBeVisible)
            {
                m_arrowVisibleTimer[i] = Mathf.Min(m_arrowVisibleTimer[i] + (beatDeltaTime * m_fadeInSpeed), 1.0f);
            }
            else
            {
                m_arrowVisibleTimer[i] = Mathf.Max(m_arrowVisibleTimer[i] - (beatDeltaTime * m_fadeOutSpeed), 0.0f);
            }
            float scale = m_arrowVisibleTimer[i];
            arrow.transform.localScale = new Vector3(scale, scale, scale);
            arrow.enabled = scale > 0.0f;
            arrow.flipX = shouldFlip;
            if (shouldBeVisible && playerMomentum > m_previousMomentum && i == playerMomentum - 2)
            {
                ParticleSystem.EmitParams emitParams = new ParticleSystem.EmitParams();
                if (shouldFlip)
                {
                    emitParams.rotation = 180;
                }
                // This can CRASH Unity when loading, as emission parameters will break if the Particle system is new (and we're emitting on the first frame)
                // Which happens if the momentum was saved as >1.  "Start" in this file has the "fix".
                m_momentumParticles[i].Emit(emitParams, 10);
                //m_momentumParticles[i].Emit(10);
            }
        }

        if (playerMomentum != m_previousMomentum)
        {
            m_previousMomentum = playerMomentum;
        }
    }
}
