﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CommandInfo("", "WorldSay", "Say something in the world")]
public class WorldSay : Command, ILocalizable
{
    [TextArea(5, 10)]
    [SerializeField] protected string storyText = "";

    [Tooltip("Notes about this story text for other authors, localization, etc.")]
    [SerializeField] protected string description = "";

    [SerializeField] protected Character character;

    public override void OnEnter()
    {
        // Stop all other world say
        DialogueHolder[] allHolders = FindObjectsOfType<DialogueHolder>();
        foreach(DialogueHolder holder in allHolders)
        {
            holder.StopWorldSay();
        }
        DialogueHolder dialogueHolder = GetCharacter().GetComponentInParent<DialogueHolder>();
        if (storyText.Length == 0)
        {
            dialogueHolder.StopWorldSay();
            Continue();
            return;
        }
        int bubbleDirection = character == null ? 1 : -1;
        dialogueHolder.WorldSay(storyText, bubbleDirection, CommandIndex == 0);
        dialogueHolder.LineFinishedEvent += OnWaitComplete;
        
        MidiInput.Instance.MidiIOEvent += HandleMidiIOEvent;
    }

    public override void OnExit()
    {
        MidiInput.Instance.MidiIOEvent -= HandleMidiIOEvent;
    }

    void HandleMidiIOEvent(int note, bool down, float velocity)
    {
        DialogueHolder dialogueHolder = GetCharacter().GetComponentInParent<DialogueHolder>();
        dialogueHolder.HandleMidiIOEvent(note, down, velocity);
    }
    /*
        if (down)
        {
            Skip();
        }
    }*/
    /*
    public void Skip()
    {
        if (IsExecuting)
        {
            CancelInvoke();
            Continue();
        }
    }*/

    Character GetCharacter()
    {
        if (character != null)
        {
            return character;
        }
        else
        {
            return ParentBlock.GetFlowchart().GetComponent<Character>();
        }
    }

    protected virtual void OnWaitComplete()
    {
        DialogueHolder dialogueHolder = GetCharacter().GetComponentInParent<DialogueHolder>();
        dialogueHolder.LineFinishedEvent -= OnWaitComplete;

        Continue();
    }

    public override void OnStopExecuting()
    {
        DialogueHolder dialogueHolder = GetCharacter().GetComponentInParent<DialogueHolder>();
        dialogueHolder.StopWorldSay();
    }

    public override string GetSummary()
    {
        string namePrefix = "";
        if (character != null)
        {
            namePrefix = character.NameText + ": ";
        }
        return namePrefix + "\"" + storyText + "\"";
    }

    #region ILocalizable implementation

    public virtual string GetStandardText()
    {
        return storyText;
    }

    public virtual void SetStandardText(string standardText)
    {
        storyText = standardText;
    }

    public virtual string GetDescription()
    {
        return description;
    }

    public virtual string GetStringId()
    {
        // String id for WorldSay commands is WORLDSAY.<Localization Id>.<Command id>.[Character Name]
        string stringId = "WORLDSAY." + GetFlowchartLocalizationId() + "." + itemId + ".";
        if (character != null)
        {
            stringId += character.NameText;
        }

        return stringId;
    }

    #endregion
}
