﻿using UnityEngine;

[RequireComponent(typeof(Controller2D))]
public class Player : MonoBehaviour
{
    public float maxJumpHeight = 4f;
    public float minJumpHeight = 1f;
    public float timeToJumpApex = .4f;
    private float accelerationTimeAirborne = .2f;
    public float accelerationTimeGrounded = 3.0f;
    public float moveSpeed = 2.5f;

    public Vector2 wallJumpClimb;
    public Vector2 wallJumpOff;
    public Vector2 wallLeap;

    public bool canDoubleJump;
    private bool isDoubleJumping = false;

    public float wallSlideSpeedMax = 3f;
    public float wallStickTime = .25f;
    private float timeToWallUnstick;
    public float wallClimbSpeed = 3.0f;

    private float gravity;
    private float maxJumpVelocity;
    private float minJumpVelocity;
    private Vector3 velocity;
    private float velocityXSmoothing;

    private Controller2D controller;

    private Vector2 directionalInput;
    private bool wallSliding;
    private int wallDirX;

    Animator m_animator;
    Transform m_innerCat;
    //MusicManager m_musicManager;

    public float m_rotateSpeed = 180.0f;

    private void Start()
    {
        controller = GetComponent<Controller2D>();
        gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);
        m_animator = GetComponentInChildren<Animator>();
        m_innerCat = m_animator.transform.GetChild(1);
        //m_musicManager = FindObjectOfType<MusicManager>();
    }

    string m_currentAnimation = null;
    public void ActuallySetTrigger(string name)
    {
        if (name != m_currentAnimation)
        {
            if (m_currentAnimation != null)
            {
                m_animator.ResetTrigger(m_currentAnimation);
            }
            m_animator.SetTrigger(name);
            m_currentAnimation = name;
        }
    }

    private void Update()
    {
        CalculateVelocity();
        HandleWallSliding();

        controller.Move(velocity * Time.deltaTime, directionalInput);

        if (controller.collisions.above || controller.collisions.below)
        {
            velocity.y = 0f;
        }
    }

    public void SetDirectionalInput(Vector2 input)
    {
        directionalInput = input;
    }

    public void OnJumpInputDown()
    {
        bool jumping = false;
        if (wallSliding || ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below))
        {
            if (wallDirX == directionalInput.x)
            {
                velocity.x = -wallDirX * wallJumpClimb.x;
                velocity.y = wallJumpClimb.y;
            }
            else if (directionalInput.x == 0)
            {
                velocity.x = -wallDirX * wallJumpOff.x;
                velocity.y = wallJumpOff.y;
            }
            else
            {
                velocity.x = -wallDirX * wallLeap.x;
                velocity.y = wallLeap.y;
            }
            isDoubleJumping = false;
            jumping = true;
        }
        if (controller.collisions.below)
        {
            velocity.y = maxJumpVelocity;
            isDoubleJumping = false;
            jumping = true;
        }
        if (canDoubleJump && !controller.collisions.below && !isDoubleJumping && !wallSliding)
        {
            velocity.y = maxJumpVelocity;
            isDoubleJumping = true;
            jumping = true;
        }

        if (jumping)
        {
            AnimateJump();
        }
    }

    void AnimateJump()
    {
        if (m_currentAnimation != "StartJump")
        {
            ActuallySetTrigger("StartJump");
        }
        else
        {
            m_animator.Play("Jump", 0, 0.0f);
        }
    }

    public void OnJumpInputUp()
    {
        if (velocity.y > minJumpVelocity)
        {
            velocity.y = minJumpVelocity;
        }
    }

    private void HandleWallSliding()
    {
        wallDirX = (controller.collisions.left) ? -1 : 1;
        wallSliding = false;
        if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0)
        {
            wallSliding = true;

            if (velocity.y < -wallSlideSpeedMax)
            {
                velocity.y = -wallSlideSpeedMax;
            }

            if (timeToWallUnstick > 0f)
            {
                velocityXSmoothing = 0f;
                velocity.x = 0f;
                if (directionalInput.x != wallDirX && directionalInput.x != 0f)
                {
                    timeToWallUnstick -= Time.deltaTime;
                }
                else
                {
                    timeToWallUnstick = wallStickTime;
                }
            }
            else
            {
                timeToWallUnstick = wallStickTime;
            }
            if (directionalInput.y != 0.0f)
            {
                velocity.y = directionalInput.y * wallClimbSpeed;
            }
        }
    }

    private void CalculateVelocity()
    {
        float targetVelocityX = directionalInput.x * moveSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below ? accelerationTimeGrounded : accelerationTimeAirborne));

        if (controller.collisions.below && velocity.y == 0)
        {
            if (Mathf.Abs(velocity.x) < 0.5f)
            {
                ActuallySetTrigger("StopWalk");
            }
            else
            {
                ActuallySetTrigger("StartWalk");
            }
        }
        if (m_currentAnimation == "StartWalk")
        {
            m_animator.speed = Mathf.Abs(velocity.x) / 1.25f;
        }
        else
        {
            m_animator.speed = 1.0f;
        }
        Quaternion targetInnerRotation = m_innerCat.transform.localRotation;
        if (velocity.x > 0.0f)
        {
            targetInnerRotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
        }
        else if (velocity.x < 0.0f)
        {
            targetInnerRotation = Quaternion.Euler(0.0f, 270.0f, 0.0f);
        }
        m_innerCat.transform.localRotation = Quaternion.RotateTowards(m_innerCat.transform.localRotation, targetInnerRotation, m_rotateSpeed * Time.deltaTime);

        /*if (m_musicManager != null)
        {
            m_musicManager.SetSpeed(velocity.x);
        }*/

        velocity.y += gravity * Time.deltaTime;
    }
}
