﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace GamingGarrison
{
    public class TiledTSXImporter
    {
        static bool CreateTileSprite(string sourcePath, string targetPath, int pixelsPerUnit, int width, int height)
        {
            TextureImporter ti = AssetImporter.GetAtPath(targetPath) as TextureImporter;

            TextureImporterSettings textureSettings = new TextureImporterSettings();
            ti.ReadTextureSettings(textureSettings);

            // Pivot should be moved from center based on size difference from pixelsPerUnit
            SpriteMeshType meshType = SpriteMeshType.FullRect;
            SpriteAlignment alignment = SpriteAlignment.Custom;
            Vector2 pivot = new Vector2(0.5f, 0.5f) + Vector2.Scale(new Vector2(0.5f, 0.5f), new Vector2(1.0f - (width / pixelsPerUnit), 1.0f - (height / pixelsPerUnit)));
            FilterMode filterMode = FilterMode.Point;

            if (textureSettings.spritePixelsPerUnit != pixelsPerUnit
                || textureSettings.spriteMeshType != meshType
                || textureSettings.spriteAlignment != (int)alignment
                || textureSettings.spritePivot != pivot
                || textureSettings.filterMode != filterMode)
            {
                textureSettings.spritePixelsPerUnit = pixelsPerUnit;
                textureSettings.spriteMeshType = meshType;
                textureSettings.spriteAlignment = (int)alignment;
                textureSettings.spritePivot = pivot;
                textureSettings.filterMode = filterMode;

                ti.SetTextureSettings(textureSettings);

                EditorUtility.SetDirty(ti);
                ti.SaveAndReimport();
            }

            return true;
        }

        static Tile CreateTileAsset(string spritePath, string tileAssetPath, Tile.ColliderType colliderType)
        {
            Tile tile = AssetDatabase.LoadAssetAtPath<Tile>(tileAssetPath);
            Sprite toAssign = AssetDatabase.LoadAssetAtPath<Sprite>(spritePath);
            if (toAssign == null)
            {
                Debug.LogError("Sprite at " + spritePath + " is null when creating Tile");
            }

            if (tile == null)
            {
                tile = Tile.CreateInstance<Tile>();
                tile.sprite = toAssign;
                tile.colliderType = colliderType;

                AssetDatabase.CreateAsset(tile, tileAssetPath);
            }
            else if (tile.sprite != toAssign || tile.colliderType != colliderType)
            {
                tile.sprite = toAssign;
                tile.colliderType = colliderType;
                EditorUtility.SetDirty(tile);
            }

            return tile;
        }

        static void CopyTileImages(TSX.Tile[] tiles, string pathWithoutFile, string tilesetSpriteTargetDir, string tilesetTileTargetDir, string[] imageSourcePaths, string[] imageTargetPaths, string[] tileTargetPaths)
        {
            for (int i = 0; i < tiles.Length; i++)
            {
                TSX.Tile tile = tiles[i];
                string imageSourcePath = tile.image.source;
                imageSourcePath = Path.Combine(pathWithoutFile, imageSourcePath);
                imageSourcePaths[i] = imageSourcePath;

                string tileImageName = Path.GetFileName(imageSourcePath);
                string imageTargetPath = Path.Combine(tilesetSpriteTargetDir, tileImageName);
                imageTargetPaths[i] = imageTargetPath;

                EditorUtility.DisplayProgressBar("Copying...", imageSourcePath + " to " + imageTargetPath, (float)i / (float)tiles.Length);

                string tileName = Path.GetFileNameWithoutExtension(imageSourcePath) + ".asset";
                tileTargetPaths[i] = Path.Combine(tilesetTileTargetDir, tileName);

                if (File.Exists(imageSourcePath) && (!File.Exists(imageTargetPath) || (!File.GetLastWriteTime(imageSourcePath).Equals(File.GetLastWriteTime(imageTargetPath)))))
                {
                    File.Copy(imageSourcePath, imageTargetPath, true);
                    AssetDatabase.ImportAsset(imageTargetPath, ImportAssetOptions.ForceSynchronousImport);
                }
            }
        }

        static bool CreateTileAssets(TSX.Tile[] tiles, TSX.Tileset tileset, string[] imageSourcePaths, string[] imageTargetPaths, string[] tileTargetPaths, out Tile[] outputTiles)
        {
            outputTiles = new Tile[tiles.Length];
            int pixelsPerUnit = Mathf.Max(tileset.tilewidth, tileset.tileheight);

            bool success = true;
            for (int i = 0; i < tiles.Length; i++)
            {
                EditorUtility.DisplayProgressBar("Importing " + tileset.name + " tileset sprites", null, (float)i / (float)tiles.Length);
                if (File.Exists(imageTargetPaths[i]))
                {
                    TSX.Tile tile = tiles[i];
                    success = CreateTileSprite(imageSourcePaths[i], imageTargetPaths[i], pixelsPerUnit, tile.image.width, tile.image.height);
                    if (!success)
                    {
                        break;
                    }
                }
            }
            for (int i = 0; i < tiles.Length; i++)
            {
                EditorUtility.DisplayProgressBar("Importing " + tileset.name + " tileset tiles", null, (float)i / (float)tiles.Length);
                if (File.Exists(imageTargetPaths[i]))
                {
                    TSX.Tile tile = tiles[i];

                    Tile.ColliderType colliderType = Tile.ColliderType.None;
                    if (tile.HasCollisionData())
                    {
                        colliderType = Tile.ColliderType.Sprite;
                    }
                    Tile newTile = CreateTileAsset(imageTargetPaths[i], tileTargetPaths[i], colliderType);
                    success = newTile != null;
                    if (!success)
                    {
                        break;
                    }
                    outputTiles[i] = newTile;
                }
            }
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            return success;
        }

        public static Tile[] ImportTSXFile(string path, string tilesetDir, out TSX.Tileset tilesetOut)
        {
            tilesetOut = null;
            if (!ImportUtils.CreateAssetFolderIfMissing(tilesetDir, true))
            {
                return null;
            }

            string pathWithoutFile = Path.GetDirectoryName(path);

            Debug.Log("Loading TSX file from " + path + " into " + tilesetDir);

            TSX.Tileset tileset = ImportUtils.ReadXMLIntoObject<TSX.Tileset>(path);
            if (tileset == null)
            {
                return null;
            }
            tilesetOut = tileset;

            Debug.Log("Loading tileset = " + tileset.name);

            TSX.Tile[] tiles = tileset.tiles;

            if (tiles == null)
            {
                Debug.Log("Tiles not found");
                return null;
            }

            string tilesetSpriteTargetDir = tilesetDir + Path.DirectorySeparatorChar + tileset.name;
            if (!ImportUtils.CreateAssetFolderIfMissing(tilesetSpriteTargetDir, false))
            {
                return null;
            }
            string tilesetTileTargetDir = tilesetDir + Path.DirectorySeparatorChar + tileset.name + Path.DirectorySeparatorChar + "TileAssets";
            if (!ImportUtils.CreateAssetFolderIfMissing(tilesetTileTargetDir, false))
            {
                return null;
            }

            string[] imageSourcePaths = new string[tiles.Length];
            string[] imageTargetPaths = new string[tiles.Length];
            string[] tileTargetPaths = new string[tiles.Length];

            CopyTileImages(tiles, pathWithoutFile, tilesetSpriteTargetDir, tilesetTileTargetDir, imageSourcePaths, imageTargetPaths, tileTargetPaths);

            Tile[] tileAssets;
            bool success = CreateTileAssets(tiles, tileset, imageSourcePaths, imageTargetPaths, tileTargetPaths, out tileAssets);
            EditorUtility.ClearProgressBar();

            if (!success)
            {
                return null;
            }

            GameObject newPaletteGO = new GameObject(tileset.name, typeof(Grid));
            newPaletteGO.GetComponent<Grid>().cellSize = new Vector3(1.0f, 1.0f, 0.0f);
            GameObject paletteTilemapGO = new GameObject("Layer1", typeof(Tilemap), typeof(TilemapRenderer));
            paletteTilemapGO.transform.SetParent(newPaletteGO.transform);

            paletteTilemapGO.GetComponent<TilemapRenderer>().enabled = false;

            Tilemap paletteTilemap = paletteTilemapGO.GetComponent<Tilemap>();
            int x = 0;
            int y = 0;
            int z = 0;
            for (int i = 0; i < tileAssets.Length; i++)
            {
                paletteTilemap.SetTile(new Vector3Int(x, y, z), tileAssets[i]);
                x++;
                if (x >= 5)
                {
                    x = 0;
                    y--;
                }
            }
            string palettePath = tilesetTileTargetDir + Path.DirectorySeparatorChar + tileset.name + ".prefab";
            palettePath = palettePath.Replace('\\', '/');
            UnityEngine.Object newPrefab = PrefabUtility.CreateEmptyPrefab(palettePath);
            PrefabUtility.ReplacePrefab(newPaletteGO, newPrefab, ReplacePrefabOptions.Default);
            GameObject.DestroyImmediate(newPaletteGO);

            GridPalette gridPalette = ScriptableObject.CreateInstance<GridPalette>();
            gridPalette.cellSizing = GridPalette.CellSizing.Manual;
            gridPalette.name = "Palette Settings";
            AssetDatabase.AddObjectToAsset(gridPalette, palettePath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            return tileAssets;
        }
    }
}
