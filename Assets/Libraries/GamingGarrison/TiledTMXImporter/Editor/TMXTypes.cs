﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System;

namespace GamingGarrison
{
    namespace TMX
    {
        [XmlRoot(ElementName = "map")]
        public class Map
        {
            [XmlAttribute]
            public string version;

            [XmlAttribute]
            public string tiledversion;

            [XmlAttribute]
            public string orientation;

            [XmlAttribute]
            public string renderorder;

            [XmlAttribute]
            public int width;

            [XmlAttribute]
            public int height;

            [XmlAttribute]
            public int tilewidth;

            [XmlAttribute]
            public int tileheight;

            [XmlAttribute]
            public int nextobjectid;

            [XmlElement(ElementName = "tileset")]
            public TilesetReference[] tilesets;

            [XmlElement(ElementName = "layer")]
            public Layer[] layers;

            [XmlElement]
            public ObjectGroup objectgroup;
        }

        public class TilesetReference
        {
            [XmlAttribute]
            public int firstgid;

            [XmlAttribute]
            public string source;
        }

        public class Layer
        {
            [XmlAttribute]
            public string name;

            [XmlAttribute]
            public int width;

            [XmlAttribute]
            public int height;

            [XmlAttribute]
            public bool visible = true;

            /// <summary>
            /// Between 0 and 1
            /// </summary>
            [XmlAttribute]
            public float opacity = 1.0f;

            [XmlElement]
            public Properties properties;

            [XmlElement]
            public Data data;
        }

        public class Properties
        {
            [XmlElement]
            public Property[] property;

            public IDictionary<string, string> ToDictionary()
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                foreach(Property p in property)
                {
                    dictionary.Add(p.name, p.value);
                }
                return dictionary;
            }
        }

        public class Property
        {
            [XmlAttribute]
            public string name;

            [XmlAttribute]
            public string value;
        }

        public class Data
        {
            [XmlAttribute]
            public string encoding;

            [XmlText]
            public string text;
        }

        public class ObjectGroup
        {
            [XmlAttribute]
            public string name;

            [XmlAttribute]
            public bool visible = true;

            [XmlElement(ElementName = "object")]
            public Object[] objects;
        }

        public class Object
        {
            [XmlAttribute]
            public int id;

            [XmlAttribute]
            public string name;

            [XmlAttribute]
            public string type;

            [XmlAttribute]
            public float x;

            [XmlAttribute]
            public float y;

            [XmlAttribute]
            public float width;

            [XmlAttribute]
            public float height;

            [XmlAttribute]
            public float rotation;

            [XmlAttribute]
            public uint gid;

            [XmlAttribute]
            public bool visible = true;

            [XmlAttribute]
            public uint tid;

            [XmlElement]
            public Properties properties;

            [XmlElement]
            public Ellipse ellipse;

            [XmlElement]
            public Polygon polygon;

            [XmlElement]
            public Polyline polyline;

            [XmlElement]
            public Text text;
        }

        public class Ellipse
        {

        }

        public class Polygon
        {
            [XmlAttribute]
            public string points;
        }

        public class Polyline
        {
            [XmlAttribute]
            public string points;
        }

        public class Text
        {
            [XmlAttribute]
            public string fontfamily;

            [XmlAttribute]
            public int pixelsize;

            [XmlAttribute]
            public bool wrap;

            [XmlAttribute]
            public string color;

            [XmlAttribute]
            public bool bold;

            [XmlAttribute]
            public bool italic;

            [XmlAttribute]
            public bool underline;

            [XmlAttribute]
            public bool strikeout;

            [XmlAttribute]
            public bool kerning;

            [XmlAttribute]
            public string halign;

            [XmlAttribute]
            public string valign;

            [XmlText]
            public string text;
        }
    }
}
