﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEngine.Tilemaps;

namespace GamingGarrison
{
    public class TiledTMXImporter
    {
        class ImportedTileset
        {
            public Tile[] tiles;
            public int firstGID;
            public TSX.Tileset tileset;

            public ImportedTileset(Tile[] tilesIn, int firstGIDIn, TSX.Tileset tilesetIn)
            {
                tiles = tilesIn;
                firstGID = firstGIDIn;
                tileset = tilesetIn;
            }
        }

        const uint FLIPPED_HORIZONTALLY_FLAG = 0x80000000;
        const uint FLIPPED_VERTICALLY_FLAG = 0x40000000;
        const uint FLIPPED_DIAGONALLY_FLAG = 0x20000000;

        static void FindTileDataAndMatrix(uint gid, ImportedTileset[] importedTilesets, out Tile unityTile, out TSX.Tile tilesetTile, out Matrix4x4 matrix)
        {
            unityTile = null;
            tilesetTile = null;
            matrix = Matrix4x4.identity;

            bool flippedHorizontally = (gid & FLIPPED_HORIZONTALLY_FLAG) != 0;
            bool flippedVertically = (gid & FLIPPED_VERTICALLY_FLAG) != 0;
            bool flippedDiagonally = (gid & FLIPPED_DIAGONALLY_FLAG) != 0;

            gid &= ~(FLIPPED_HORIZONTALLY_FLAG | FLIPPED_VERTICALLY_FLAG | FLIPPED_DIAGONALLY_FLAG);

            ImportedTileset tilesetContainingID = null;
            for (int j = importedTilesets.Length - 1; j >= 0; --j)
            {
                int firstGID = importedTilesets[j].firstGID;
                if (firstGID <= gid)
                {
                    tilesetContainingID = importedTilesets[j];
                    break;
                }
            }

            if (tilesetContainingID != null)
            {
                int relativeID = (int)gid - tilesetContainingID.firstGID;
                for (int t = 0; t < tilesetContainingID.tiles.Length; t++)
                {
                    TSX.Tile tile = tilesetContainingID.tileset.tiles[t];
                    int id = tile.id;
                    if (id == relativeID)
                    {
                        unityTile = tilesetContainingID.tiles[t];
                        tilesetTile = tile;
                        break;
                    }
                }

                if (unityTile != null)
                {
                    if (flippedHorizontally || flippedVertically || flippedDiagonally)
                    {
                        if (flippedDiagonally)
                        {
                            matrix = Matrix4x4.Rotate(Quaternion.Euler(0.0f, 0.0f, 90.0f));
                            matrix = Matrix4x4.Scale(new Vector3(1.0f, -1.0f, 1.0f)) * matrix;
                        }

                        if (flippedHorizontally)
                        {
                            matrix = Matrix4x4.Scale(new Vector3(-1.0f, 1.0f, 1.0f)) * matrix;
                        }
                        if (flippedVertically)
                        {
                            matrix = Matrix4x4.Scale(new Vector3(1.0f, -1.0f, 1.0f)) * matrix;
                        }
                    }
                }
            }
        }

        static bool FillTilemapFromCSV(Tilemap tilemap, int width, int height, string csv, ImportedTileset[] importedTilesets)
        {
            string[] numbersAsStrings = csv.Split(',');
            Debug.Assert(numbersAsStrings.Length == width * height, "The CSV length isn't equal to the width times height in the TMX layer");

            bool anyTilesWithCollision = false;
            for (int i = 0; i < numbersAsStrings.Length; i++)
            {
                uint value;
                bool worked = uint.TryParse(numbersAsStrings[i], out value);
                if (!worked)
                {
                    Debug.LogError("Could not parse GID " + numbersAsStrings[i]);
                    return false;
                }

                Tile unityTile;
                TSX.Tile tilesetTile;
                Matrix4x4 matrix;
                FindTileDataAndMatrix(value, importedTilesets, out unityTile, out tilesetTile, out matrix);

                if (unityTile != null)
                {
                    int x = i % width;
                    int y = -((i / width) + 1);

                    Vector3Int pos = new Vector3Int(x, y, 0);

                    tilemap.SetTile(pos, unityTile);
                    tilemap.SetTransformMatrix(pos, matrix);

                    if (tilesetTile.HasCollisionData())
                    {
                        anyTilesWithCollision = true;
                    }
                }
                else if (value > 0)
                {
                    Debug.LogError("Could not find tile " + value + " in tilemap " + tilemap.name);
                    return false;
                }
            }

            if (anyTilesWithCollision)
            {
                tilemap.gameObject.AddComponent<TilemapCollider2D>();
            }

            return true;
        }

        static bool ImportLayer(TMX.Layer layer, GameObject newGrid, int layerID, ImportedTileset[] importedTilesets, ITilemapImportOperation[] importOperations)
        {
            GameObject newLayer = new GameObject(layer.name, typeof(Tilemap), typeof(TilemapRenderer));
            newLayer.transform.SetParent(newGrid.transform);
            Tilemap layerTilemap = newLayer.GetComponent<Tilemap>();
            if (layer.opacity < 1.0f)
            {
                layerTilemap.color = new Color(1.0f, 1.0f, 1.0f, layer.opacity);
            }
            bool worked = FillTilemapFromCSV(layerTilemap, layer.width, layer.height, layer.data.text, importedTilesets);
            if (!worked)
            {
                return false;
            }
            TilemapRenderer renderer = newLayer.GetComponent<TilemapRenderer>();
            renderer.sortingOrder = layerID;
            if (!layer.visible)
            {
                renderer.enabled = false;
            }

            IDictionary<string, string> properties = (layer.properties == null ? new Dictionary<string, string>() : layer.properties.ToDictionary());

            foreach (ITilemapImportOperation operation in importOperations)
            {
                operation.HandleCustomProperties(newLayer, properties);
            }
            Undo.RegisterCreatedObjectUndo(newLayer, "Import tile layer");
            return true;
        }

        static bool ImportMapObject(TMX.Object mapObject, ImportedTileset[] importedTilesets, GameObject newObjectLayer, TMX.Map map, ITilemapImportOperation[] importOperations, string tilesetDir)
        {
            Tile unityTile;
            TSX.Tile tilesetTile;
            Matrix4x4 matrix;
            FindTileDataAndMatrix(mapObject.gid, importedTilesets, out unityTile, out tilesetTile, out matrix);

            Vector2 pixelsToUnits = new Vector2(1.0f / map.tilewidth, -1.0f / map.tileheight);

            GameObject newObject = new GameObject(mapObject.name);
            newObject.transform.SetParent(newObjectLayer.transform);
            Vector2 corner = Vector2.Scale(new Vector2(mapObject.x, mapObject.y), pixelsToUnits);
            Vector2 pos = corner + Vector2.Scale(new Vector2(mapObject.width * 0.5f, mapObject.height * 0.5f), pixelsToUnits);
            newObject.transform.position = pos;

            if (mapObject.rotation != 0.0f)
            {
                newObject.transform.RotateAround(corner, new Vector3(0.0f, 0.0f, 1.0f), -mapObject.rotation);
            }

            if (unityTile != null)
            {
                SpriteRenderer renderer = newObject.AddComponent<SpriteRenderer>();
                renderer.sprite = unityTile.sprite;
                renderer.sortingOrder = map.layers.Length;
                if (unityTile.colliderType == Tile.ColliderType.Sprite)
                {
                    newObject.AddComponent<PolygonCollider2D>();
                }
                else if (unityTile.colliderType == Tile.ColliderType.Grid)
                {
                    newObject.AddComponent<BoxCollider2D>();
                }
                newObject.transform.position += new Vector3(0.0f, 1.0f, 0.0f); // ... Not sure why Tile object positions are a tile off vertically...
            }
            else
            {
                // If no tile used, must be a non-tile object of some sort (collision or text)
                if (mapObject.ellipse != null)
                {
                    EllipseCollider2D collider = newObject.AddComponent<EllipseCollider2D>();
                    collider.RadiusX = (mapObject.width * 0.5f) / map.tilewidth;
                    collider.RadiusY = (mapObject.height * 0.5f) / map.tileheight;
                }
                else if (mapObject.polygon != null)
                {
                    PolygonCollider2D collider = newObject.AddComponent<PolygonCollider2D>();
                    string points = mapObject.polygon.points;
                    collider.points = ImportUtils.PointsFromString(points, pixelsToUnits);
                }
                else if (mapObject.polyline != null)
                {
                    EdgeCollider2D collider = newObject.AddComponent<EdgeCollider2D>();
                    string points = mapObject.polyline.points;
                    collider.points = ImportUtils.PointsFromString(points, pixelsToUnits);
                }
                else if (mapObject.text != null)
                {
                    TextMesh textMesh = newObject.AddComponent<TextMesh>();
                    textMesh.text = mapObject.text.text;
                    textMesh.anchor = TextAnchor.MiddleCenter;

                    Color color = Color.white;
                    if (mapObject.text.color != null)
                    {
                        ColorUtility.TryParseHtmlString(mapObject.text.color, out color);
                    }
                    textMesh.color = color;

                    // Saving an OS font as an asset in unity (through script) is seemingly impossible right now in a platform independent way
                    // So we'll skip fonts for now

                    textMesh.fontSize = (int)mapObject.text.pixelsize; // Guess a good resolution for the font
                    float targetWorldTextHeight = (float)mapObject.text.pixelsize / (float)map.tileheight;
                    textMesh.characterSize = targetWorldTextHeight * 10.0f / textMesh.fontSize;

                    Renderer renderer = textMesh.gameObject.GetComponent<MeshRenderer>();
                    renderer.sortingOrder = map.layers.Length + 1;
                    renderer.sortingLayerID = SortingLayer.GetLayerValueFromName("Default");
                }
                else
                {
                    // Regular box collision
                    BoxCollider2D collider = newObject.AddComponent<BoxCollider2D>();
                    collider.size = new Vector2(mapObject.width / map.tilewidth, mapObject.height / map.tileheight);
                }
            }

            if (mapObject.visible == false)
            {
                Renderer renderer = newObject.GetComponent<Renderer>();
                if (renderer != null)
                {
                    renderer.enabled = false;
                }
            }

            IDictionary<string, string> properties = (mapObject.properties == null ? new Dictionary<string, string>() : mapObject.properties.ToDictionary());

            foreach (ITilemapImportOperation operation in importOperations)
            {
                operation.HandleCustomProperties(newObject, properties);
            }
            return true;
        }

        static bool CheckTilesArrayHasNoNulls(Tile[] importedTiles)
        {
            for (int j = 0; j < importedTiles.Length; j++)
            {
                if (importedTiles[j] == null)
                {
                    Debug.LogError("Some of the tiles have imported as null :o");
                    return false;
                }
            }
            return true;
        }

        public static bool ImportTMXFile(string path, string tilesetDir, Grid targetGrid)
        {
            string tmxParentFolder = Path.GetDirectoryName(path);

            TMX.Map map = ImportUtils.ReadXMLIntoObject<TMX.Map>(path);
            if (map == null)
            {
                return false;
            }
            if (map.tilesets != null)
            {
                // First we need to load (or import) all the tilesets referenced by the TMX file...
                ImportedTileset[] importedTilesets = new ImportedTileset[map.tilesets.Length];
                for (int i = 0; i < map.tilesets.Length; i++)
                {
                    TMX.TilesetReference tilesetReference = map.tilesets[i];

                    // The source path in the tileset reference is recorded relative to the tmx
                    string tsxPath = Path.Combine(tmxParentFolder, tilesetReference.source);
                    TSX.Tileset actualTileset;
                    Tile[] importedTiles = TiledTSXImporter.ImportTSXFile(tsxPath, tilesetDir, out actualTileset);
                    if (importedTiles == null || (importedTiles.Length > 0 && importedTiles[0] == null))
                    {
                        Debug.LogError("Failed to import tileset at " + tsxPath + " properly...");
                        return false;
                    }
                    if (!CheckTilesArrayHasNoNulls(importedTiles))
                    {
                        return false;
                    }
                    importedTilesets[i] = new ImportedTileset(importedTiles, tilesetReference.firstgid, actualTileset);
                    if (!CheckTilesArrayHasNoNulls(importedTilesets[i].tiles))
                    {
                        return false;
                    }
                }
                foreach (ImportedTileset importedTileset in importedTilesets)
                {
                    if (!CheckTilesArrayHasNoNulls(importedTileset.tiles))
                    {
                        return false;
                    }
                }

                if (map.layers != null)
                {
                    foreach (ImportedTileset importedTileset in importedTilesets)
                    {
                        if (!CheckTilesArrayHasNoNulls(importedTileset.tiles))
                        {
                            return false;
                        }
                    }
                    // Set up the Grid to store everything in
                    GameObject newGrid = null;
                    if (targetGrid != null)
                    {
                        newGrid = targetGrid.gameObject;
                        for (int i = newGrid.transform.childCount - 1; i >= 0; --i)
                        {
                            Undo.DestroyObjectImmediate(newGrid.transform.GetChild(i).gameObject);
                        }
                    }
                    else
                    {
                        newGrid = new GameObject("Grid", typeof(Grid));
                        newGrid.GetComponent<Grid>().cellSize = new Vector3(1.0f, 1.0f, 0.0f);
                        Undo.RegisterCreatedObjectUndo(newGrid, "Import map to new Grid");
                    }

                    foreach (ImportedTileset importedTileset in importedTilesets)
                    {
                        if (!CheckTilesArrayHasNoNulls(importedTileset.tiles))
                        {
                            return false;
                        }
                    }

                    ITilemapImportOperation[] importOperations = ImportUtils.GetObjectsThatImplementInterface<ITilemapImportOperation>();
                    // Load all the tile layers
                    for (int i = 0; i < map.layers.Length; i++)
                    {
                        TMX.Layer layer = map.layers[i];

                        bool success = ImportLayer(layer, newGrid, i, importedTilesets, importOperations);
                        if (!success)
                        {
                            return false;
                        }
                    }

                    // Load all the objects
                    if (map.objectgroup != null && map.objectgroup.objects != null && map.objectgroup.objects.Length > 0)
                    {
                        GameObject newObjectLayer = new GameObject(map.objectgroup.name);
                        newObjectLayer.transform.SetParent(newGrid.transform);
                        Undo.RegisterCreatedObjectUndo(newObjectLayer, "Import object layer");

                        for (int i = 0; i < map.objectgroup.objects.Length; i++)
                        {
                            TMX.Object mapObject = map.objectgroup.objects[i];

                            bool success = ImportMapObject(mapObject, importedTilesets, newObjectLayer, map, importOperations, tilesetDir);
                            if (!success)
                            {
                                return false;
                            }
                        }
                        if (!map.objectgroup.visible)
                        {
                            Renderer[] renderers = newObjectLayer.GetComponentsInChildren<Renderer>();
                            foreach(Renderer r in renderers)
                            {
                                r.enabled = false;
                            }
                        }
                    }
                }
            }

            return true;
        }
    }
}
