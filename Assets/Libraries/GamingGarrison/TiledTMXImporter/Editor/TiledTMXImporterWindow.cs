﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;

namespace GamingGarrison
{
    public class TiledTMXImporterWindow : EditorWindow
    {
        string[] m_sourcePaths;
        Grid m_targetGrid;
        string m_tilesetDir = "Assets/TileSets";

        [MenuItem("Window/Tiled TMX Importer")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow<TiledTMXImporterWindow>(false, "TMX Importer", true);
        }

        public static string[] DropZone(string title, int w, int h)
        {
            GUILayout.Box(title, GUILayout.Width(w), GUILayout.Height(h));
            if (DragAndDrop.objectReferences != null && DragAndDrop.objectReferences.Length > 0) // Don't want this to block regular object dragging
            {
                return null;
            }
            if (DragAndDrop.paths == null || DragAndDrop.paths.Length != 1)
            {
                return null;
            }
            EventType eventType = Event.current.type;
            bool isAccepted = false;

            if (eventType == EventType.DragUpdated || eventType == EventType.DragPerform)
            {
                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                if (eventType == EventType.DragPerform)
                {
                    DragAndDrop.AcceptDrag();
                    isAccepted = true;
                }
                Event.current.Use();
            }

            return isAccepted ? DragAndDrop.paths : null;
        }

        private void OnEnable()
        {
            RefreshSelectionAndGrid();

            EditorApplication.hierarchyWindowChanged += RefreshSelectionAndGrid;
        }

        private void OnDisable()
        {
            EditorApplication.hierarchyWindowChanged -= RefreshSelectionAndGrid;
        }

        void RefreshSelectionAndGrid()
        {
            Grid[] gridsInScene = FindObjectsOfType<Grid>();
            if (gridsInScene != null && gridsInScene.Length == 1)
            {
                m_targetGrid = gridsInScene[0];
            }
            
            foreach(Grid grid in gridsInScene)
            {
                grid.enabled = !grid.enabled;
                grid.enabled = !grid.enabled;  //Turn it off and on again to work-around the fact that Unity's Grid doesn't update on a redo
            }
        }

        void OnGUI()
        {
            GUILayout.Label("Drop a TMX file below:", EditorStyles.boldLabel);

            string[] paths = DropZone("Drop here", 100, 100);
            if (paths != null)
            {
                foreach (string path in paths)
                {
                    Debug.Log("Path dragged = " + path);
                }

                m_sourcePaths = paths;
            }

            bool allExist = false;

            if (m_sourcePaths != null && m_sourcePaths.Length > 0)
            {
                allExist = true;
                foreach (string path in m_sourcePaths)
                {
                    if (!File.Exists(path))
                    {
                        allExist = false;
                        break;
                    }
                }

                for (int i = 0; i < m_sourcePaths.Length; i++)
                {
                    GUILayout.Label(m_sourcePaths[i]);
                }

                if (GUILayout.Button("Clear Path"))
                {
                    m_sourcePaths = null;
                }
            }
            m_targetGrid = EditorGUILayout.ObjectField("Target Tilemap Grid:", m_targetGrid, typeof(Grid), true) as Grid;
            GUILayout.Label("(Leave this empty to spawn a new Grid in the scene)");
            GUILayout.Space(10);
            m_tilesetDir = EditorGUILayout.TextField("Target Tileset directory:", m_tilesetDir);

            if (allExist)
            {
                GUI.backgroundColor = Color.green;
                GUI.enabled = true;
            }
            else
            {
                EditorGUILayout.HelpBox("Drop a TMX file from your operating system", MessageType.Warning);
                GUI.enabled = false;
                GUI.backgroundColor = Color.red;
            }
            if (GUILayout.Button("Import"))
            {
                bool success = TiledTMXImporter.ImportTMXFile(m_sourcePaths[0], m_tilesetDir, m_targetGrid);
                if (success)
                {
                    Debug.Log("Import succeeded!");
                    RefreshSelectionAndGrid();
                }
                else
                {
                    Debug.Log("Import failed :(");
                }
            }
        }
    }
}
