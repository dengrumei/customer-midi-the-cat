﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System;

namespace GamingGarrison
{
    namespace TSX
    {
        [XmlRoot(ElementName = "tileset")]
        public class Tileset
        {
            [XmlAttribute]
            public string name;

            [XmlAttribute]
            public int tilewidth;

            [XmlAttribute]
            public int tileheight;

            [XmlAttribute]
            public int tilecount;

            [XmlAttribute]
            public int columns;

            [XmlElement]
            public Grid grid;

            [XmlElement(ElementName = "tile")]
            public Tile[] tiles;
        }

        public class Grid
        {
            [XmlAttribute]
            public string orientation;

            [XmlAttribute]
            public int width;

            [XmlAttribute]
            public int height;
        }

        public class Tile
        {
            [XmlAttribute]
            public int id;

            [XmlElement]
            public Image image;

            [XmlElement]
            public ObjectGroup objectgroup;

            public bool HasCollisionData()
            {
                return objectgroup != null && objectgroup.objects != null && objectgroup.objects.Length > 0;
            }
        }

        public class Image
        {
            [XmlAttribute]
            public int width;

            [XmlAttribute]
            public int height;

            [XmlAttribute]
            public string source;
        }

        public class ObjectGroup
        {
            [XmlAttribute]
            public string draworder;

            [XmlElement(ElementName = "object")]
            public Object[] objects;
        }

        /// <summary>
        /// Can be used here to represent a collision object on a tile
        /// </summary>
        public class Object
        {
            [XmlAttribute]
            public int id;

            [XmlAttribute]
            public float x;

            [XmlAttribute]
            public float y;

            [XmlAttribute]
            public float width;

            [XmlAttribute]
            public float height;
        }
    }
}
