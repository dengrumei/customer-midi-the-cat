﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEditor;
using UnityEngine;
using System.Linq;

namespace GamingGarrison
{
    public class ImportUtils
    {
        public static T ReadXMLIntoObject<T>(string path)
        {
            StreamReader streamReader = File.OpenText(path);
            if (streamReader == null)
            {
                return default(T);
            }

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.DTD;

            XmlReader xmlReader = XmlTextReader.Create(streamReader, settings);

            XmlSerializer serializer = new XmlSerializer(typeof(T));

            try
            {
                object deserialized = serializer.Deserialize(xmlReader);
                return (T)deserialized;
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message + ": " + e.StackTrace);
                return default(T);
            }
        }

        public static bool CreateAssetFolderIfMissing(string path, bool askPermission)
        {
            if (!AssetDatabase.IsValidFolder(path))
            {
                bool ok = (askPermission ? EditorUtility.DisplayDialog(path + " not found", "Create new directory?", "OK", "Cancel") : true);
                if (ok)
                {
                    ok = CreateAssetFolder(path);
                    if (!ok)
                    {
                        Debug.LogError("Target directory " + path + " could not be created!");
                        return false;
                    }
                }
                else
                {
                    Debug.LogError("Permission not given to create folder by user");
                }
            }
            return true;
        }

        static bool CreateAssetFolder(string path)
        {
            DirectoryInfo parent = Directory.GetParent(path);
            string folderName = Path.GetFileName(path);
            string guid = AssetDatabase.CreateFolder(parent.ToString(), folderName);
            if (guid == null || guid.Length == 0)
            {
                return false;
            }
            return true;
        }

        public static T[] GetObjectsThatImplementInterface<T>()
        {
            List<T> typeList = new List<T>();

            Type type = typeof(T);
            IEnumerable<object> types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => x.IsClass && type.IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
                .Select(x => Activator.CreateInstance(x));

            foreach (object t in types)
            {
                typeList.Add((T)t);
            }
            return typeList.ToArray();
        }

        public static Vector2[] PointsFromString(string pointsString, Vector2 scale)
        {
            string[] pointsSplit = pointsString.Split(' ');
            Vector2[] points = new Vector2[pointsSplit.Length];
            for (int i = 0; i < points.Length; i++)
            {
                string[] vectorComponents = pointsSplit[i].Split(',');
                Debug.Assert(vectorComponents.Length == 2, "This string should have 2 components separated by a comma: " + pointsSplit[i]);
                points[i] = new Vector2(float.Parse(vectorComponents[0]), float.Parse(vectorComponents[1]));
                points[i] = Vector2.Scale(scale, points[i]);
            }
            return points;
        }
    }
}
