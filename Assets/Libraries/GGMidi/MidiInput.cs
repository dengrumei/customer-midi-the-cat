﻿using UnityEngine;
using System.Collections;
using MidiJack;

public class MidiInput : MonoBehaviour
{

    public delegate void MidiIOEventHandler(int note, bool down, float velocity);

    public event MidiIOEventHandler MidiIOEvent;

    bool[] m_midiKeysDown;
    bool[] m_pcKeysDown;

    public AudioClip pianoC3;
    bool playSoundsOnMidiInput = true;
    int pcBottomNote = 60;
    public static KeyCode[] pcKeys =
    {
        KeyCode.Q, KeyCode.Alpha2, KeyCode.W, KeyCode.Alpha3, KeyCode.E, KeyCode.R, KeyCode.Alpha5, KeyCode.T, KeyCode.Alpha6, KeyCode.Y, KeyCode.Alpha7, KeyCode.U,
        KeyCode.I, KeyCode.Alpha9, KeyCode.O, KeyCode.Alpha0, KeyCode.P, KeyCode.LeftBracket, KeyCode.Equals, KeyCode.RightBracket
    };
    /*KeyCode[] pcKeys =
    {
        KeyCode.A, KeyCode.W, KeyCode.S, KeyCode.E, KeyCode.D, KeyCode.F, KeyCode.T, KeyCode.G, KeyCode.Y, KeyCode.H, KeyCode.U, KeyCode.J, KeyCode.K, KeyCode.O, KeyCode.L, KeyCode.P, KeyCode.Semicolon, KeyCode.BackQuote
    };*/

    static MidiInput m_instance;

    public bool m_muteKeys;

    public static bool s_usingPCKeys = false;

    public static MidiInput Instance
    {
        get
        {
            if (m_instance == null)
            {
                m_instance = FindObjectOfType<MidiInput>();
            }
            return m_instance;
        }
    }

    void Start()
    {
        m_midiKeysDown = new bool[128];
        m_pcKeysDown = new bool[128];
    }

    public int GetKeyCount()
    {
        return m_midiKeysDown.Length;
    }

    public bool GetKeyDown(int note)
    {
        return m_midiKeysDown[note] || m_pcKeysDown[note];
    }
	
    // Update is called once per frame
    void Update ()
    {
        // MIDI input
        for (int i = 0; i < m_midiKeysDown.Length; i++)
        {
            float keyPress = MidiMaster.GetKey(i);
            if (keyPress > 0.0f && !m_midiKeysDown[i])
            {
                m_midiKeysDown[i] = true;
                s_usingPCKeys = false;
                if (playSoundsOnMidiInput && !m_muteKeys)
                {
                    MidiUtil.PlayKeySound(i, pianoC3);
                }
                //Debug.Log("Key down = " + i);
                MidiIOEvent(i, true, MidiMaster.GetKey(i));
            }
            else if (keyPress <= 0.0f && m_midiKeysDown[i])
            {
                m_midiKeysDown[i] = false;
                //Debug.Log("Key up = " + i);
                MidiIOEvent(i, false, 0.0f);
            }
        }
        // PC keyboard input
        for (int i = 0; i < pcKeys.Length; i++)
        {
            if (Input.GetKeyDown(pcKeys[i]))
            {
                s_usingPCKeys = true;
                int noteID = pcBottomNote + i;
                if (!m_muteKeys)
                {
                    MidiUtil.PlayKeySound(noteID, pianoC3);
                }
                m_pcKeysDown[noteID] = true;
                MidiIOEvent(noteID, true, 1.0f);
            }
            if (Input.GetKeyUp(pcKeys[i]))
            {
                int noteID = pcBottomNote + i;
                m_pcKeysDown[noteID] = false;
                MidiIOEvent(noteID, false, 0.0f);
            }
        }
    }
}
