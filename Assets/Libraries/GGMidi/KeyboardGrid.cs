﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KeyboardGrid : MonoBehaviour
{
	public GameObject whiteNotePrefab;
	public GameObject blackNotePrefab;
	public GameObject keyLabelPrefab;
    public bool showOnlyPCKeysMode;

    public GameObject noteLabelPrefab;
    public GameObject highlightCirclePrefab;

    public Color[] m_noteColoursFromA =
    {
        Color.black, Color.gray,
        Color.blue,
        Color.red, Color.gray,
        Color.green, Color.gray,
        Color.yellow,
        Color.cyan, Color.gray,
        Color.magenta
    };

	int keysWide;
	int lowestKeyNote;

	float keyHeight = 2f;
	float blackKeyHeight = 1f;

	float[] gridPositions;
	float keyWidth;

	GameObject[] keys;
	Color[] originalColours;

	MidiInput midiIO;

	public float keyDepressionDistance = 0.1f;
	public Color keyDepressionColour = Color.gray;

	bool[] keysDown;
    GameObject[] highlights;

	private int keyDownCount;
	private int keysDownBeforePanicking = 5;
	public float panicTimeLimit = 1f;
	private float panicTimer;

	// Use this for initialization
	void Awake ()
	{
		if(ConfigControl.lowestNote != -1 && ConfigControl.highestNote != -1)
		{
			lowestKeyNote = ConfigControl.lowestNote;
			keysWide = MidiUtil.WhiteKeysBetweenNotes(ConfigControl.lowestNote, ConfigControl.highestNote);
		}

        //Camera mainCamera = Camera.main;
        //float width = mainCamera.orthographicSize * mainCamera.aspect;
        RectTransform rectTransform = GetComponent<RectTransform>();
        float width = rectTransform.rect.width;
		float height = rectTransform.rect.height;
        keyHeight = height;
        blackKeyHeight = height * 0.6f;

		keyWidth = (width) / keysWide;
        float offset = 0.0f;
		//float offset = - width + (keyWidth * 0.5f);

		int currentNote = lowestKeyNote;
		for(int i = 0; i < keysWide; i++)
		{
			int fromA = MidiUtil.NotesFromA(currentNote) % 12;
			bool blackNote = MidiUtil.m_blackNotesFromA[fromA];
			if(blackNote)
			{
				currentNote++;
			}
			currentNote++;
		}
		int keysNeeded = currentNote - lowestKeyNote;
		keys = new GameObject[keysNeeded];
		originalColours = new Color[keysNeeded];
		keysDown = new bool[keysNeeded];
        highlights = new GameObject[keysNeeded];

		gridPositions = new float[keysNeeded];
		currentNote = lowestKeyNote;
		for(int i = 0; i < keysWide; i++)
		{
			int fromA = MidiUtil.NotesFromA(currentNote) % 12;
			bool blackNote = MidiUtil.m_blackNotesFromA[fromA];
			if(blackNote)
			{
				//add a black note
				GameObject newBlackKey = GameObject.Instantiate(blackNotePrefab) as GameObject;
				newBlackKey.transform.SetParent(transform);
				newBlackKey.transform.localPosition = new Vector3((keyWidth * i) + offset, (keyHeight) - (blackKeyHeight / 2), -1);
				newBlackKey.GetComponent<RectTransform>().sizeDelta = new Vector2(keyWidth * 0.55f, blackKeyHeight);
                gridPositions[currentNote - lowestKeyNote] = newBlackKey.transform.position.x;
				keys[currentNote - lowestKeyNote] = newBlackKey;
				currentNote++;
			}

			GameObject newKey = GameObject.Instantiate(whiteNotePrefab) as GameObject;
			newKey.transform.SetParent(transform);
			newKey.transform.localPosition = new Vector3((keyWidth * i) + offset, 0, 0);
            newKey.GetComponent<RectTransform>().sizeDelta = new Vector2(keyWidth * 0.95f, keyHeight);
            newKey.GetComponent<RectTransform>().SetAsFirstSibling();
            gridPositions[currentNote - lowestKeyNote] = newKey.transform.position.x;
			keys[currentNote - lowestKeyNote] = newKey;

			if(noteLabelPrefab != null)
			{
				GameObject newLabel = GameObject.Instantiate(noteLabelPrefab) as GameObject;
				newLabel.transform.SetParent(newKey.transform);
				newLabel.transform.localPosition = new Vector3(keyWidth / 2, keyHeight / 3, -1f);
                newLabel.GetComponent<RectTransform>().sizeDelta = new Vector2(1.0f, 1.0f);
                newLabel.transform.SetParent(transform, true);
				Text text = newLabel.GetComponent<Text>();
				fromA = MidiUtil.NotesFromA(currentNote) % 12;
                text.text = MidiUtil.NoteToString(currentNote);
			}
            if (keyLabelPrefab != null)
            {
                if (!(showOnlyPCKeysMode && !MidiInput.s_usingPCKeys))
                {
                    GameObject newLabel = GameObject.Instantiate(keyLabelPrefab) as GameObject;
                    newLabel.transform.SetParent(newKey.transform);
                    newLabel.transform.localPosition = new Vector3(keyWidth / 2, keyHeight / 5, -1f);
                    newLabel.GetComponent<RectTransform>().sizeDelta = new Vector2(1.0f, 1.0f);
                    Text text = newLabel.GetComponent<Text>();
                    fromA = MidiUtil.NotesFromA(currentNote) % 12;

                    if (MidiInput.s_usingPCKeys)
                    {
                        text.text = MidiUtil.m_pcKeyNamesFromA[fromA];
                    }
                    else
                    {
                        text.text = MidiUtil.m_noteNamesFromA[fromA];
                    }
                    text.color = m_noteColoursFromA[fromA];
                }
            }
            currentNote++;
		}
	}

    void OnEnable()
    {
        midiIO = FindObjectOfType<MidiInput>();
        midiIO.MidiIOEvent += HandleMidiIOEvent;
    }

    void OnDisable()
    {
        midiIO.MidiIOEvent -= HandleMidiIOEvent;
    }

    void SetLaneHighlight(int lane, bool shouldHighlight)
    {
        if (highlights[lane] == null && shouldHighlight)
        {
            GameObject newHighlight = GameObject.Instantiate(highlightCirclePrefab) as GameObject;
            newHighlight.transform.SetParent(keys[lane].transform);
            newHighlight.transform.localPosition = new Vector3(keyWidth / 2, keyHeight / 5, -3.0f);
            newHighlight.GetComponent<RectTransform>().sizeDelta = new Vector2(1.0f, 1.0f);
            int fromA = MidiUtil.NotesFromA(lowestKeyNote + lane) % 12;
            newHighlight.GetComponent<Image>().color = m_noteColoursFromA[fromA];
            highlights[lane] = newHighlight;
        }
        else if (highlights[lane] != null && !shouldHighlight)
        {
            Destroy(highlights[lane]);
            highlights[lane] = null;
        }
    }

    public void SetNoteHighlighted(int note, bool shouldHighlight)
    {
        int lane = GetGridLane(note);
        SetLaneHighlight(lane, shouldHighlight);
    }

    public void SetAllNoteHighlights(bool shouldHighlight)
    {
        for (int i = 0; i < highlights.Length; i++)
        {
            SetLaneHighlight(i, shouldHighlight);
        }
    }

    public Vector3 GetKeyPosition(int note)
    {
        int lane = GetGridLane(note);
        bool blackKey = MidiUtil.IsBlackNote(note);
        if (blackKey)
        {
            return keys[lane].transform.position + new Vector3(0.0f, -keyHeight * 0.25f, 0.0f);
        }
        return keys[lane].transform.position + new Vector3(keyWidth * 0.5f, 0.0f, 0.0f);
    }

    public void SetKeyDown(int note, bool down)
    {
        HandleMidiIOEvent(note, down, 127);
    }

	void HandleMidiIOEvent (int note, bool down, float velocity)
	{
        if (gridPositions == null)
        {
            return;
        }
		int lane = GetGridLane(note);
		GameObject key = keys[lane];

		Vector3 localScale = key.transform.localScale;

		bool blackKey = MidiUtil.m_blackNotesFromA[MidiUtil.NotesFromA(note) % 12];
		if(down)
		{
            if (keysDown[lane])
            {
                return;
            }
			originalColours[lane] = key.GetComponent<Image>().color;
			key.GetComponent<Image>().color = keyDepressionColour;

			if(blackKey)
			{
                key.transform.position += new Vector3(0, -keyDepressionDistance * 0.5f, 0f);
				key.GetComponent<RectTransform>().sizeDelta = new Vector2(key.GetComponent<RectTransform>().sizeDelta.x, blackKeyHeight + (keyDepressionDistance));
            }
            else
            {
                key.transform.position += new Vector3(0, -keyDepressionDistance, 0f);
            }
			keysDown[lane] = true;
			keyDownCount++;
		}
		else if(keysDown[lane])
		{
            if (!keysDown[lane])
            {
                return;
            }
			key.GetComponent<Image>().color = originalColours[lane];
			
			if(blackKey)
			{
                key.transform.position += new Vector3(0, keyDepressionDistance * 0.5f, 0f);
                key.GetComponent<RectTransform>().sizeDelta = new Vector2(key.GetComponent<RectTransform>().sizeDelta.x, blackKeyHeight);
			}
            else
            {
                key.transform.position += new Vector3(0, keyDepressionDistance, 0f);
            }
			keysDown[lane] = false;
			keyDownCount--;
		}
	}

	public void Update()
	{
		if(keyDownCount >= keysDownBeforePanicking)
		{
			panicTimer += Time.deltaTime;
		}
		else
		{
			panicTimer = 0f;
		}
	}

	public bool IsUserPanicking()
	{
		return panicTimer > 0;
	}

	public bool HasUserPanicked()
	{
		return panicTimer > panicTimeLimit;
	}

	public int GetGridLane(int note)
	{
		//Debug.Log("  ------- GetGridLane(note = "+note+")");
		//Debug.Log("lowestKeyNote = "+lowestKeyNote);
		int fromLowestKey = (note - lowestKeyNote);
		//Debug.Log("fromLowestKey = "+fromLowestKey);

		//Debug.Log("gridPositions.length = "+gridPositions.Length);
		
		int octavesAvailable = (gridPositions.Length / 12) + 1;
        //Debug.Log("octavesAvailabe = "+octavesAvailable);

        while (fromLowestKey < -12)
        {
            fromLowestKey += 12;
        }
        fromLowestKey = (fromLowestKey + (12 * octavesAvailable)) % (12 * octavesAvailable);
		//Debug.Log("fromLowestKey = "+fromLowestKey);
		
		if(fromLowestKey >= gridPositions.Length)
		{
			fromLowestKey = fromLowestKey - (12 * (octavesAvailable - 1));
		}
		if(fromLowestKey < 0 || fromLowestKey >= gridPositions.Length)
		{
			Debug.Log("Wtf, fromLowestKey is "+ fromLowestKey + " with note " + note +" octaves available = "+octavesAvailable);
			return 0;
		}
		return fromLowestKey;
	}

	public float GetGridPosition(int note)
	{
		return gridPositions[GetGridLane(note)];
	}
}
