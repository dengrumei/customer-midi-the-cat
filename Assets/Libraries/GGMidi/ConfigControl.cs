﻿using UnityEngine;
using System.Collections;

public class ConfigControl : MonoBehaviour
{
	public TextMesh instruction;

	MidiInput midiIO;

    public static int lowestNote = 60; // 36; Values for my Keystation
    public static int highestNote = 71; // 96;

	int configStage = 0;

	// Use this for initialization
	void Start ()
	{
		midiIO = FindObjectOfType<MidiInput>();
		midiIO.MidiIOEvent += HandleMidiIOEvent;

        instruction.text = "Press the LOWEST key on your keyboard";
    }

	void HandleMidiIOEvent (int note, bool down, float velocity)
	{
		if(down)
		{
			if(configStage == 0)
			{
				lowestNote = note;
				instruction.text = "Press the HIGHEST key on your keyboard";
				configStage = 1;
			}
			else if(configStage == 1)
			{
				highestNote = note;
				instruction.text = "Done!  Press C for campaign, G for group mode";
				configStage = 2;
			}
			else if(configStage == 2)
			{
				if(MidiUtil.m_noteNamesFromA[MidiUtil.NotesFromA(note)%12] == "C")
				{
					AutoFade.LoadLevel("Dunk1", 1f, 1f, Color.black);
				}
				else if(MidiUtil.m_noteNamesFromA[MidiUtil.NotesFromA(note)%12] == "G")
				{
					//AutoFade.LoadLevel("SkytracksScene", 1f, 1f, Color.black);
				}
			}
		}
	}
}
