using UnityEngine;
using System.Collections;

public class MidiUtil
{
    public static bool[] m_blackNotesFromA = { false, true, false, false, true, false, true, false, false, true, false, true };
    public static int[] m_staffRowFromA = { 0, 0, 1, 2, 2, 3, 3, 4, 5, 5, 6, 6 };
    public static string[] m_noteNamesFromA = { "A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#" };
    public static string[] m_pcKeyNamesFromA = {"Y", "Y#", "U", "Q", "Q#", "W", "W#", "E", "R", "R#", "T", "T#" };

    public static int NotesFromA(int noteNumber)
    {
        int fromA = ((noteNumber + 3) % 24);
        return fromA;
    }

    public static int ClosestWhiteNote(int noteNumber)
    {
        if (IsBlackNote(noteNumber))
        {
            return noteNumber - 1;
        }
        else return noteNumber;
    }

    public static bool IsBlackNote(int noteNumber)
    {
        int fromAInOctave = NotesFromA(noteNumber) % 12;
        return m_blackNotesFromA[fromAInOctave];
    }

    public static int WhiteKeysBetweenNotes(int lowNote, int highNote)
    {
        return KeysBetweenNotes(lowNote, highNote, false);
    }

    public static int BlackKeysBetweenNotes(int lowNote, int highNote)
    {
        return KeysBetweenNotes(lowNote, highNote, true);
    }

    public static int KeysBetweenNotesIfSame(int lowNote, int highNote, bool blackKeysWanted = false)
    {
        if (MidiUtil.IsBlackNote(lowNote) != MidiUtil.IsBlackNote(highNote) || MidiUtil.IsBlackNote(lowNote) != blackKeysWanted)
        {
            return -1;
        }
        return KeysBetweenNotes(lowNote, highNote, blackKeysWanted);
    }

    /** Counts either the white or the black keys between two notes in one direction (and inclusive) */
    public static int KeysBetweenNotes(int lowNote, int highNote, bool blackKeysWanted = false)
    {
        int count = 0;
        for (int i = lowNote; i <= highNote; i++)
        {
            int notesFromA = NotesFromA(i) % 12;
            if (m_blackNotesFromA[notesFromA] == blackKeysWanted)
            {
                count++;
            }
        }
        return count;
    }

    public static int[] MajorScaleNotes = { 0, -1, 1, -1, 2, 3, -1, 4, -1, 5, -1, 6 };

    public static int ScaleKeysBetweenNotes(int noteA, int noteB, int[] scaleNotes, int scaleRootFromC = 0)
    {
        int count = 0;
        if (noteB < noteA) // We don't care about direction
        {
            int temp = noteA;
            noteA = noteB;
            noteB = temp;
        }
        for (int i = noteA; i <= noteB; i++)
        {
            int scaleNote = GetScaleNoteFromNote(i, scaleNotes, scaleRootFromC);
            if (scaleNote >= 0)
            {
                count++;
            }
        }
        return count;
    }

    public static int GetScaleNoteFromNote(int note, int[] scaleNotes, int scaleRootFromC = 0)
    {
        int notesFromScaleRoot = ((note + 12) - scaleRootFromC) % 12;
        return scaleNotes[notesFromScaleRoot];
    }

    public static double Max(double a, double b)
    {
        return (a > b ? a : b);
    }

    //Very font specific (MusiQwik)
    public static string NoteToString(int noteNumber)
    {
        int fromA = NotesFromA(noteNumber);
        int staffRow = m_staffRowFromA[fromA % 12];
        if (fromA >= 12)
        {
            staffRow += 7;
        }

        string result = "";
        if (m_blackNotesFromA[fromA % 12])
        {
            result += (char)(208 + staffRow);
        }
        result += (char)(80 + staffRow);
        return result;
    }

    private static float scale = Mathf.Pow (2f,1.0f/12f);

    public static void PlayKeySound(int noteID, AudioClip clip)
    {
        float pitch = MidiUtil.GetPitchFromSemitoneShift(noteID - 48);
        MidiUtil.PlayClipAtPoint(clip, Camera.main.transform.position, 0.25f, pitch);
    }

    public static float GetPitchFromSemitoneShift(int shift)
    {
        return Mathf.Pow(scale, shift);
    }

    public static GameObject PlayClipAtPoint(AudioClip clip, Vector3 position, float volume, float pitch)
    {
        GameObject obj = new GameObject();
        obj.transform.position = position;
        AudioSource source = obj.AddComponent<AudioSource>();
        source.pitch = pitch;
        source.PlayOneShot(clip, volume);
        GameObject.Destroy(obj, clip.length / pitch);
        return obj;
    }

    public static double RoundBeatToNearestInterval(double beatTime, double intervalLength)
    {
        double intervalTime = beatTime / intervalLength;
        int nearestInterval = (int)(intervalTime + 0.5);
        return nearestInterval * intervalLength;
    }

    public static double RoundBeatToLatestInterval(double beatTime, double intervalLength)
    {
        double intervalTime = beatTime / intervalLength;
        int latestInterval = (int)intervalTime;
        return latestInterval * intervalLength;
    }

    public static int[] s_MajorScaleNoteToNoteOffset = new int[7] { 0, 2, 4, 5, 7, 9, 11 };
    public static int MajorScaleNoteToNoteOffset(int scaleNote)
    {
        return s_MajorScaleNoteToNoteOffset[scaleNote];
    }

    /// <summary>
    /// Returns -1 if note is not in the scale
    /// </summary>
    /// <param name="note"></param>
    /// <returns></returns>
    public static int NoteOffsetToMajorScaleNote(int note)
    {
        if (note < 0)
        {
            return -1;
        }
        note = note % 12;
        int scaleNote = -1;
        for (int n = 0; n < 7; n++)
        {
            int testNote = MidiUtil.MajorScaleNoteToNoteOffset(n);
            if (testNote == note)
            {
                scaleNote = n;
                break;
            }
        }
        return scaleNote;
    }

    static int[] s_MajorPentatonicScaleNoteToNoteOffset = new int[5] { 0, 2, 5, 7, 9 };
    public static int MajorPentatonicScaleNoteToNoteOffset(int scaleNote)
    {
        return s_MajorPentatonicScaleNoteToNoteOffset[scaleNote];
    }
}
