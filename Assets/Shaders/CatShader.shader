﻿Shader "Unlit/CatShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1, 1, 1, 1)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _Color;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			float3 rgb2hsv(float3 c)
			{
			    float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			    float4 p = lerp(float4(c.bg, K.wz), float4(c.gb, K.xy), step(c.b, c.g));
			    float4 q = lerp(float4(p.xyw, c.r), float4(c.r, p.yzx), step(p.x, c.r));

			    float d = q.x - min(q.w, q.y);
			    float e = 1.0e-10;
			    return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
			}

			float3 hsv2rgb(float3 c)
			{
			    float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
			    float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
			    return c.z * lerp(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed3 luminanceWeights = fixed3(0.299, 0.587, 0.114);
				float luminance = dot(col.rgb, luminanceWeights);

				float3 hsv = rgb2hsv(col.rgb);
				float3 targetHSV = rgb2hsv(_Color.rgb);
				targetHSV.z = luminance * targetHSV.z;
				float3 targetRGB = hsv2rgb(targetHSV);

				col = lerp(col, float4(targetRGB, col.a), smoothstep(0.5, 1.0, 1.0 - hsv.y));
				//float3 endHSV = lerp(hsv, targetHSV, smoothstep(0.0, 1.0, 1.0 - hsv.y));
				//col = half4(hsv2rgb(endHSV), col.a);

				//float value = hsv.y;
				//col = half4(value, value, value, 1.0);

				//float targetLuminance = dot(_Color.rgb, luminanceWeights);
				//fixed4 toTarget = _Color - col;
				//col += toTarget * luminance * (1.0 - hsv.y);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
