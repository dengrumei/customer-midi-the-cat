﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

/*

That shader is usually used to draw light obstacles.
Have main texture and multiplicative color. 

*/


Shader "Unlit/GoopShader" {
Properties {
	_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	_NormalTex ("Normalmap", 2D) = "bump" {}
	_HeightTex("Heightmap", 2D) = "white" {}
	_UVLookup("UV Lookup", 2D) = "white" {}
	_UVScroll("Scroll speed", Vector) = ( 0, 0, 0, 0 )
}

SubShader {
	Tags {
		"Queue"="Transparent" 
		"IgnoreProjector"="True" 
		"RenderType"="Transparent" 
		"LightObstacle"="True"
	}
	LOD 100
	
	Cull Off
	ZWrite Off
	Lighting Off
	Blend SrcAlpha OneMinusSrcAlpha 
	
	Pass {  
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR0;
				float3 worldPos : TEXCOORD1;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _UVLookup;
			float4 _UVScroll;

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
				//o.texcoord.x += _SinTime.x;
				//o.texcoord.y -= _Time.y;
				o.color = v.color;
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

				return o;
			}
			
			fixed4 frag (v2f i) : COLOR
			{
				float time = _Time.y + i.worldPos.x;
				fixed4 originalCol = tex2D(_MainTex, i.texcoord);
				fixed2 lookupCoord = i.texcoord;
				lookupCoord = tex2D(_UVLookup, lookupCoord).rg;
				lookupCoord.xy += time * _UVScroll.xy;
				lookupCoord.xy += sin(time) * _UVScroll.zw;
				//o.texcoord.x += _SinTime.x;
				//o.texcoord.y -= _Time.y;
				fixed4 col = tex2D(_MainTex, lookupCoord);
				//col.a = originalCol.a;
				return col*i.color;
			}
		ENDCG
	}
}

}
